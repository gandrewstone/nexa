---
name: "nexa-linux-x86"
enable_cache: true
suites:
- "focal"
architectures:
- "amd64"
packages:
- "curl"
- "binutils"
- "g++8"
- "gcc-8"
- "binutils-gold"
- "git"
- "pkg-config"
- "autoconf"
- "libtool"
- "automake"
- "faketime"
- "binutils"
- "bison"
- "bsdmainutils"
- "ca-certificates"
- "python"
- "openjdk-17-jdk-headless"
- "llvm"
- "libclang-dev"
- "python3-git" # building Rostrum
- "clang" # for rocksdb in Rostrum
- "cmake" # for rocksdb in Rostrum

reference_datetime: "2020-08-20 00:00:00"
remotes:
- "url": "https://gitlab.com/nexa/nexa.git"
  "dir": "nexa"
files: []
script: |
  set -e -o pipefail

  if [ "${ONLY_LIBNEXA_ARG}" != "--enable-only-libnexa" ]; then
    JAVA_LIBNEXA="--enable-javalibnexa"
  fi

  WRAP_DIR=$HOME/wrapped
  HOSTS="x86_64-linux-gnu"
  CONFIGFLAGS="--enable-glibc-back-compat --enable-reduce-exports --disable-bench --disable-gui-tests --disable-tests --enable-shared ${JAVA_LIBNEXA} ${ONLY_LIBNEXA_ARG}"
  FAKETIME_HOST_PROGS=""
  FAKETIME_PROGS="date ar ranlib nm strip"
  HOST_CFLAGS="-O2 -g"
  HOST_CXXFLAGS="-O2 -g"
  HOST_LDFLAGS=-static-libstdc++

  export TAR_OPTIONS="--mtime="$REFERENCE_DATE\\\ $REFERENCE_TIME""
  export TZ="UTC"
  export BUILD_DIR=`pwd`
  mkdir -p ${WRAP_DIR}
  if test -n "$GBUILD_CACHE_ENABLED"; then
    export SOURCES_PATH=${GBUILD_COMMON_CACHE}
    export BASE_CACHE=${GBUILD_PACKAGE_CACHE}
    mkdir -p ${BASE_CACHE} ${SOURCES_PATH}
  fi

  # Create global faketime wrappers
  function create_global_faketime_wrappers {
  for prog in ${FAKETIME_PROGS}; do
    echo '#!/bin/bash' > ${WRAP_DIR}/${prog}
    echo "REAL=\`which -a ${prog} | grep -v ${WRAP_DIR}/${prog} | head -1\`" >> ${WRAP_DIR}/${prog}
    echo 'export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/faketime/libfaketime.so.1' >> ${WRAP_DIR}/${prog}
    echo "export FAKETIME=\"$1\"" >> ${WRAP_DIR}/${prog}
    echo "exec \"\$REAL\" \"\$@\"" >> ${WRAP_DIR}/${prog}
    chmod +x ${WRAP_DIR}/${prog}
  done
  }

  # Create per-host faketime wrappers
  function create_per-host_faketime_wrappers {
  for i in $HOSTS; do
    for prog in ${FAKETIME_HOST_PROGS}; do
        echo '#!/bin/bash' > ${WRAP_DIR}/${i}-${prog}
        echo "REAL=\`which -a ${i}-${prog} | grep -v ${WRAP_DIR}/${i}-${prog} | head -1\`" >> ${WRAP_DIR}/${i}-${prog}
        echo 'export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/faketime/libfaketime.so.1' >> ${WRAP_DIR}/${i}-${prog}
        echo "export FAKETIME=\"$1\"" >> ${WRAP_DIR}/${i}-${prog}
        echo "exec \"\$REAL\" \"\$@\"" >> ${WRAP_DIR}/${i}-${prog}
        chmod +x ${WRAP_DIR}/${i}-${prog}
    done
  done
  }

  # Faketime for depends so intermediate results are comparable
  export PATH_orig=${PATH}
  create_global_faketime_wrappers "2000-01-01 12:00:00"
  create_per-host_faketime_wrappers "2000-01-01 12:00:00"
  export PATH=${WRAP_DIR}:${PATH}

  cd nexa
  BASEPREFIX=`pwd`/depends
  DEPENDS_LIBNEXA="ONLY_LIBNEXA=0"
  if [ "${ONLY_LIBNEXA_ARG}" = "--enable-only-libnexa" ]; then
    DEPENDS_LIBNEXA="ONLY_LIBNEXA=1 NO_RUST=1"
  fi
  # Build dependencies for each host
  for i in $HOSTS; do
    if [ "${i}" = "x86_64-linux-gnu" ]; then
      make ${MAKEOPTS} -C ${BASEPREFIX} HOST="${i}" ${DEPENDS_LIBNEXA}
    else
      make ${MAKEOPTS} -C ${BASEPREFIX} HOST="${i}" NO_RUST=1 ${DEPENDS_LIBNEXA}
    fi
  done

  # Faketime for binaries
  export PATH=${PATH_orig}
  create_global_faketime_wrappers "${REFERENCE_DATETIME}"
  create_per-host_faketime_wrappers "${REFERENCE_DATETIME}"
  export PATH=${WRAP_DIR}:${PATH}

  # Create the release tarball using (arbitrarily) the first host
  ./autogen.sh
  CONFIG_SITE=${BASEPREFIX}/`echo "${HOSTS}" | awk '{print $1;}'`/share/config.site ./configure ${ONLY_LIBNEXA_ARG} --prefix=/
  make dist
  SOURCEDIST=`echo nexa-*.tar.gz`
  DISTNAME=`echo ${SOURCEDIST} | sed 's/.tar.*//'`
  # Correct tar file order
  mkdir -p temp
  pushd temp
  tar xf ../$SOURCEDIST
  find nexa* | sort | tar --no-recursion --mode='u+rw,go+r-w,a+X' --owner=0 --group=0 -c -T - | gzip -9n > ../$SOURCEDIST
  popd

  ORIGPATH="$PATH"
  # Extract the release tarball into a dir for each host and build
  for i in ${HOSTS}; do
    export PATH=${BASEPREFIX}/${i}/native/bin:${ORIGPATH}
    mkdir -p distsrc-${i}
    cd distsrc-${i}
    INSTALLPATH=`pwd`/installed/${DISTNAME}
    mkdir -p ${INSTALLPATH}
    cp ../INSTALL.md ${INSTALLPATH}/
    tar --strip-components=1 -xf ../$SOURCEDIST
    CONFIG_SITE=${BASEPREFIX}/${i}/share/config.site ./configure --prefix=/ --disable-ccache --disable-maintainer-mode --disable-dependency-tracking ${CONFIGFLAGS} CFLAGS="${HOST_CFLAGS}" CXXFLAGS="${HOST_CXXFLAGS}" LDFLAGS="${HOST_LDFLAGS}"
    if [ "$i" = "x86_64-linux-gnu" ]; then
        # Scope of rostrum support is for servers running linux.
        # We'll support x86_64 only for now.
        # But do not build it if we are only building libnexa
        if [ "${ONLY_LIBNEXA_ARG}" != "--enable-only-libnexa" ]; then
          make ${MAKEOPTS} rostrum
        fi
    fi
    export JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64
    make ${MAKEOPTS}
    make ${MAKEOPTS} -C src check-security
    make ${MAKEOPTS} -C src check-symbols
    make install DESTDIR=${INSTALLPATH}
    if [ -f src/rostrum ]; then
        cp src/rostrum ${OUTDIR}/rostrum-${i}.debug
        strip src/rostrum
        cp src/rostrum ${INSTALLPATH}/bin/rostrum
    fi
    # BU save the executables with debug symbols
    if [ -f src/nexad ]; then
      cp -f src/nexad ${OUTDIR}/nexad-${i}.debug
    fi
    if [ -f src/qt/nexa-qt ]; then
      cp -f src/qt/nexa-qt ${OUTDIR}/nexa-qt-${i}.debug
    fi

    make install-strip DESTDIR=${INSTALLPATH}
    #BU clean and tar up the output files
    cd installed
    find . -name "lib*.la" -delete
    find . -name "lib*.a" -delete
    rm -rf ${DISTNAME}/lib/pkgconfig
    find ${DISTNAME} | sort | tar --no-recursion --mode='u+rw,go+r-w,a+X' --owner=0 --group=0 -c -T - | gzip -9n > ${OUTDIR}/${DISTNAME}-${i}.tar.gz
    cd ../../
  done
  mkdir -p $OUTDIR/src
  mv $SOURCEDIST $OUTDIR/src
  mv ${OUTDIR}/${DISTNAME}-x86_64-*.tar.gz ${OUTDIR}/${DISTNAME}-linux64.tar.gz
