```
listactiveaddresses

Lists addresses 
made public by common use as inputs or as the resulting change

Result:
[
]

Examples:
> nexa-cli listaddressgroupings 
> curl --user myusername --data-binary '{"jsonrpc": "1.0", "id":"curltest", "method": "listaddressgroupings", "params": [] }' -H 'content-type: text/plain;' http://127.0.0.1:7227/

```
