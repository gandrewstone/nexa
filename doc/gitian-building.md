Gitian build
============

Nexa has an actively maintained fork of the Gitian project that can be found here: https://gitlab.com/nexa/gitian-builder

Use the instructions and build scripts in that repo to build deterministic Nexa binaries using Gitian tools and docker containers. 
