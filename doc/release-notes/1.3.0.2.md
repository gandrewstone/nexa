Release Notes for Nexa 1.3.0.2
======================================================

Nexa version 1.3.0.2 is now available from:

  <https://gitlab.com/nexa/nexa/-/releases>

Please report bugs using the issue tracker at github:

  <https://gitlab.com/nexa/nexa/-/issues>

This is minor release of Nexa, for more information about Nexa see:

- https://nexa.org
- https://spec.nexa.org

Main changes in 1.3.0.2
-----------------------

This is list of the main changes that have been merged in this release:

- improve the performance of `getbalance` and `listunspent` rpc calls for huge wallet
- add possibility to specify the destination address for the `consolidate` rpc command
- minor bug fixes

Commit details
--------------

- `a1a68a2b0` Improve the performance of listunspent() and getbalance() (Peter Tschipper)
- `6d759d6da` Add 2 optional parameters to consolidate RPC command (Andrea Suisani)
- `794bae49f` fix include issue and add better error reporting in cashlib (Andrew Stone)
- `a8a622558` Increase the package failure params when mining blocks (Peter Tschipper)
- `748368d6d` allow compiling with bdb 4.8 when using --with-incompatible-bdb (Griffith)
- `b23192578` Remove unused uint256 from COutPut (Peter TSchipper)
- `020420c5b` Remove the deprecated minTxFee and fallbackFee tweaks (Peter Tschipper)
- `a120e9874` #include <deque> in adaptive blocksize header (Jørgen Svennevik Notland)
- `98f61686f` Fix bug in loading of txpool (Peter Tschipper)
- `6f6519d2b` Only sort the coins when the wallet is small (Peter Tschipper)
- `138186dc3` change lock type to proper writelock for mempool.isSpent (Griffith)
- `3978dd07f` AssertWriteLock now checks if the current thread has the exclusive lock (Griffith)
- `e35d7fe4e` Fix spurious failures in mining_adapative_blocksize.py (Peter TSchipper)

Credits
-------

Thanks to everyone who directly contributed to this release:

- Andrea Suisani
- Andrew Stone
- Griffith
- Jørgen Svennevik Notland
- Peter Tschipper
