<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>AddressBookPage</name>
    <message>
        <location filename="../forms/addressbookpage.ui" line="+30"/>
        <source>Right-click to edit address or label</source>
        <translation>右クリックでアドレスまたはラベルを編集します</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Create a new address</source>
        <translation>新規アドレスの作成</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;New</source>
        <translation>新規(&amp;N)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Copy the currently selected address to the system clipboard</source>
        <translation>現在選択されているアドレスをシステムのクリップボードにコピーする</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy</source>
        <translation>コピー(&amp;C)</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>C&amp;lose</source>
        <translation>閉じる(&amp;C)</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="+90"/>
        <source>&amp;Copy Address</source>
        <translation>アドレスをコピー (&amp;C)</translation>
    </message>
    <message>
        <location filename="../forms/addressbookpage.ui" line="-53"/>
        <source>Delete the currently selected address from the list</source>
        <translation>選択されたアドレスを一覧から削除する</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Export the data in the current tab to a file</source>
        <translation>ファイルに現在のタブのデータをエクスポート</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Export</source>
        <translation>エクスポート (&amp;E)</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>&amp;Delete</source>
        <translation>削除(&amp;D)</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="-39"/>
        <source>Choose the address to send coins to</source>
        <translation>送信先のアドレスを選択</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose the address to receive coins with</source>
        <translation>支払いを受け取るアドレスを指定する</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>C&amp;hoose</source>
        <translation>選択(&amp;C)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sending addresses</source>
        <translation>アドレス送信中</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Receiving addresses</source>
        <translation>アドレス受信中</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>These are your Nexa addresses for sending payments. Always check the amount and the receiving address before sending coins.</source>
        <translation>これらは支払いを送信するためのあなたの Nexa アドレスです。コインを送信する前に、常に額と受信アドレスを確認してください。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>These are your Nexa addresses for receiving payments. It is recommended to use a new receiving address for each transaction.</source>
        <translation>これらは支払いを受け取るためのビットコインアドレスです。トランザクションごとに新しい受け取り用アドレスを作成することが推奨されます。</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Copy &amp;Label</source>
        <translation>ラベルをコピー (&amp;L)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation>編集 (&amp;E)</translation>
    </message>
    <message>
        <location line="+173"/>
        <source>Export Address List</source>
        <translation>アドレス帳をエクスポート</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>CSVファイル (*.csv)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Exporting Failed</source>
        <translation>エクスポートに失敗しました</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the address list to %1. Please try again.</source>
        <translation>トランザクション履歴を %1 へ保存する際にエラーが発生しました。再試行してください。</translation>
    </message>
</context>
<context>
    <name>AddressTableModel</name>
    <message>
        <location filename="../addresstablemodel.cpp" line="+159"/>
        <source>Label</source>
        <translation>ラベル</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>アドレス</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>(no label)</source>
        <translation>（ラベル無し）</translation>
    </message>
</context>
<context>
    <name>AskPassphraseDialog</name>
    <message>
        <location filename="../forms/askpassphrasedialog.ui" line="+26"/>
        <source>Passphrase Dialog</source>
        <translation>パスフレーズ ダイアログ</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Enter passphrase</source>
        <translation>パスフレーズを入力</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>New passphrase</source>
        <translation>新しいパスフレーズ</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Repeat new passphrase</source>
        <translation>新しいパスフレーズをもう一度</translation>
    </message>
    <message>
        <location filename="../askpassphrasedialog.cpp" line="+47"/>
        <source>Encrypt wallet</source>
        <translation>ウォレットを暗号化する</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to unlock the wallet.</source>
        <translation>この操作はウォレットをアンロックするためにパスフレーズが必要です。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unlock wallet</source>
        <translation>ウォレットをアンロックする</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to decrypt the wallet.</source>
        <translation>この操作はウォレットの暗号化解除のためにパスフレーズが必要です。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Decrypt wallet</source>
        <translation>ウォレットの暗号化を解除する</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change passphrase</source>
        <translation>パスフレーズの変更</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Confirm wallet encryption</source>
        <translation>ウォレットの暗号化を確認する</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: If you encrypt your wallet and lose your passphrase, you will &lt;b&gt;LOSE ALL OF YOUR NEXA COINS&lt;/b&gt;!</source>
        <translation>警告: もしもあなたのウォレットを暗号化してパスフレーズを失ってしまったなら、&lt;b&gt;あなたの Nexa はすべて失われます&lt;/b&gt;!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Are you sure you wish to encrypt your wallet?</source>
        <translation>本当にウォレットを暗号化しますか?</translation>
    </message>
    <message>
        <location line="+119"/>
        <location line="+29"/>
        <source>Warning: The Caps Lock key is on!</source>
        <translation>警告: Caps Lock キーがオンになっています!</translation>
    </message>
    <message>
        <location line="-140"/>
        <location line="+61"/>
        <source>Wallet encrypted</source>
        <translation>ウォレットは暗号化されました</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Enter the new passphrase to the wallet.&lt;br/&gt;Please use a passphrase of &lt;b&gt;ten or more random characters&lt;/b&gt;, or &lt;b&gt;eight or more words&lt;/b&gt;.</source>
        <translation>ウォレットの新しいパスフレーズを入力してください。&lt;br/&gt;&lt;b&gt;10文字以上のランダムな文字&lt;/b&gt;で構成されたものか、&lt;b&gt;8単語以上の単語&lt;/b&gt;で構成されたパスフレーズを使用してください。</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Enter the old passphrase and new passphrase to the wallet.</source>
        <translation>ウォレットの古いパスフレーズおよび新しいパスフレーズを入力してください。</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>%1 will close now to finish the encryption process. Remember that encrypting your wallet cannot fully protect your coins from being stolen by malware infecting your computer.</source>
        <translation>暗号化プロセスを終了するため、%1 を閉じます。 ウォレットを暗号化しても、コンピューターに感染するマルウェアによるコインの盗難を完全に防ぐことはできないことに注意してください。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>IMPORTANT: Any previous backups you have made of your wallet file should be replaced with the newly generated, encrypted wallet file. For security reasons you should no longer keep previous unencrypted backups as your funds will be at risk if someone gains access to them.</source>
        <translation>重要: ウォレット ファイルに関して以前に作成したバックアップは、新しく生成された暗号化されたウォレット ファイルに置き換える必要があります。セキュリティ上の理由から、誰かがアクセスすると資金が危険にさらされるため、以前の暗号化されていないバックアップは保存しないでください。</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+8"/>
        <location line="+42"/>
        <location line="+6"/>
        <source>Wallet encryption failed</source>
        <translation>ウォレットの暗号化に失敗しました</translation>
    </message>
    <message>
        <location line="-55"/>
        <source>Wallet encryption failed due to an internal error. Your wallet was not encrypted.</source>
        <translation>内部エラーによりウォレットの暗号化が失敗しました。ウォレットは暗号化されませんでした。</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+48"/>
        <source>The supplied passphrases do not match.</source>
        <translation>パスフレーズが同じではありません。</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Wallet unlock failed</source>
        <translation>ウォレットのアンロックに失敗しました</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+11"/>
        <location line="+19"/>
        <source>The passphrase entered for the wallet decryption was incorrect.</source>
        <translation>ウォレットの暗号化解除のパスフレーズが正しくありません。</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Wallet decryption failed</source>
        <translation>ウォレットの暗号化解除に失敗しました</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Wallet passphrase was successfully changed.</source>
        <translation>ウォレットのパスフレーズの変更が成功しました。</translation>
    </message>
</context>
<context>
    <name>BanTableModel</name>
    <message>
        <location filename="../bantablemodel.cpp" line="+86"/>
        <source>IP/Netmask</source>
        <translation>IPアドレス/ネットマスク</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Banned Until</source>
        <translation>以下の時間までbanする:</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation>ユーザエージェント</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ban Reason</source>
        <translation>禁止理由</translation>
    </message>
</context>
<context>
    <name>BitcoinGUI</name>
    <message>
        <location filename="../nexagui.cpp" line="+351"/>
        <source>Sign &amp;message...</source>
        <translation>メッセージの署名... (&amp;m)</translation>
    </message>
    <message>
        <location line="+435"/>
        <source>Synchronizing with network...</source>
        <translation>ネットワークに同期中……</translation>
    </message>
    <message>
        <location line="-556"/>
        <source>&amp;Overview</source>
        <translation>概要(&amp;O)</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Node</source>
        <translation>ノード</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Show general overview of wallet</source>
        <translation>ウォレットの概要を見る</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Request payments (generates QR codes and %1: URIs)</source>
        <translation>支払いの要求 (QR コードと %1: URI を生成)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Transactions</source>
        <translation>取引(&amp;T)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse transaction history</source>
        <translation>取引履歴を閲覧</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Tokens</source>
        <translation>トークン</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse or Send Tokens</source>
        <translation>トークンの参照または送信</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Token History</source>
        <translation>トークン履歴</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse Token History</source>
        <translation>トークン履歴を参照する</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>E&amp;xit</source>
        <translation>終了(&amp;E)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit application</source>
        <translation>アプリケーションを終了</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;About %1</source>
        <translation>%1&amp;について</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about %1</source>
        <translation>%1 に関する情報を表示</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>About &amp;Qt</source>
        <translation>Qt について(&amp;Q)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about Qt</source>
        <translation>Qt の情報を表示</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Options...</source>
        <translation>オプション... (&amp;O)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify configuration options for %1</source>
        <translation>%1 の構成オプションを変更します</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Unlimited...</source>
        <translation>&amp;Unlimited...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify Bitcoin Unlimited Options</source>
        <translation>ビットコイン無制限オプションの変更</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Encrypt Wallet...</source>
        <translation>ウォレットの暗号化... (&amp;E)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Backup Wallet...</source>
        <translation>ウォレットのバックアップ... (&amp;B)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Restore Wallet...</source>
        <translation>ウォレットを復元...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Restore wallet from another location</source>
        <translation>別の場所からウォレットを復元する</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Change Passphrase...</source>
        <translation>パスフレーズの変更... (&amp;C)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sending addresses...</source>
        <translation>送金先アドレス一覧 (&amp;S)...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Receiving addresses...</source>
        <translation>受け取り用アドレス一覧 (&amp;R)...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open &amp;URI...</source>
        <translation>URI を開く (&amp;U)...</translation>
    </message>
    <message>
        <location line="+421"/>
        <source>Importing blocks from disk...</source>
        <translation>ディスクからブロックをインポートしています...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindexing blocks on disk...</source>
        <translation>ディスク上のブロックのインデックスを再作成中...</translation>
    </message>
    <message>
        <location line="-554"/>
        <source>Send coins to a Nexa address</source>
        <translation>Nexa アドレスにコインを送る</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Backup wallet to another location</source>
        <translation>ウォレットを他の場所にバックアップ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Change the passphrase used for wallet encryption</source>
        <translation>ウォレット暗号化用パスフレーズの変更</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Debug window</source>
        <translation>デバッグ ウインドウ (&amp;D)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open debugging and diagnostic console</source>
        <translation>デバッグと診断コンソールを開く</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>&amp;Verify message...</source>
        <translation>メッセージの検証... (&amp;V)</translation>
    </message>
    <message>
        <location line="+522"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="-785"/>
        <source>Wallet</source>
        <translation>ウォレット</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>&amp;Send</source>
        <translation>送金 (&amp;S)</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Receive</source>
        <translation>入金 (&amp;R)</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>&amp;Show / Hide</source>
        <translation>見る/隠す (&amp;S)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show or hide the main Window</source>
        <translation>メイン ウインドウを表示または非表示</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Encrypt the private keys that belong to your wallet</source>
        <translation>あなたのウォレットの秘密鍵を暗号化します</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Sign messages with your Nexa addresses to prove you own them</source>
        <translation>あなたが所有していることを証明するために、あなたの Nexa アドレスでメッセージに署名してください</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Verify messages to ensure they were signed with specified Nexa addresses</source>
        <translation>指定された Nexa アドレスで署名されたことを確認するためにメッセージを検証します</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>&amp;File</source>
        <translation>ファイル(&amp;F)</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&amp;Settings</source>
        <translation>設定(&amp;S)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Help</source>
        <translation>ヘルプ(&amp;H)</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Tabs toolbar</source>
        <translation>タブツールバー</translation>
    </message>
    <message>
        <location line="-95"/>
        <source>Show the list of used sending addresses and labels</source>
        <translation>使用済みの送金用アドレスとラベルの一覧を表示する</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show the list of used receiving addresses and labels</source>
        <translation>支払いを受け取るアドレスとラベルのリストを表示する</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Command-line options</source>
        <translation>コマンドラインオプション (&amp;C)</translation>
    </message>
    <message numerus="yes">
        <location line="+390"/>
        <source>%n active connection(s) to Nexa network</source>
        <translation>
            <numerusform>%n の Nexa ネットワークへのアクティブな接続</numerusform>
        </translation>
    </message>
    <message>
        <location line="+34"/>
        <source>No block source available...</source>
        <translation>利用可能なブロックがありません...</translation>
    </message>
    <message numerus="yes">
        <location line="+9"/>
        <source>Processed %n block(s) of transaction history.</source>
        <translation>
            <numerusform>トランザクション履歴の %n ブロックを処理しました。</numerusform>
        </translation>
    </message>
    <message>
        <location line="+27"/>
        <source>%1 behind</source>
        <translation>%1 遅延</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Last received block was generated %1 ago.</source>
        <translation>最後に受信されたブロックは %1 前に生成されました。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transactions after this will not yet be visible.</source>
        <translation>この後の取引はまだ表示されません。</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Information</source>
        <translation>情報</translation>
    </message>
    <message>
        <location line="-86"/>
        <source>Up to date</source>
        <translation>バージョンは最新です</translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Open a %1: URI or payment request</source>
        <translation>%1: URI または支払い要求を開く</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show the %1 help message to get a list with possible Nexa command-line options</source>
        <translation>%1 ヘルプ メッセージを表示して、可能な Nexa コマンドライン オプションのリストを取得します</translation>
    </message>
    <message>
        <location line="+194"/>
        <source>%1 client</source>
        <translation>%1 クライアント</translation>
    </message>
    <message>
        <location line="+268"/>
        <source>Catching up...</source>
        <translation>追跡中...</translation>
    </message>
    <message>
        <location line="+164"/>
        <source>Date: %1
</source>
        <translation>日付: %1
</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Amount: %1
</source>
        <translation>総額: %1
</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type: %1
</source>
        <translation>タイプ: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Label: %1
</source>
        <translation>ラベル: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address: %1
</source>
        <translation>アドレス: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent transaction</source>
        <translation>送金取引</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Incoming transaction</source>
        <translation>着金取引</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>HD key generation is &lt;b&gt;enabled&lt;/b&gt;</source>
        <translation>HD キー生成は&lt;b&gt;有効&lt;/b&gt;です</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>HD key generation is &lt;b&gt;disabled&lt;/b&gt;</source>
        <translation>HD キー生成は&lt;b&gt;無効&lt;/b&gt;です</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt;</source>
        <translation>ウォレットは&lt;b&gt;暗号化されて、アンロックされています&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;locked&lt;/b&gt;</source>
        <translation>ウォレットは&lt;b&gt;暗号化されて、ロックされています&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>CoinControlDialog</name>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="+14"/>
        <source>Coin Selection</source>
        <translation>コイン選択</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Quantity:</source>
        <translation>数量:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Bytes:</source>
        <translation>バイト:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Amount:</source>
        <translation>総額:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Priority:</source>
        <translation>優先度:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Fee:</source>
        <translation>手数料:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation>ダスト：</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation>手数料差引後:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>釣り銭:</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>(un)select max inputs</source>
        <translation>すべて選択/選択解除</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Tree mode</source>
        <translation>ツリーモード</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>List mode</source>
        <translation>リストモード</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Amount</source>
        <translation>総額</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with label</source>
        <translation>ラベルに対する入金一覧</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with address</source>
        <translation>アドレスに対する入金一覧</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Date</source>
        <translation>日付</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmations</source>
        <translation>検証数</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed</source>
        <translation>検証済み</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Priority</source>
        <translation>優先度</translation>
    </message>
    <message>
        <location filename="../coincontroldialog.cpp" line="+45"/>
        <source>Copy address</source>
        <translation>アドレスをコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>ラベルをコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+29"/>
        <source>Copy amount</source>
        <translation>総額のコピー</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Copy transaction ID</source>
        <translation>取引 ID をコピー</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lock unspent</source>
        <translation>未使用トランザクションをロックする</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unlock unspent</source>
        <translation>未使用トランザクションをアンロックする</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Copy quantity</source>
        <translation>数量をコピーする</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy fee</source>
        <translation>手数料をコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>手数料差引後の値をコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>バイト数をコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>優先度をコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation>ダストをコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>釣り銭をコピー</translation>
    </message>
    <message>
        <location line="+352"/>
        <source>highest</source>
        <translation>最高</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>higher</source>
        <translation>非常に高</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>high</source>
        <translation>高</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium-high</source>
        <translation>中〜高</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium</source>
        <translation>中</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low-medium</source>
        <translation>低〜中</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low</source>
        <translation>低</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lower</source>
        <translation>非常に低</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lowest</source>
        <translation>最低</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>(%1 locked)</source>
        <translation>(%1 がロック済み)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>none</source>
        <translation>なし</translation>
    </message>
    <message>
        <location line="+167"/>
        <source>This label turns red if the transaction size is greater than 1000 bytes.</source>
        <translation>トランザクションのサイズが1000バイトを超える場合にはこのラベルは赤色になります。</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>This label turns red if the priority is smaller than &quot;medium&quot;.</source>
        <translation>優先度が「中」未満の場合、このラベルは赤色になります。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>This label turns red if any recipient receives an amount smaller than %1.</source>
        <translation>受取人のうち誰かの受取額が %1 未満の場合にこのラベルは赤色になります。</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Can vary +/- %1 satoshi(s) per input.</source>
        <translation>ひとつの入力につき %1 satoshi 前後ずれることがあります。</translation>
    </message>
    <message>
        <location line="-41"/>
        <source>yes</source>
        <translation>はい</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>no</source>
        <translation>いいえ</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+8"/>
        <source>This means a fee of at least %1 per kB is required.</source>
        <translation>これは少なくとも1kBあたり %1 の手数料が必要であることを意味します。</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Can vary +/- 1 byte per input.</source>
        <translation>ひとつの入力につき1バイト程度ずれることがあります。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transactions with higher priority are more likely to get included into a block.</source>
        <translation>より高い優先度を持つトランザクションの方がブロックに取り込まれやすくなります。</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+64"/>
        <source>(no label)</source>
        <translation>（ラベル無し）</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>change from %1 (%2)</source>
        <translation>%1 (%2) からのおつり</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(change)</source>
        <translation>(おつり)</translation>
    </message>
</context>
<context>
    <name>EditAddressDialog</name>
    <message>
        <location filename="../forms/editaddressdialog.ui" line="+14"/>
        <source>Edit Address</source>
        <translation>アドレスの編集</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Label</source>
        <translation>ラベル(&amp;L)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The label associated with this address list entry</source>
        <translation>このアドレス帳項目に結びつけられているラベル</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>The address associated with this address list entry. This can only be modified for sending addresses.</source>
        <translation>このアドレス帳項目に結びつけられているアドレス。この項目は送金用アドレスの場合のみ編集することができます。</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>&amp;Address</source>
        <translation>アドレス帳 (&amp;A)</translation>
    </message>
    <message>
        <location filename="../editaddressdialog.cpp" line="+25"/>
        <source>New receiving address</source>
        <translation>新しい入金アドレス</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New sending address</source>
        <translation>新しい送信アドレス</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit receiving address</source>
        <translation>入金アドレスを編集</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit sending address</source>
        <translation>送信アドレスを編集</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>The entered address &quot;%1&quot; is already in the address book.</source>
        <translation>入力されたアドレス &quot;%1&quot; は既にアドレス帳にあります。</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>The entered address &quot;%1&quot; is not a valid Nexa address.</source>
        <translation>入力されたアドレス &quot;%1&quot; は無効な Nexa アドレスです。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not unlock wallet.</source>
        <translation>ウォレットをアンロックできませんでした。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New key generation failed.</source>
        <translation>新しいキーの生成に失敗しました。</translation>
    </message>
</context>
<context>
    <name>EditTokenDialog</name>
    <message>
        <location filename="../forms/edittokendialog.ui" line="+14"/>
        <source>Edit Token</source>
        <translation>トークンを編集</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Group ID</source>
        <translation>グループID</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The group ID of the token to be edited.</source>
        <translation>編集するトークンのグループ ID。</translation>
    </message>
    <message>
        <location filename="../edittokendialog.cpp" line="+25"/>
        <source>Add new token</source>
        <translation>新しいトークンを追加</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove existing token</source>
        <translation>既存のトークンを削除する</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Token Dialog Error! Please notify a developer</source>
        <translation>トークンダイアログエラー! 開発者に通知してください</translation>
    </message>
    <message>
        <location line="+23"/>
        <location line="+20"/>
        <source>Group ID required.</source>
        <translation>グループIDが必要です。</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Failed to add token: Invalid Group ID.</source>
        <translation>トークンの追加に失敗しました: 無効なグループ ID。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Failed to add token.</source>
        <translation>トークンの追加に失敗しました。</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Failed to remove token.</source>
        <translation>トークンの削除に失敗しました。</translation>
    </message>
</context>
<context>
    <name>FreespaceChecker</name>
    <message>
        <location filename="../intro.cpp" line="+75"/>
        <source>A new data directory will be created.</source>
        <translation>新しいデータ ディレクトリが作成されます。</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>name</source>
        <translation>name</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Directory already exists. Add %1 if you intend to create a new directory here.</source>
        <translation>ディレクトリがもうあります。 新しいのディレクトリを作るつもりなら%1を書いてください。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Path already exists, and is not a directory.</source>
        <translation>パスが存在しますがディレクトリではありません。</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot create data directory here.</source>
        <translation>ここにデータ ディレクトリを作成することはできません。</translation>
    </message>
</context>
<context>
    <name>HelpMessageDialog</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+39"/>
        <source>version</source>
        <translation>バージョン</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+2"/>
        <source>(%1-bit)</source>
        <translation>(%1ビット)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>About %1</source>
        <translation>%1 について</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Command-line options</source>
        <translation>コマンドライン オプション</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Usage:</source>
        <translation>使用法:</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>command-line options</source>
        <translation>コマンドライン オプション</translation>
    </message>
</context>
<context>
    <name>Intro</name>
    <message>
        <location filename="../forms/intro.ui" line="+14"/>
        <source>Welcome</source>
        <translation>ようこそ</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Welcome to %1.</source>
        <translation>%1 へようこそ。</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>As this is the first time the program is launched, you can choose where %1 will store its data.</source>
        <translation>プログラムを初めて起動するので、%1 がデータを保存する場所を選択できます。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 will download and store a copy of the Nexa block chain. At least %2GB of data will be stored in this directory, and it will grow over time. The wallet will also be stored in this directory.</source>
        <translation>%1 は Nexa ブロック チェーンのコピーをダウンロードして保存します。 このディレクトリには少なくとも %2GB のデータが保存され、時間の経過とともに大きくなります。 ウォレットもこのディレクトリに格納されます。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Use the default data directory</source>
        <translation>初期値のデータ ディレクトリを使用</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use a custom data directory:</source>
        <translation>任意のデータ ディレクトリを使用:</translation>
    </message>
    <message>
        <location filename="../intro.cpp" line="+81"/>
        <source>Error: Specified data directory &quot;%1&quot; cannot be created.</source>
        <translation>エラー: 指定のデータディレクトリ &quot;%1&quot; を作成できません。</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%n GB of free space available</source>
        <translation>
            <numerusform>%n GBの空き容量が利用可能</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>(of %n GB needed)</source>
        <translation>
            <numerusform>(%n GB必要)</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessThanValidator</name>
    <message>
        <location filename="../unlimiteddialog.cpp" line="+486"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>アップストリーム トラフィック シェーピング パラメータを空白にすることはできません</translation>
    </message>
</context>
<context>
    <name>ModalOverlay</name>
    <message>
        <location filename="../forms/modaloverlay.ui" line="+14"/>
        <source>Form</source>
        <translation>フォーム</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet. This means that recent transactions will not be visible, and the balance will not be up-to-date until this process has completed.</source>
        <translation>表示されている情報が古い可能性があります。 接続が確立されると、ウォレットは自動的に Nexa ネットワークと同期しますが、このプロセスはまだ完了していません。 これは、最近の取引が表示されず、このプロセスが完了するまで残高が最新でないことを意味します。</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Spending coins may not be possible during that phase!</source>
        <translation>そのフェイズ中はコインを使うことができないかもしれません！</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Amount of blocks left</source>
        <translation>残りのブロック数</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+26"/>
        <source>unknown...</source>
        <translation>知らない...</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Last block time</source>
        <translation>最終ブロックの日時</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Progress</source>
        <translation>進捗</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>~</source>
        <translation>~</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Progress increase per Hour</source>
        <translation>1時間あたりの進捗増加</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+20"/>
        <source>calculating...</source>
        <translation>計算中...</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Estimated time left until synced</source>
        <translation>同期までの推定残り時間</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Hide</source>
        <translation>隠す</translation>
    </message>
    <message>
        <location filename="../modaloverlay.cpp" line="+140"/>
        <source>Unknown. Reindexing (%1)...</source>
        <translation>知らない。 再インデックス中 (%1)...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Reindexing...</source>
        <translation>知らない。 再インデックス中...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown. Syncing Headers (%1)...</source>
        <translation>知らない。 ヘッダー (%1) を同期しています...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Syncing Headers...</source>
        <translation>知らない。 ヘッダー を同期しています...</translation>
    </message>
</context>
<context>
    <name>OpenURIDialog</name>
    <message>
        <location filename="../forms/openuridialog.ui" line="+14"/>
        <source>Open URI</source>
        <translation>URI を開く</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open payment request from URI or file</source>
        <translation>URI またはファイルから支払いリクエストを開く</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>URI:</source>
        <translation>URI:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Select payment request file</source>
        <translation>支払いリクエストファイルを選択してください</translation>
    </message>
    <message>
        <location filename="../openuridialog.cpp" line="+40"/>
        <source>Select payment request file to open</source>
        <translation>開きたい支払いリクエストファイルを選択してください</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../forms/optionsdialog.ui" line="+14"/>
        <source>Options</source>
        <translation>設定</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Main</source>
        <translation>メイン (&amp;M)</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Number of script &amp;verification threads</source>
        <translation>スクリプト検証用スレッド数 (&amp;V)</translation>
    </message>
    <message>
        <location line="+227"/>
        <source>Allow incoming connections</source>
        <translation>外部からの接続を許可する</translation>
    </message>
    <message>
        <location line="+44"/>
        <location line="+187"/>
        <source>IP address of the proxy (e.g. IPv4: 127.0.0.1 / IPv6: ::1)</source>
        <translation>プロキシのIPアドレス (例えば IPv4: 127.0.0.1 / IPv6: ::1)</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Minimize instead of exit the application when the window is closed. When this option is enabled, the application will be closed only after selecting Exit in the menu.</source>
        <translation>ウィンドウを閉じる際にアプリケーションを終了するのではなく、最小化します。このオプションが有効化された場合、メニューから終了を選択した場合にのみアプリケーションは閉じられます。</translation>
    </message>
    <message>
        <location line="+80"/>
        <location line="+13"/>
        <source>Third party URLs (e.g. a block explorer) that appear in the transactions tab as context menu items. %s in the URL is replaced by transaction hash. Multiple URLs are separated by vertical bar |.</source>
        <translation>トランザクションタブのコンテキストメニュー項目に表示する、サードパーティURL (例えばブロックエクスプローラ)。URL中の%sはトランザクションのハッシュ値に置き換えられます。垂直バー | で区切ることで、複数のURLを指定できます。</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Third party transaction URLs</source>
        <translation>サードパーティのトランザクションURL</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Active command-line options that override above options:</source>
        <translation>上のオプションを置き換えることのできる、有効なコマンドラインオプションの一覧:</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Reset all client options to default.</source>
        <translation>すべてのオプションを初期値に戻します。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>オプションをリセット (&amp;R)</translation>
    </message>
    <message>
        <location line="-504"/>
        <source>&amp;Network</source>
        <translation>ネットワーク (&amp;N)</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>(0 = auto, &lt;0 = leave that many cores free)</source>
        <translation>(0 = 自動、0以上 = 指定した数のコアをフリーにする)</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>W&amp;allet</source>
        <translation>ウォレット (&amp;A)</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Expert</source>
        <translation>エクスポート</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Enable coin &amp;control features</source>
        <translation>コインコントロール機能を有効化する (&amp;C)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>If you disable the spending of unconfirmed change, the change from a transaction cannot be used until that transaction has at least one confirmation. This also affects how your balance is computed.</source>
        <translation>未検証のおつりの使用を無効化すると、トランザクションが少なくとも1検証を獲得するまではそのトランザクションのおつりは利用できなくなります。これは残高の計算方法にも影響します。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Spend unconfirmed change</source>
        <translation>未検証のおつりを使用する (&amp;S)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Instant Transactions is enabled you can spend unconfirmed transactions immediately.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;インスタント トランザクションが有効になっている場合、未確認のトランザクションをすぐに使用できます。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Instant Transactions</source>
        <translation>&amp;即時取引</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When creating and sending transactions, auto consolidate will, if required, automatically create a chain of transactions which have inputs no greater than the consensus input limit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;トランザクションを作成して送信するとき、必要に応じて、コンセンサス入力制限以下の入力を持つ一連のトランザクションが自動統合によって自動的に作成されます。&lt;/p&gt;&lt;/body &gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Auto Consolidate</source>
        <translation>自動統合</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>&amp;Rescan wallet on startup</source>
        <translation>起動時にウォレットを再スキャンする</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Token Whitelist is enabled you can control what tokens appear in your wallet.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;トークン ホワイトリストを有効にすると、ウォレットに表示されるトークンを制御できます。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Token Whitelist</source>
        <translation>トークンホワイトリスト</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Automatically open the Nexa client port on the router. This only works when your router supports UPnP and it is enabled.</source>
        <translation>自動的にルーター上の Nexa クライアントのポートを開きます。あなたのルーターが UPnP に対応していて、それが有効になっている場合に作動します。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Map port using &amp;UPnP</source>
        <translation>UPnP を使ってポートを割り当てる (&amp;U)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Accept connections from outside.</source>
        <translation>外部からの接続を受け入れます。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Connect to the Nexa network through a SOCKS5 proxy.</source>
        <translation>SOCKS5 プロキシ経由でNexaネットワークに接続する</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Connect through SOCKS5 proxy (default proxy):</source>
        <translation>SOCKS5 プロキシ経由で接続する (デフォルトプロキシ): (&amp;C)</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+187"/>
        <source>Proxy &amp;IP:</source>
        <translation>プロキシの IP (&amp;I) :</translation>
    </message>
    <message>
        <location line="-155"/>
        <location line="+187"/>
        <source>&amp;Port:</source>
        <translation>ポート (&amp;P) :</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>Port of the proxy (e.g. 9050)</source>
        <translation>プロキシのポート番号 (例 9050)</translation>
    </message>
    <message>
        <location line="-163"/>
        <source>Used for reaching peers via:</source>
        <translation>ピアへ到達するために使われた方法:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tor</source>
        <translation>Tor</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Connect to the Nexa network through a separate SOCKS5 proxy for Tor hidden services.</source>
        <translation>Tor秘匿サービスを利用するため、独立なSOCKS5プロキシ経由でNexaネットワークに接続する</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Use separate SOCKS5 proxy to reach peers via Tor hidden services:</source>
        <translation>Tor秘匿サービス経由でピアに到達するため、独立なSOCKS5プロキシを利用する:</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>&amp;Window</source>
        <translation>ウインドウ (&amp;W)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show only a tray icon after minimizing the window.</source>
        <translation>ウインドウを最小化したあとトレイ アイコンだけを表示する。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Minimize to the tray instead of the taskbar</source>
        <translation>タスクバーの代わりにトレイに最小化 (&amp;M)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>M&amp;inimize on close</source>
        <translation>閉じる時に最小化 (&amp;i)</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Display</source>
        <translation>表示 (&amp;D)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>User Interface &amp;language:</source>
        <translation>ユーザインターフェースの言語 (&amp;l) :</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The user interface language can be set here. This setting will take effect after restarting %1.</source>
        <translation>ユーザー インターフェイス言語はここで設定できます。 この設定は、%1 の再起動後に有効になります。</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Unit to show amounts in:</source>
        <translation>額を表示する単位 (&amp;U) :</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Choose the default subdivision unit to show in the interface and when sending coins.</source>
        <translation>インターフェース上の表示とコインの送信で使用する単位を選択します。</translation>
    </message>
    <message>
        <location line="-518"/>
        <source>Whether to show coin control features or not.</source>
        <translation>コインコントロール機能を表示するかどうか。</translation>
    </message>
    <message>
        <location line="-137"/>
        <source>Automatically start %1 after logging in to the system.</source>
        <translation>システムにログインした後、%1 を自動的に起動します。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Start %1 on system login</source>
        <translation>システム ログイン時に %1 を開始(&amp;S)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a, one time only, full database reindex on the next startup.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;次回の起動時に、一度だけ、完全なデータベースの再インデックスを自動的に開始します。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindex on startup</source>
        <translation>起動時に再インデックス</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a full blockchain resynchronization on the next startup (one time only).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;次回の起動時に完全なブロックチェーンの再同期を自動的に開始します (1 回のみ)。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Resynchronize block data on startup</source>
        <translation>起動時にブロックデータを再同期する</translation>
    </message>
    <message>
        <location line="+277"/>
        <location line="+23"/>
        <location line="+23"/>
        <source>Shows if the supplied default SOCKS5 proxy is used to reach peers via this network type.</source>
        <translation>指定されたデフォルトの SOCKS5 プロキシを使用して、このネットワーク タイプを介してピアに到達するかどうかを示します。</translation>
    </message>
    <message>
        <location line="+387"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>キャンセル (&amp;C)</translation>
    </message>
    <message>
        <location filename="../optionsdialog.cpp" line="+135"/>
        <source>default</source>
        <translation>初期値</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>none</source>
        <translation>なし</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Confirm options reset</source>
        <translation>オプションのリセットの確認</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+27"/>
        <source>Client restart required to activate changes.</source>
        <translation>変更を有効化するにはクライアントを再起動する必要があります。</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>クライアントを終了します。続行してもよろしいですか？</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>This change would require a client restart.</source>
        <translation>この変更はクライアントの再起動が必要です。</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>The supplied proxy address is invalid.</source>
        <translation>プロキシアドレスが無効です。</translation>
    </message>
</context>
<context>
    <name>OverviewPage</name>
    <message>
        <location filename="../forms/overviewpage.ui" line="+14"/>
        <source>Form</source>
        <translation>フォーム</translation>
    </message>
    <message>
        <location line="+59"/>
        <location line="+386"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet.</source>
        <translation>表示された情報は古いかもしれません。接続が確立されると、あなたのウォレットは Nexa ネットワークと自動的に同期しますが、このプロセスはまだ完了していません。</translation>
    </message>
    <message>
        <location line="-139"/>
        <source>Watch-only:</source>
        <translation>監視限定:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Available:</source>
        <translation>利用可能:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Your current spendable balance</source>
        <translation>あなたの利用可能残高</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Pending:</source>
        <translation>検証待ち:</translation>
    </message>
    <message>
        <location line="-236"/>
        <source>Total of transactions that have yet to be confirmed, and do not yet count toward the spendable balance</source>
        <translation>未検証の取引で利用可能残高に反映されていない数</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Immature:</source>
        <translation>未完成:</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Mined balance that has not yet matured</source>
        <translation>完成していない採掘された残高</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Balances</source>
        <translation>残高</translation>
    </message>
    <message>
        <location line="+161"/>
        <source>Total:</source>
        <translation>合計:</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Your current total balance</source>
        <translation>あなたの現在の残高</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Your current balance in watch-only addresses</source>
        <translation>監視限定アドレス内の現在の残高</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Spendable:</source>
        <translation>使用可能:</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Recent transactions</source>
        <translation>最近のトランザクション</translation>
    </message>
    <message>
        <location line="-317"/>
        <source>Unconfirmed transactions to watch-only addresses</source>
        <translation>監視限定アドレスに対する未検証のトランザクション</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Mined balance in watch-only addresses that has not yet matured</source>
        <translation>ウォッチオンリーアドレスの採掘された残高のうち、成熟していないもの</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Current total balance in watch-only addresses</source>
        <translation>監視限定アドレス内の現在の全残高</translation>
    </message>
</context>
<context>
    <name>PaymentServer</name>
    <message>
        <location filename="../paymentserver.cpp" line="+470"/>
        <location line="+13"/>
        <location line="+10"/>
        <source>URI handling</source>
        <translation>URI の操作</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Invalid payment address %1</source>
        <translation>支払いのアドレス「%1」は無効です</translation>
    </message>
    <message>
        <location line="+113"/>
        <location line="+11"/>
        <location line="+35"/>
        <location line="+12"/>
        <location line="+21"/>
        <location line="+98"/>
        <source>Payment request rejected</source>
        <translation>支払い要求は拒否されました</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Payment request network doesn&apos;t match client network.</source>
        <translation>支払いリクエストのネットワークは現在のクライアントのネットワークに一致しません。</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Payment request is not initialized.</source>
        <translation>支払いリクエストは開始されていません。</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Requested payment amount of %1 is too small (considered dust).</source>
        <translation>要求された支払額 %1 は少なすぎます (ダストとみなされてしまいます)。</translation>
    </message>
    <message>
        <location line="-288"/>
        <location line="+240"/>
        <location line="+47"/>
        <location line="+126"/>
        <location line="+15"/>
        <location line="+15"/>
        <source>Payment request error</source>
        <translation>支払いのリクエストのエラーです</translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Cannot start click-to-pay handler</source>
        <translation>Click-to-Pay ハンドラを開始できません</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Payment request fetch URL is invalid: %1</source>
        <translation>支払い要求の取得先URLが無効です: %1</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>URI cannot be parsed! This can be caused by an invalid Nexa address or malformed URI parameters.</source>
        <translation>URI を解析できません! これは無効な Nexa アドレスあるいや不正な形式の URI パラメーターによって引き起こされる場合があります。</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Payment request file handling</source>
        <translation>支払いリクエストファイルを処理しています</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Payment request file cannot be read! This can be caused by an invalid payment request file.</source>
        <translation>支払いリクエストファイルを読み込めませんでした！無効な支払いリクエストファイルにより引き起こされた可能性があります。</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Payment request expired.</source>
        <translation>支払いリクエストの期限が切れました。</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Unverified payment requests to custom payment scripts are unsupported.</source>
        <translation>カスタム支払いスクリプトに対する、検証されていない支払いリクエストはサポートされていません。</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+21"/>
        <source>Invalid payment request.</source>
        <translation>無効な支払いリクエスト。</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Refund from %1</source>
        <translation>%1 からの返金</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Payment request %1 is too large (%2 bytes, allowed %3 bytes).</source>
        <translation>支払リクエスト %1 は大きすぎます（%2バイトですが、%3バイトまでが許されています）。</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error communicating with %1: %2</source>
        <translation>%1: %2とコミュニケーション・エラーです</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Payment request cannot be parsed!</source>
        <translation>支払リクエストを読み込めませんでした！</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Bad response from server %1</source>
        <translation>サーバーの返事は無効 %1</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Payment acknowledged</source>
        <translation>支払いは確認しました</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Network request error</source>
        <translation>ネットワーク・リクエストのエラーです</translation>
    </message>
</context>
<context>
    <name>PeerTableModel</name>
    <message>
        <location filename="../peertablemodel.cpp" line="+120"/>
        <source>User Agent</source>
        <translation>ユーザエージェント</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Node/Service</source>
        <translation>ノード・サービス</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ping Time</source>
        <translation>Ping時間</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../nexaunits.cpp" line="+187"/>
        <source>Amount</source>
        <translation>総額</translation>
    </message>
    <message>
        <location filename="../guiutil.cpp" line="+135"/>
        <source>Enter a NEXA address (e.g. %1)</source>
        <translation>NEXA アドレスを入力してください (例: %1)</translation>
    </message>
    <message>
        <location line="+831"/>
        <source>%1 d</source>
        <translation>%1日</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 h</source>
        <translation>%1 h</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 m</source>
        <translation>%1 m</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+68"/>
        <source>%1 s</source>
        <translation>%1秒</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>None</source>
        <translation>なし</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 ms</source>
        <translation>%1ミリ秒</translation>
    </message>
    <message numerus="yes">
        <location line="+19"/>
        <source>%n seconds(s)</source>
        <translation>
            <numerusform>%n 秒</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n minutes(s)</source>
        <translation>
            <numerusform>%n 分</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n hour(s)</source>
        <translation>
            <numerusform>%n 時間</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n day(s)</source>
        <translation>
            <numerusform>%n 日</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <location line="+8"/>
        <source>%n week(s)</source>
        <translation>
            <numerusform>%n 週間</numerusform>
        </translation>
    </message>
    <message>
        <location line="-2"/>
        <source>%1 and %2</source>
        <translation>%1 と %2</translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%n year(s)</source>
        <translation>
            <numerusform>%n 年</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>QRImageWidget</name>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+36"/>
        <source>&amp;Save Image...</source>
        <translation>画像を保存(&amp;S)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy Image</source>
        <translation>画像をコピー(&amp;C)</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Save QR Code</source>
        <translation>QR コードの保存</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>PNG Image (*.png)</source>
        <translation>PNG画像ファイル(*.png)</translation>
    </message>
</context>
<context>
    <name>RPCConsole</name>
    <message>
        <location filename="../forms/debugwindow.ui" line="+239"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+17"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+19"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+1753"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+23"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location line="-2715"/>
        <source>Client version</source>
        <translation>クライアントのバージョン</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>&amp;Information</source>
        <translation>情報 (&amp;I)</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Debug window</source>
        <translation>デバッグ ウインドウ</translation>
    </message>
    <message>
        <location line="+620"/>
        <source>General</source>
        <translation>一般</translation>
    </message>
    <message>
        <location line="-587"/>
        <source>Using BerkeleyDB version</source>
        <translation>使用中のBerkleyDBバージョン</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Startup time</source>
        <translation>起動した日時</translation>
    </message>
    <message>
        <location line="+474"/>
        <source>Network</source>
        <translation>ネットワーク</translation>
    </message>
    <message>
        <location line="-467"/>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Number of connections</source>
        <translation>接続数</translation>
    </message>
    <message>
        <location line="+415"/>
        <source>Block chain</source>
        <translation>ブロック チェーン</translation>
    </message>
    <message>
        <location line="-408"/>
        <source>Current number of blocks</source>
        <translation>現在のブロック数</translation>
    </message>
    <message>
        <location line="+884"/>
        <location line="+1694"/>
        <source>Received</source>
        <translation>受取</translation>
    </message>
    <message>
        <location line="-1614"/>
        <location line="+1591"/>
        <source>Sent</source>
        <translation>送金</translation>
    </message>
    <message>
        <location line="-417"/>
        <source>&amp;Peers</source>
        <translation>ピア (&amp;P)</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Banned peers</source>
        <translation>Banされたピア</translation>
    </message>
    <message>
        <location line="+57"/>
        <location filename="../rpcconsole.cpp" line="+311"/>
        <location line="+856"/>
        <source>Select a peer to view detailed information.</source>
        <translation>詳しい情報を見たいピアを選択してください。</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Whitelisted</source>
        <translation>ホワイトリスト</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Direction</source>
        <translation>方向</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Version</source>
        <translation>バージョン</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Starting Block</source>
        <translation>開始ブロック</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Headers</source>
        <translation>同期済みヘッダ</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Blocks</source>
        <translation>同期済みブロック</translation>
    </message>
    <message>
        <location line="-2488"/>
        <location line="+2390"/>
        <source>User Agent</source>
        <translation>ユーザエージェント</translation>
    </message>
    <message>
        <location line="-2370"/>
        <source>Datadir</source>
        <translation>データディレクトリ</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Last block time (time since)</source>
        <translation>最終ブロック時刻 (からの時間)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Last block size</source>
        <translation>最後のブロック サイズ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Tx pool</source>
        <translation>Tx プール内のトランザクション</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Orphan pool</source>
        <translation>孤立プール内のトランザクション</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Messages in CAPD pool</source>
        <translation>CAPD プール内のメッセージ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - usage</source>
        <translation>Tx プール - 使用量</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - txns per second</source>
        <translation>Tx プール - 1 秒あたりの送信数</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (Totals)</source>
        <translation>XThin (合計)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (24-Hour Averages)</source>
        <translation>XThin (24 時間平均)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (Totals)</source>
        <translation>コンパクト（合計）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (24-Hour Averages)</source>
        <translation>コンパクト（24時間平均）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (Totals)</source>
        <translation>グラフェン (合計)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (24-Hour Averages)</source>
        <translation>グラフェン (24 時間平均)</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Open the %1 debug log file from the current data directory. This can take a few seconds for large log files.</source>
        <translation>現在のデータ ディレクトリから %1 デバッグ ログ ファイルを開きます。 大きなログ ファイルの場合、これには数秒かかることがあります。</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Block Propagation</source>
        <translation>ブロックの伝播</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Transaction Pools</source>
        <translation>トランザクション プール</translation>
    </message>
    <message>
        <location line="+246"/>
        <source>Decrease font size</source>
        <translation>フォントサイズを小さくする</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Increase font size</source>
        <translation>フォントサイズを大きくする</translation>
    </message>
    <message>
        <location line="+380"/>
        <source>&amp;Transaction Rate</source>
        <translation>&amp;取引率</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Instantaneous Rate (1s)</source>
        <translation>瞬時レート (1 秒)</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+536"/>
        <source>Peak</source>
        <translation>最高</translation>
    </message>
    <message>
        <location line="-463"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Runtime</source>
        <translation>ランタイム</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>24-Hours</source>
        <translation>24時間</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Displayed</source>
        <translation>表示された</translation>
    </message>
    <message>
        <location line="-769"/>
        <location line="+536"/>
        <source>Average</source>
        <translation>平均</translation>
    </message>
    <message>
        <location line="-265"/>
        <source>Smoothed Rate (60s)</source>
        <translation>平滑化率 (60s)</translation>
    </message>
    <message>
        <location line="+769"/>
        <source>Services</source>
        <translation>サービス</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Ban Score</source>
        <translation>Banスコア</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Connection Time</source>
        <translation>接続時間</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Send</source>
        <translation>最終送信</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Receive</source>
        <translation>最終受信</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Ping Time</source>
        <translation>Ping時間</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>The duration of a currently outstanding ping.</source>
        <translation>現在実行中のpingにかかっている時間。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ping Wait</source>
        <translation>Ping待ち</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Time Offset</source>
        <translation>時間オフセット</translation>
    </message>
    <message>
        <location line="-2524"/>
        <source>&amp;Open</source>
        <translation>開く (&amp;O)</translation>
    </message>
    <message>
        <location line="+431"/>
        <source>&amp;Console</source>
        <translation>コンソール (&amp;C)</translation>
    </message>
    <message>
        <location line="+195"/>
        <source>&amp;Network Traffic</source>
        <translation>ネットワーク (&amp;N)</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>&amp;Clear</source>
        <translation>クリア(&amp;C)</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Totals</source>
        <translation>合計</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-530"/>
        <source>In:</source>
        <translation>入力:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Out:</source>
        <translation>出力:</translation>
    </message>
    <message>
        <location filename="../forms/debugwindow.ui" line="-704"/>
        <source>Debug log file</source>
        <translation>デバッグ用ログファイル</translation>
    </message>
    <message>
        <location line="+541"/>
        <source>Clear console</source>
        <translation>コンソールをクリア</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-207"/>
        <source>&amp;Disconnect Node</source>
        <translation>ノードを切断する (&amp;D)</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Ban Node for</source>
        <translation>ノードをbanする:</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>1 &amp;hour</source>
        <translation>1時間 (&amp;H)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;day</source>
        <translation>1日 (&amp;D)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;week</source>
        <translation>1週間 (&amp;W)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;year</source>
        <translation>1年 (&amp;Y)</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>&amp;Unban Node</source>
        <translation>ノードのbanを解除する (&amp;U)</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Welcome to the %1 RPC console.</source>
        <translation>%1 RPC コンソールへようこそ。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Use up and down arrows to navigate history, and &lt;b&gt;Ctrl-L&lt;/b&gt; to clear screen.</source>
        <translation>上下の矢印で履歴をたどれます。 &lt;b&gt;Ctrl-L&lt;/b&gt; でスクリーンを消去できます。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type &lt;b&gt;help&lt;/b&gt; for an overview of available commands.</source>
        <translation>使用可能なコマンドを見るには &lt;b&gt;help&lt;/b&gt; と入力します。</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <source>Disabled</source>
        <translation>無効</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>%1 B</source>
        <translation>%1 B</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>(node id: %1)</source>
        <translation>(ノードID: %1)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>via %1</source>
        <translation>%1経由</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>never</source>
        <translation>一度もなし</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Inbound</source>
        <translation>内向き</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Outbound</source>
        <translation>外向き</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>はい</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>いいえ</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+6"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
</context>
<context>
    <name>ReceiveCoinsDialog</name>
    <message>
        <location filename="../forms/receivecoinsdialog.ui" line="+166"/>
        <source>&amp;Amount:</source>
        <translation>総額:(&amp;A)</translation>
    </message>
    <message>
        <location line="-113"/>
        <source>&amp;Label:</source>
        <translation>ラベル(&amp;L):</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>&amp;Message:</source>
        <translation>メッセージ (&amp;M):</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Reuse one of the previously used receiving addresses. Reusing addresses has security and privacy issues. Do not use this unless re-generating a payment request made before.</source>
        <translation>以前利用した受取用アドレスのどれかを再利用します。アドレスの再利用はセキュリティおよびプライバシーにおいて問題があります。以前作成した支払リクエストを再生成するとき以外は利用しないでください。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>R&amp;euse an existing receiving address (not recommended)</source>
        <translation>既存の受取用アドレスを再利用する (非推奨) (&amp;E)</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+110"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened. Note: The message will not be sent with the payment over the Nexa network.</source>
        <translation>支払リクエストが開始された時に表示される、支払リクエストに添える任意のメッセージです。注意：メッセージはNexaネットワークを通じて、支払と共に送られるわけではありません。</translation>
    </message>
    <message>
        <location line="-136"/>
        <location line="+106"/>
        <source>An optional label to associate with the new receiving address.</source>
        <translation>受取用アドレスに紐づく任意のラベル。</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Use this form to request payments. All fields are &lt;b&gt;optional&lt;/b&gt;.</source>
        <translation>このフォームを使用して支払のリクエストを行いましょう。すべての項目は&lt;b&gt;任意入力&lt;/b&gt;です。</translation>
    </message>
    <message>
        <location line="-136"/>
        <location line="+120"/>
        <source>An optional amount to request. Leave this empty or zero to not request a specific amount.</source>
        <translation>リクエストする任意の金額。特定の金額をリクエストするのでない場合には、この欄は空白のままかゼロにしてください。</translation>
    </message>
    <message>
        <location line="-46"/>
        <source>Clear all fields of the form.</source>
        <translation>全ての入力項目をクリア</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>クリア</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Requested payments history</source>
        <translation>支払リクエスト履歴</translation>
    </message>
    <message>
        <location line="-132"/>
        <source>&amp;Request payment</source>
        <translation>支払をリクエストする (&amp;R)</translation>
    </message>
    <message>
        <location line="+157"/>
        <source>Show the selected request (does the same as double clicking an entry)</source>
        <translation>選択されたリクエストを表示する（項目をダブルクリックすることでも表示できます）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show</source>
        <translation>表示</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Remove the selected entries from the list</source>
        <translation>リストから選択項目を削除</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove</source>
        <translation>削除</translation>
    </message>
    <message>
        <location filename="../receivecoinsdialog.cpp" line="+50"/>
        <source>Copy URI</source>
        <translation>URIをコピー</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>ラベルをコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy message</source>
        <translation>メッセージをコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>総額のコピー</translation>
    </message>
</context>
<context>
    <name>ReceiveRequestDialog</name>
    <message>
        <location filename="../forms/receiverequestdialog.ui" line="+29"/>
        <source>QR Code</source>
        <translation>QR コード</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Copy &amp;URI</source>
        <translation>URI をコピーする (&amp;U)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Copy &amp;Address</source>
        <translation>アドレスをコピーする (&amp;A)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Save Image...</source>
        <translation>画像を保存(&amp;S)</translation>
    </message>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+78"/>
        <source>Request payment to %1</source>
        <translation>%1 への支払いリクエストを行う</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment information</source>
        <translation>支払い情報</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address</source>
        <translation>アドレス</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount</source>
        <translation>総額</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Label</source>
        <translation>ラベル</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Message</source>
        <translation>メッセージ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Resulting URI too long, try to reduce the text for label / message.</source>
        <translation>URI が長くなり過ぎます。ラベルやメッセージのテキストを短くしてください。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error encoding URI into QR Code.</source>
        <translation>QR コード用の URI エンコードでエラー。</translation>
    </message>
</context>
<context>
    <name>RecentRequestsTableModel</name>
    <message>
        <location filename="../recentrequeststablemodel.cpp" line="+30"/>
        <source>Date</source>
        <translation>日付</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Label</source>
        <translation>ラベル</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message</source>
        <translation>メッセージ</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Amount</source>
        <translation>総額</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>(no label)</source>
        <translation>（ラベル無し）</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>(no message)</source>
        <translation>（メッセージなし）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>(no amount)</source>
        <translation>（金額なし）</translation>
    </message>
</context>
<context>
    <name>SendCoinsDialog</name>
    <message>
        <location filename="../forms/sendcoinsdialog.ui" line="+14"/>
        <location filename="../sendcoinsdialog.cpp" line="+626"/>
        <source>Send Coins</source>
        <translation>コインを送る</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Coin Control Features</source>
        <translation>コインコントロール機能</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Select specific coins you want to use in this transaction</source>
        <translation>この取引で使用したい特定のコインを選択してください</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Inputs...</source>
        <translation>入力...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>automatically selected</source>
        <translation>自動選択</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Insufficient funds!</source>
        <translation>残高不足です！</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Quantity:</source>
        <translation>数量:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Bytes:</source>
        <translation>バイト:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Amount:</source>
        <translation>総額:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Priority:</source>
        <translation>優先度:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Fee:</source>
        <translation>手数料:</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>After Fee:</source>
        <translation>手数料差引後:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>釣り銭:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>If this is activated, but the change address is empty or invalid, change will be sent to a newly generated address.</source>
        <translation>これが有効にもかかわらずおつりアドレスが空欄であったり無効であった場合には、おつりは新しく生成されたアドレスへ送金されます。</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+16"/>
        <source>Custom change address</source>
        <translation>カスタムおつりアドレス</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>Transaction Fee:</source>
        <translation>トランザクション手数料：</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Choose your transaction fee.</source>
        <translation>取引手数料を選択してください。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose...</source>
        <translation>選択……</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>collapse fee-settings</source>
        <translation>手数料設定を折りたたむ</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>per kilobyte</source>
        <translation>1キロバイトあたり手数料</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+16"/>
        <source>If the custom fee is set to 1000 satoshis and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 satoshis in fee, while &quot;total at least&quot; pays 1000 satoshis. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation>カスタム手数料が1000satoshiに設定されている場合、トランザクションサイズが250バイトとすると、「1キロバイトあたり手数料」では250satoshiの手数料のみを支払いますが、「最小手数料」では1000satoshiを支払います。1キロバイトを超えるトランザクションの場合には、どちらの方法を選択したとしても1キロバイトあたりで支払われます。</translation>
    </message>
    <message>
        <location line="-64"/>
        <source>Hide</source>
        <translation>隠す</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>total at least</source>
        <translation>最小手数料</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Custom fee amount</source>
        <translation>カスタム料金の金額</translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+13"/>
        <source>Paying only the minimum fee is just fine as long as there is less transaction volume than space in the blocks. But be aware that this can end up in a never confirming transaction once there is more demand for nexa transactions than the network can process.</source>
        <translation>ブロックの容量に比べてトランザクション流量が少ないうちは最小手数料のみの支払で十分です。しかしながらネットワークが処理しきれないほどnexaトランザクションの需要がひとたび生まれてしまった場合には、永遠に検証がされないトランザクションになってしまう可能性があることに注意してください。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>(read the tooltip)</source>
        <translation>（ツールチップをお読みください）</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Use the recommended fee amount.  You can select a faster or slower confirmation time by moving the &quot;Confirmation Time&quot; slider.</source>
        <translation>推奨される手数料額を使用してください。 「確認時間」スライダーを動かすことで、確認時間を早くするか遅くするかを選択できます。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Recommended:</source>
        <translation>推奨：</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Choose a custom fee amount</source>
        <translation>カスタム料金の金額を選択してください</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Custom:</source>
        <translation>カスタム：</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>(Smart fee not initialized yet. This usually takes a few blocks...)</source>
        <translation>（スマート手数料はまだ初期化されていません。これにはおおよそ数ブロックほどかかります……）</translation>
    </message>
    <message>
        <location line="+29"/>
        <location line="+33"/>
        <source>Transaction confirmation time</source>
        <translation>取引確認時間</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Confirmation time:</source>
        <translation>検証時間：</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>normal</source>
        <translation>普通</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>fast</source>
        <translation>高速</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Send as zero-fee transaction if possible</source>
        <translation>可能な場合には手数料ゼロのトランザクションとして送金する</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(confirmation may take longer)</source>
        <translation>（検証に長い時間がかかる可能性があります）</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Send to multiple recipients at once</source>
        <translation>一度に複数の人に送る</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add &amp;Recipient</source>
        <translation>受取人を追加 (&amp;R)</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Clear all fields of the form.</source>
        <translation>全ての入力項目をクリア</translation>
    </message>
    <message>
        <location line="-880"/>
        <source>Dust:</source>
        <translation>ダスト：</translation>
    </message>
    <message>
        <location line="+883"/>
        <source>Clear &amp;All</source>
        <translation>すべてクリア (&amp;A)</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Current Balance</source>
        <translation>経常収支</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Balance:</source>
        <translation>残高:</translation>
    </message>
    <message>
        <location line="-87"/>
        <source>Confirm the send action</source>
        <translation>送る操作を確認する</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;end</source>
        <translation>送金 (&amp;E)</translation>
    </message>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="-231"/>
        <source>Confirm send coins</source>
        <translation>コインを送る確認</translation>
    </message>
    <message>
        <location line="-52"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+4"/>
        <source>%1 to %2</source>
        <translation>%1 から %2</translation>
    </message>
    <message>
        <location line="-289"/>
        <source>Copy quantity</source>
        <translation>数量をコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>総額のコピー</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy fee</source>
        <translation>手数料をコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>手数料差引後の値をコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>バイト数をコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>優先度をコピーする</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy change</source>
        <translation>釣り銭をコピー</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>&lt;b&gt;Public label:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;パブリック ラベル:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Total Amount %1</source>
        <translation>合計：&#x3000;%1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>or</source>
        <translation>または</translation>
    </message>
    <message>
        <location line="+190"/>
        <source>The amount to pay must be larger than 0.</source>
        <translation>支払額は0より大きくないといけません。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount exceeds your balance.</source>
        <translation>額が残高を超えています。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The total exceeds your balance when the %1 transaction fee is included.</source>
        <translation>%1 の取引手数料を含めると額が残高を超えています。</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction creation failed!</source>
        <translation>トラザクションの作成に失敗しました!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The transaction was rejected! This might happen if some of the coins in your wallet were already spent, such as if you used a copy of wallet.dat and coins were spent in the copy but not marked as spent here.</source>
        <translation>トランザクションは拒否されました。wallet.dat のコピーを使い、そしてコピーしたウォレットからコインを使用したことがマークされなかったときなど、ウォレットのいくつかのコインがすでに使用されている場合に、このエラーは起こるかもしれません。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A fee higher than %1 is considered an absurdly high fee.</source>
        <translation>%1 よりも高い手数料の場合、手数料が高すぎると判断されます。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Payment request expired.</source>
        <translation>支払いリクエストの期限が切れました。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Public Label exeeds limit of </source>
        <translation>パブリック ラベルが制限を超えています </translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Pay only the required fee of %1</source>
        <translation>要求手数料 %1 のみを支払う</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>The recipient address is not valid. Please recheck.</source>
        <translation>受取アドレスが不正です。再チェックしてください。</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Duplicate address found: addresses should only be used once each.</source>
        <translation>重複したアドレスが見つかりました: アドレスはそれぞれ一度のみ使用することができます。</translation>
    </message>
    <message>
        <location line="+252"/>
        <source>Warning: Invalid Nexa address</source>
        <translation>警告：無効なNexaアドレスです</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Confirm custom change address</source>
        <translation>パブリック ラベルが制限を超えています</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The address you selected for change is not part of this wallet. Any or all funds in your wallet may be sent to this address. Are you sure?</source>
        <translation>変更のために選択したアドレスは、このウォレットの一部ではありません。 ウォレット内の一部またはすべての資金をこのアドレスに送信できます。 本気ですか？</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>(no label)</source>
        <translation>（ラベル無し）</translation>
    </message>
    <message>
        <location line="-28"/>
        <source>Warning: Unknown change address</source>
        <translation>警告：未知のおつりアドレスです</translation>
    </message>
    <message>
        <location line="-777"/>
        <source>Copy dust</source>
        <translation>ダストをコピーする</translation>
    </message>
    <message>
        <location line="+291"/>
        <source>Are you sure you want to send?</source>
        <translation>送ってよろしいですか？</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>added as transaction fee</source>
        <translation>取引手数料として追加された</translation>
    </message>
</context>
<context>
    <name>SendCoinsEntry</name>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="+86"/>
        <location line="+677"/>
        <location line="+560"/>
        <source>A&amp;mount:</source>
        <translation>金額(&amp;A):</translation>
    </message>
    <message>
        <location line="-1224"/>
        <source>Pay &amp;To:</source>
        <translation>送り先(&amp;T):</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Choose previously used address</source>
        <translation>前に使用したアドレスを選ぶ</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>Private Description:</source>
        <translation>私的な説明:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Private &amp;Label:</source>
        <translation>自社ブランド：</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>The Nexa address to send the payment to</source>
        <translation>支払の送金先Nexaアドレス</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>クリップボードからアドレスを貼付ける</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+553"/>
        <location line="+560"/>
        <source>Remove this entry</source>
        <translation>この項目を削除する</translation>
    </message>
    <message>
        <location line="-1091"/>
        <source>A message that was attached to the coin: URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>コインに添付されたメッセージ: 参照用にトランザクションと共に保存される URI。 注: このメッセージは Nexa ネットワーク経由では送信されません。</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter a private label for this address to add it to the list of used addresses</source>
        <translation>このアドレスのプライベート ラベルを入力して、使用済みアドレスのリストに追加します</translation>
    </message>
    <message>
        <location line="+11"/>
        <location filename="../sendcoinsentry.cpp" line="+43"/>
        <source>Enter a public label for this transaction</source>
        <translation>この取引の公開ラベルを入力してください</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Public Label:</source>
        <translation>パブリックレーベル:</translation>
    </message>
    <message>
        <location line="-180"/>
        <source>The fee will be deducted from the amount being sent. The recipient will receive less coins than you enter in the amount field. If multiple recipients are selected, the fee is split equally.</source>
        <translation>送金する金額から手数料が差し引かれます。受取人は数量フィールドで指定した量よりも少ないビットコインを受け取ります。受取人が複数いる場合には、手数料は均等割されます。</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Amount to send</source>
        <translation>送金額</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>S&amp;ubtract fee from amount</source>
        <translation>送金額から手数料を差し引く (&amp;U)</translation>
    </message>
    <message>
        <location line="+643"/>
        <source>This is an unauthenticated payment request.</source>
        <translation>これは未認証の支払いリクエストです。</translation>
    </message>
    <message>
        <location line="+556"/>
        <source>This is an authenticated payment request.</source>
        <translation>これは認証済みの支払いリクエストです。</translation>
    </message>
    <message>
        <location filename="../sendcoinsentry.cpp" line="-12"/>
        <source>A message that was attached to the %1 URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>%1 URIに添付されていたメッセージです。これは参照用としてトランザクションとともに保存されます。注意：このメッセージはNexaネットワークを通して送信されるわけではありません。</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Enter a private label for this address to add it to your address book</source>
        <translation>アドレス帳に追加するには、このアドレスのプライベート ラベルを入力してください</translation>
    </message>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="-541"/>
        <location line="+556"/>
        <source>Pay To:</source>
        <translation>支払先:</translation>
    </message>
    <message>
        <location line="-522"/>
        <location line="+560"/>
        <source>Memo:</source>
        <translation>メモ：</translation>
    </message>
</context>
<context>
    <name>ShutdownWindow</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+76"/>
        <source>%1 is shutting down...</source>
        <translation>%1 をシャットダウンしています...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do not shut down the computer until this window disappears.</source>
        <translation>このウィンドウが消えるまでコンピュータをシャットダウンしないで下さい。</translation>
    </message>
</context>
<context>
    <name>SignVerifyMessageDialog</name>
    <message>
        <location filename="../forms/signverifymessagedialog.ui" line="+14"/>
        <source>Signatures - Sign / Verify a Message</source>
        <translation>署名 - メッセージの署名/検証</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sign Message</source>
        <translation>メッセージの署名 (&amp;S)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You can sign messages/agreements with your addresses to prove you can receive coins sent to them. Be careful not to sign anything vague or random, as phishing attacks may try to trick you into signing your identity over to them. Only sign fully-detailed statements you agree to.</source>
        <translation>あなたの所有しているアドレスによりメッセージや合意書に署名をすることで、それらアドレスに対して送られたビットコインを受け取ることができることを証明できます。フィッシング攻撃により不正にあなたの識別情報を署名させられてしまうことを防ぐために、不明確なものやランダムなものに対して署名しないよう注意してください。合意することが可能な、よく詳細の記された文言にのみ署名するようにしてください。</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The Nexa address to sign the message with</source>
        <translation>メッセージを署名するNexaアドレス</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+210"/>
        <source>Choose previously used address</source>
        <translation>前に使用したアドレスを選ぶ</translation>
    </message>
    <message>
        <location line="-200"/>
        <location line="+210"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="-200"/>
        <source>Paste address from clipboard</source>
        <translation>クリップボードからアドレスを貼付ける</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter the message you want to sign here</source>
        <translation>ここにあなたが署名するメッセージを入力します</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Signature</source>
        <translation>署名</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Copy the current signature to the system clipboard</source>
        <translation>現在の署名をシステムのクリップボードにコピーする</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Sign the message to prove you own this Nexa address</source>
        <translation>この Nexa アドレスを所有していることを証明するためにメッセージに署名</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sign &amp;Message</source>
        <translation>メッセージの署名 (&amp;M)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all sign message fields</source>
        <translation>入力項目の内容をすべて消去します</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+143"/>
        <source>Clear &amp;All</source>
        <translation>すべてクリア (&amp;A)</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>&amp;Verify Message</source>
        <translation>メッセージの検証 (&amp;V)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter the receiver&apos;s address, message (ensure you copy line breaks, spaces, tabs, etc. exactly) and signature below to verify the message. Be careful not to read more into the signature than what is in the signed message itself, to avoid being tricked by a man-in-the-middle attack. Note that this only proves the signing party receives with the address, it cannot prove sendership of any transaction!</source>
        <translation>受取人のアドレスとメッセージ（改行やスペース、タブなども完全に一致するよう注意してください）および署名を以下に入力し、メッセージの署名を検証してください。中間者攻撃により騙されるのを防ぐため、署名対象のメッセージに書かれていること以上の意味を署名から読み取ろうとしないよう注意してください。これは署名作成者がこのアドレスで受け取ったことを証明するだけであり、トランザクションの送信権限を証明するものではないことに注意してください！</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>The Nexa address the message was signed with</source>
        <translation>メッセージの署名に使われたNexaアドレス</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Verify the message to ensure it was signed with the specified Nexa address</source>
        <translation>指定された Nexa アドレスで署名されたことを保証するメッセージを検証</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify &amp;Message</source>
        <translation>メッセージの検証 (&amp;M)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all verify message fields</source>
        <translation>入力項目の内容をすべて消去します</translation>
    </message>
    <message>
        <location filename="../signverifymessagedialog.cpp" line="+40"/>
        <source>Click &quot;Sign Message&quot; to generate signature</source>
        <translation>署名を作成するには&quot;メッセージの署名&quot;をクリック</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Wallet unlock was cancelled.</source>
        <translation>ウォレットのアンロックはキャンセルされました。</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Message signed.</source>
        <translation>メッセージに署名しました。</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Message verified.</source>
        <translation>メッセージは検証されました。</translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="../networkstyle.cpp" line="+19"/>
        <source>[testnet]</source>
        <translation>[testnet]</translation>
    </message>
</context>
<context>
    <name>TokenDescDialog</name>
    <message>
        <location filename="../forms/tokendescdialog.ui" line="+17"/>
        <source>Token details</source>
        <translation>トークンの詳細</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This pane shows a detailed description of the token&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;このペインにはトークンの詳細な説明が表示されます&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>TokenDisplayDialog</name>
    <message>
        <location filename="../forms/tokendisplaydialog.ui" line="+14"/>
        <source>Token Display Manager</source>
        <translation>トークン表示マネージャー</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add a token to the list below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;以下のリストにトークンを追加します&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add New Token</source>
        <translation>新しいトークンを追加</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove a token from the list below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;以下のリストからトークンを削除します&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove Token</source>
        <translation>トークンを削除</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Display</source>
        <translation>表示 </translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ticker</source>
        <translation>ティッカー</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Group ID</source>
        <translation>グループID</translation>
    </message>
    <message>
        <location filename="../tokendisplaydialog.cpp" line="+43"/>
        <source>Copy GroupID</source>
        <translation>グループIDをコピー</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Token Ticker</source>
        <translation>トークンティッカーをコピー</translation>
    </message>
</context>
<context>
    <name>TokenHistoryView</name>
    <message>
        <location filename="../tokenhistoryview.cpp" line="+78"/>
        <location line="+19"/>
        <source>All</source>
        <translation>すべて</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>今日</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>今週</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>今月</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>先月</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>今年</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>期間...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>で受信</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent</source>
        <translation>送金</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>自分自身</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mint</source>
        <translation>ミント</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>その他</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter token ID to search</source>
        <translation>トークンIDを入力して検索します</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>最小の額</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy token id</source>
        <translation>トークンIDをコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>総額のコピー</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction id</source>
        <translation>取引 ID をコピー</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction idem</source>
        <translation>取引同上をコピー</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy raw transaction</source>
        <translation>生トランザクションをコピー</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>取引の詳細を表示</translation>
    </message>
    <message>
        <location line="+167"/>
        <source>Export Token History</source>
        <translation>トークン履歴のエクスポート</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>CSVファイル (*.csv)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Confirmed</source>
        <translation>検証済み</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation>監視限定</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>日付</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>タイプ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>ラベル</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>アドレス</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction ID</source>
        <translation>トランザクションID</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction Idem</source>
        <translation>取引同上</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token ID</source>
        <translation>トークンID</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token Amount</source>
        <translation>トークンの量</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Failed</source>
        <translation>エクスポートに失敗しました</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the token history to %1.</source>
        <translation>トークン履歴を %1 に保存しようとしてエラーが発生しました。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>エクスポートに成功しました</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The token history was successfully saved to %1.</source>
        <translation>トークン履歴は %1 に正常に保存されました。</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Range:</source>
        <translation>期間:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>から</translation>
    </message>
</context>
<context>
    <name>TokenTableModel</name>
    <message>
        <location filename="../tokentablemodel.cpp" line="+354"/>
        <source>Date</source>
        <translation>日付</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>タイプ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Token ID</source>
        <translation>トークンID</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Net Amount</source>
        <translation>正味金額</translation>
    </message>
    <message numerus="yes">
        <location line="+60"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>%n 以上のブロックを開く</numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open until %1</source>
        <translation>ユニット %1 を開く</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Offline</source>
        <translation>オフライン</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>未検証</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>検証中（%2の推奨検証数のうち、%1検証が完了）</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>検証されました (%1 検証済み)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicted</source>
        <translation>衝突</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation>二重支出</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation>捨てられた</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>未成熟（%1検証。%2検証完了後に使用可能となります）</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>このブロックは他のどのノードによっても受け取られないで、多分受け入れられないでしょう！</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>生成されましたが承認されませんでした</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation>で受信</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>送り主</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent</source>
        <translation>送金</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>自分自身への支払い</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mint</source>
        <translation>ミント</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Melt</source>
        <translation>溶ける</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation>パブリックラベル</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation>その他</translation>
    </message>
    <message>
        <location line="+325"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>取引の状況。このフィールドの上にカーソルを置くと検証の数を表示します。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>取引を受信した日時。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>取引の種類。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>監視限定アドレスがこのトランザクションに含まれているかどうか</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>ユーザ定義のトランザクションの意図や目的。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>残高に追加または削除された総額。</translation>
    </message>
</context>
<context>
    <name>TokensViewDialog</name>
    <message>
        <location filename="../forms/tokensviewdialog.ui" line="+14"/>
        <source>Tokens</source>
        <translation>トークン</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enable/Disable which tokens in the wallet are to be displayed in QT&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;ウォレット内のどのトークンを QT に表示するかを有効/無効にします&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Manage Displayed Tokens</source>
        <translation>表示されたトークンを管理する</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tokens in Wallet:</source>
        <translation>ウォレット内のトークン:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tokens Displayed: </source>
        <translation>表示されるトークン:</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Amount: </source>
        <translation>総額:</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The number of tokens to send&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;送信するトークンの数&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>  Pay To: </source>
        <translation>支払先:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The Nexa address to send the payment to</source>
        <translation>支払の送金先Nexaアドレス</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation>前に使用したアドレスを選ぶ</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>クリップボードからアドレスを貼付ける</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remove this entry</source>
        <translation>この項目を削除する</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Send tokens&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;トークンを送信する&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Send</source>
        <translation>送信</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Token ID</source>
        <translation>トークンID</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The unique token identifier</source>
        <translation>一意のトークン識別子</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>The token group identifier</source>
        <translation>トークングループ識別子</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The name of the token</source>
        <translation>トークンの名前</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ticker</source>
        <translation>ティッカー</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The token ticker symbol</source>
        <translation>トークンティッカーシンボル</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Balance</source>
        <translation>残高</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed token balance</source>
        <translation>トークン残高を確認しました</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pending</source>
        <translation>検証待ち</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed token balance</source>
        <translation>未確認のトークン残高</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Sub</source>
        <translation>亜群</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>If checked then this item is a subgroup.</source>
        <translation>チェックすると、この項目はサブグループになります。</translation>
    </message>
    <message>
        <location filename="../tokensviewdialog.cpp" line="+397"/>
        <location line="+7"/>
        <location line="+4"/>
        <source>Data</source>
        <translation>データ</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>string</source>
        <translation>弦</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+4"/>
        <source>num</source>
        <translation>番号</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>NaN</source>
        <translation>数字ではありません</translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+84"/>
        <location line="+5"/>
        <location line="+5"/>
        <source>Total Mintage:</source>
        <translation>総鋳造数:</translation>
    </message>
    <message>
        <location line="-93"/>
        <location line="+94"/>
        <source>Token mintage is unavailable because the database needs a reindex</source>
        <translation>データベースのインデックスの再作成が必要なため、トークンの鋳造は利用できません</translation>
    </message>
    <message>
        <location line="-86"/>
        <location line="+43"/>
        <source>Name:</source>
        <translation>名前:</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Ticker:</source>
        <translation>ティッカー:</translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+43"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Hash:</source>
        <translation>ハッシュ:</translation>
    </message>
    <message>
        <location line="-39"/>
        <location line="+43"/>
        <source>Decimals:</source>
        <translation>ティッカー:</translation>
    </message>
    <message>
        <location line="-37"/>
        <location line="+14"/>
        <location line="+62"/>
        <location line="+14"/>
        <source>Current Authorities:</source>
        <translation>現在の当局:</translation>
    </message>
    <message>
        <location line="-88"/>
        <location line="+76"/>
        <source>Mint:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Melt:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Renew:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Rescript:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Subgroup:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+76"/>
        <source>Token authorities are unavailable because the database needs a reindex</source>
        <translation>データベースのインデックスの再作成が必要なため、トークン権限が利用できません</translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+77"/>
        <source>Balance:</source>
        <translation>残高:</translation>
    </message>
    <message>
        <location line="-76"/>
        <location line="+77"/>
        <source>Pending:</source>
        <translation>検証待ち:</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Are you sure you want to send?</source>
        <translation>送ってよろしいですか？</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;b&gt;Token(s)&lt;/b&gt;</source>
        <translation>&lt;b&gt;トークン&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Confirm send tokens</source>
        <translation>送信トークンの確認</translation>
    </message>
</context>
<context>
    <name>TrafficGraphWidget</name>
    <message>
        <location filename="../trafficgraphwidget.cpp" line="+74"/>
        <source>KB/s</source>
        <translation>KB/s</translation>
    </message>
</context>
<context>
    <name>TransactionDesc</name>
    <message>
        <location filename="../transactiondesc.cpp" line="+35"/>
        <source>Open until %1</source>
        <translation>ユニット %1 を開く</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>conflicted</source>
        <translation>衝突</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/offline</source>
        <translation>%1/オフライン</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/unconfirmed</source>
        <translation>%1/未検証</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 confirmations</source>
        <translation>%1 確認</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Status</source>
        <translation>ステータス</translation>
    </message>
    <message numerus="yes">
        <location line="+7"/>
        <source>, broadcast through %n node(s)</source>
        <translation>
            <numerusform>%n ノードにブロードキャスト</numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>日付</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Source</source>
        <translation>ソース</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Generated</source>
        <translation>生成された</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+13"/>
        <location line="+122"/>
        <source>From</source>
        <translation>送信</translation>
    </message>
    <message>
        <location line="-142"/>
        <location line="+21"/>
        <location line="+35"/>
        <location line="+108"/>
        <source>To</source>
        <translation>受信</translation>
    </message>
    <message>
        <location line="-209"/>
        <source>double spent</source>
        <translation>二重支出</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>abandoned</source>
        <translation>捨てられた</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>change address</source>
        <translation>住所を変える</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>own address</source>
        <translation>自分のアドレス</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+113"/>
        <source>watch-only</source>
        <translation>監視限定</translation>
    </message>
    <message>
        <location line="-110"/>
        <location line="+28"/>
        <location line="+107"/>
        <source>label</source>
        <translation>ラベル</translation>
    </message>
    <message>
        <location line="-116"/>
        <location line="+57"/>
        <location line="+48"/>
        <location line="+71"/>
        <source>Public label:</source>
        <translation>パブリックラベル:</translation>
    </message>
    <message>
        <location line="-149"/>
        <location line="+41"/>
        <location line="+63"/>
        <location line="+47"/>
        <location line="+256"/>
        <source>Credit</source>
        <translation>クレジット</translation>
    </message>
    <message numerus="yes">
        <location line="-404"/>
        <source>matures in %n more block(s)</source>
        <translation>
            <numerusform>%n 以上のブロックが満期</numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>not accepted</source>
        <translation>承認されなかった</translation>
    </message>
    <message>
        <location line="+102"/>
        <location line="+31"/>
        <location line="+261"/>
        <source>Debit</source>
        <translation>引き落とし額</translation>
    </message>
    <message>
        <location line="-280"/>
        <source>Total debit</source>
        <translation>総出金額</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Total credit</source>
        <translation>総入金額</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction fee</source>
        <translation>取引手数料</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Net amount</source>
        <translation>正味金額</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+13"/>
        <source>Message</source>
        <translation>メッセージ</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Comment</source>
        <translation>コメント</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transaction ID</source>
        <translation>トランザクションID</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction Idem</source>
        <translation>取引同上</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction size</source>
        <translation>取引規模</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Merchant</source>
        <translation>商人</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Generated coins must mature %1 blocks before they can be spent. When you generated this block, it was broadcast to the network to be added to the block chain. If it fails to get into the chain, its state will change to &quot;not accepted&quot; and it won&apos;t be spendable. This may occasionally happen if another node generates a block within a few seconds of yours.</source>
        <translation>生成されたコインは使う前に%1のブロックを完成させる必要があります。あなたが生成した時、このブロックはブロック チェーンに追加されるネットワークにブロードキャストされました。チェーンに追加されるのが失敗した場合、状態が&quot;不承認&quot;に変更されて使えなくなるでしょう。これは、別のノードがあなたの数秒前にブロックを生成する場合に時々起こるかもしれません。</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+33"/>
        <location line="+48"/>
        <source>Token ID</source>
        <translation>トークンID</translation>
    </message>
    <message>
        <location line="-76"/>
        <location line="+33"/>
        <location line="+48"/>
        <source>Ticker</source>
        <translation>ティッカー</translation>
    </message>
    <message>
        <location line="-80"/>
        <location line="+33"/>
        <location line="+48"/>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <location line="-77"/>
        <location line="+32"/>
        <location line="+48"/>
        <source>Decimals</source>
        <translation>ティッカー</translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+4"/>
        <source>Melt</source>
        <translation>溶ける</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+4"/>
        <location line="+56"/>
        <location line="+4"/>
        <source>Mint</source>
        <translation>ミント</translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+4"/>
        <source>Amount Sent</source>
        <translation>送金額</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+4"/>
        <source>Amount Received</source>
        <translation>いただいた分量</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Debug information</source>
        <translation>デバッグ情報</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transaction</source>
        <translation>取引</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inputs</source>
        <translation>入力</translation>
    </message>
    <message>
        <location line="-133"/>
        <location line="+4"/>
        <location line="+30"/>
        <location line="+4"/>
        <location line="+56"/>
        <location line="+4"/>
        <location line="+57"/>
        <source>Amount</source>
        <translation>総額</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+3"/>
        <source>true</source>
        <translation>正しい</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+3"/>
        <source>false</source>
        <translation>正しくない</translation>
    </message>
    <message>
        <location line="-533"/>
        <source>, has not been successfully broadcast yet</source>
        <translation>まだブロードキャストが成功していません</translation>
    </message>
    <message numerus="yes">
        <location line="-40"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>%n 以上のブロックを開く</numerusform>
        </translation>
    </message>
    <message>
        <location line="+73"/>
        <source>unknown</source>
        <translation>未確認</translation>
    </message>
</context>
<context>
    <name>TransactionDescDialog</name>
    <message>
        <location filename="../forms/transactiondescdialog.ui" line="+14"/>
        <source>Transaction details</source>
        <translation>取引の詳細</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>This pane shows a detailed description of the transaction</source>
        <translation>ここでは取引の詳細を表示しています</translation>
    </message>
</context>
<context>
    <name>TransactionGraphWidget</name>
    <message>
        <location filename="../transactiongraphwidget.cpp" line="+148"/>
        <source>tps</source>
        <translation>tps</translation>
    </message>
</context>
<context>
    <name>TransactionTableModel</name>
    <message>
        <location filename="../transactiontablemodel.cpp" line="+239"/>
        <source>Date</source>
        <translation>日付</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>タイプ</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>未成熟（%1検証。%2検証完了後に使用可能となります）</translation>
    </message>
    <message numerus="yes">
        <location line="-29"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>%n 以上のブロックを開く</numerusform>
        </translation>
    </message>
    <message>
        <location line="-60"/>
        <source>Address or Label</source>
        <translation></translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Open until %1</source>
        <translation>ユニット %1 を開く</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>検証されました (%1 検証済み)</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>このブロックは他のどのノードによっても受け取られないで、多分受け入れられないでしょう！</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>生成されましたが承認されませんでした</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Offline</source>
        <translation>オフライン</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>未検証</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>検証中（%2の推奨検証数のうち、%1検証が完了）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Conflicted</source>
        <translation>衝突</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation>二重支出</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation>捨てられた</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation>で受信</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>送り主</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent to</source>
        <translation>送り先</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>自分自身への支払い</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mined</source>
        <translation>発掘した</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation>パブリックラベル</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation>その他</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>watch-only</source>
        <translation>監視限定</translation>
    </message>
    <message>
        <location line="+277"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>取引の状況。このフィールドの上にカーソルを置くと検証の数を表示します。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>取引を受信した日時。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>取引の種類。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>監視限定アドレスがこのトランザクションに含まれているかどうか</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>ユーザ定義のトランザクションの意図や目的。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>残高に追加または削除された総額。</translation>
    </message>
</context>
<context>
    <name>TransactionView</name>
    <message>
        <location filename="../transactionview.cpp" line="+77"/>
        <location line="+19"/>
        <source>All</source>
        <translation>すべて</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>今日</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>今週</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>今月</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>先月</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>今年</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>期間...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>で受信</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent to</source>
        <translation>送り先</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>自分自身</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mined</source>
        <translation>発掘した</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>その他</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter address or label to search</source>
        <translation>検索するアドレスまたはラベルを入力</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>最小の額</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy address</source>
        <translation>アドレスをコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>ラベルをコピーする</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>総額のコピー</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction id</source>
        <translation>取引 ID をコピー</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy raw transaction</source>
        <translation>生トランザクションをコピー</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit label</source>
        <translation>ラベルの編集</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>取引の詳細を表示</translation>
    </message>
    <message>
        <location line="+176"/>
        <source>Export Transaction History</source>
        <translation>トランザクション履歴をエクスポートする</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Watch-only</source>
        <translation>監視限定</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Idem</source>
        <translation>Idem</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Failed</source>
        <translation>エクスポートに失敗しました</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the transaction history to %1.</source>
        <translation>トランザクション履歴を %1 へ保存する際にエラーが発生しました。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>エクスポートに成功しました</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction history was successfully saved to %1.</source>
        <translation>トランザクション履歴は正常に%1に保存されました。</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Comma separated file (*.csv)</source>
        <translation>テキスト CSV (*.csv)</translation>
    </message>
    <message>
        <location line="-179"/>
        <source>Copy transaction idem</source>
        <translation>取引同上をコピー</translation>
    </message>
    <message>
        <location line="+188"/>
        <source>Confirmed</source>
        <translation>検証済み</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>日付</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>タイプ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>ラベル</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Helbidea</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Range:</source>
        <translation>期間:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>から</translation>
    </message>
</context>
<context>
    <name>UnitDisplayStatusBarControl</name>
    <message>
        <location filename="../nexagui.cpp" line="+117"/>
        <source>Unit to show amounts in. Click to select another unit.</source>
        <translation>金額を表示する際の単位。クリックすることで他の単位を選択します。</translation>
    </message>
</context>
<context>
    <name>UnlimitedDialog</name>
    <message>
        <location filename="../forms/unlimited.ui" line="+14"/>
        <source>Unlimited</source>
        <translation>Unlimited</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Mining</source>
        <translation>&amp;採掘</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+10"/>
        <source>The largest block that will be mined</source>
        <translation>マイニングされる最大のブロック</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Maximum Generated Block Size (bytes) </source>
        <translation>最大生成ブロック サイズ (バイト) </translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Network</source>
        <translation>ネットワーク (&amp;N)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Bandwidth Restrictions in KBytes/sec (check to enable):</source>
        <translation>キロバイト/秒単位の帯域幅制限 (オンにして有効にします):</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Send</source>
        <translation>送信</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+71"/>
        <source>Max</source>
        <translation>マックス</translation>
    </message>
    <message>
        <location line="-35"/>
        <location line="+59"/>
        <source>Average</source>
        <translation>平均</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Receive</source>
        <translation>受け取る</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Active command-line options that override above options:</source>
        <translation>上のオプションを置き換えることのできる、有効なコマンドラインオプションの一覧:</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Reset all client options to default.</source>
        <translation>すべてのオプションを初期値に戻します。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>オプションをリセット (&amp;R)</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>キャンセル (&amp;C)</translation>
    </message>
    <message>
        <location filename="../unlimiteddialog.cpp" line="-345"/>
        <source>Confirm options reset</source>
        <translation>オプションのリセットの確認</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is a global reset of all settings!</source>
        <translation>これは、すべての設定のグローバル リセットです。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client restart required to activate changes.</source>
        <translation>変更を有効化するにはクライアントを再起動する必要があります。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>クライアントを終了します。続行してもよろしいですか？</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+39"/>
        <location line="+42"/>
        <location line="+42"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>アップストリーム トラフィック シェーピング パラメータを空白にすることはできません</translation>
    </message>
    <message>
        <location line="-108"/>
        <location line="+36"/>
        <location line="+43"/>
        <location line="+41"/>
        <source>Traffic shaping parameters have to be greater than 0.</source>
        <translation>トラフィック シェーピング パラメータは 0 より大きい必要があります。</translation>
    </message>
</context>
<context>
    <name>WalletFrame</name>
    <message>
        <location filename="../walletframe.cpp" line="+31"/>
        <source>No wallet has been loaded.</source>
        <translation>ウォレットがロードされていません</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Token Database Status</source>
        <translation>トークンデータベースのステータス</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token wallet not available because a -reindex is needed. Click &quot;Ok&quot; to perform a one time -reindex on the next startup. Then shutdown Nexa-Qt and restart to complete the process.</source>
        <translation>-reindex が必要なため、トークン ウォレットは使用できません。 「OK」をクリックすると、次回の起動時に 1 回限りの再インデックスが実行されます。 次に、Nexa-Qt をシャットダウンし、再起動してプロセスを完了します。</translation>
    </message>
</context>
<context>
    <name>WalletModel</name>
    <message>
        <location filename="../walletmodel.cpp" line="+300"/>
        <source>Send Coins</source>
        <translation>コインを送る</translation>
    </message>
</context>
<context>
    <name>WalletView</name>
    <message>
        <location filename="../walletview.cpp" line="+46"/>
        <location line="+20"/>
        <source>&amp;Export</source>
        <translation>エクスポート (&amp;E)</translation>
    </message>
    <message>
        <location line="-19"/>
        <location line="+20"/>
        <source>Export the data in the current tab to a file</source>
        <translation>ファイルに現在のタブのデータをエクスポート</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Backup Wallet</source>
        <translation>ウォレットのバックアップ</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+21"/>
        <source>Wallet Data (*.dat)</source>
        <translation>ウォレット データ (*.dat)</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Backup Failed</source>
        <translation>バックアップに失敗しました</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the wallet data to %1.</source>
        <translation>ウォレットデータを%1へ保存する際にエラーが発生しました。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The wallet data was successfully saved to %1.</source>
        <translation>ウォレット データは正常に%1に保存されました。</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Backup Successful</source>
        <translation>バックアップ成功</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Restore Wallet</source>
        <translation>ウォレットを復元</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Restore Failed</source>
        <translation>復元に失敗しました</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to restore the wallet data to %1.</source>
        <translation>ウォレット データを %1 に復元しようとしてエラーが発生しました。</translation>
    </message>
</context>
<context>
    <name>nexa</name>
    <message>
        <location filename="../nexastrings.cpp" line="+56"/>
        <source>Prune configured below the minimum of %d MiB.  Please use a higher number.</source>
        <translation>剪定が最小値の %d MiB以下に設定されています。もっと大きな値を使用してください。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Prune: last wallet synchronisation goes beyond pruned data. You need to -reindex (download the whole blockchain again in case of pruned node)</source>
        <translation>剪定: 最後のウォレット同期ポイントは、選定されたデータよりも過去のものとなっています。-reindexをする必要があります (剪定されたノードの場合、ブロックチェイン全体をダウンロードします)</translation>
    </message>
    <message>
        <location line="+104"/>
        <source>Error: A fatal internal error occurred, see debug.log for details</source>
        <translation>エラー：致命的な内部エラーが発生しました。詳細はdebug.logを参照してください</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Pruning blockstore...</source>
        <translation>ブロックデータを剪定しています……</translation>
    </message>
    <message>
        <location line="-165"/>
        <source>Distributed under the MIT software license, see the accompanying file COPYING or &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</source>
        <translation>MITソフトウェアライセンスのもとで配布されています。付属のCOPYINGファイルまたは&lt;http://www.opensource.org/licenses/mit-license.php&gt;を参照してください。</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>The block database contains a block which appears to be from the future. This may be due to your computer&apos;s date and time being set incorrectly. Only rebuild the block database if you are sure that your computer&apos;s date and time are correct</source>
        <translation>ブロックのデータベースに未来の時刻のブロックが含まれています。これはおそらくお使いのコンピュータに設定されている日時が間違っていることを示しています。お使いのコンピュータの日時が本当に正しい場合にのみ、ブロックのデータベースの再構築を行ってください。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>This is a pre-release test build - use at your own risk - do not use for mining or merchant applications</source>
        <translation>これはリリース前のテストビルドです - 各自の責任で利用すること - 採掘や商取引に使用しないでください</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>WARNING: abnormally high number of blocks generated, %d blocks received in the last %d hours (%d expected)</source>
        <translation>警告：異常に多くの数のブロックが生成されています。%d ブロックが最近 %d 時間以内に受け取られました。(期待値: %d)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: check your network connection, %d blocks received in the last %d hours (%d expected)</source>
        <translation>警告：ネットワーク接続を確認してください。%d ブロックが最近 %d 時間以内にに受け取られました。(期待値: %d)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.</source>
        <translation>警告： ネットワークは完全に同意しないみたいです。マイナーは何かの問題を経験してるみたいなんです。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.</source>
        <translation>警告： ピアーと完全に同意しないみたいです！アップグレードは必要かもしれません、それとも他のノードはアップグレードは必要かもしれません。</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>You can not run &quot;-salvagewallet&quot; as an HD wallet.

Please relaunch Nexa with &quot;-usehd=0&quot;.</source>
        <translation>「-salvagewallet」を HD ウォレットとして実行することはできません。

Nexa を「-usehd=0」で再起動してください。</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Corrupted block database detected</source>
        <translation>破損したブロック データベースが検出されました</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to rebuild the block database now?</source>
        <translation>ブロック データベースを今すぐ再構築しますか?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error initializing block database</source>
        <translation>ブロック データベースの初期化中にエラー</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing wallet database environment %s!</source>
        <translation>ウォレットのデータベース環境 %s 初期化エラー!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error opening block database</source>
        <translation>ブロック データベースの開始エラー</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error: Disk space is low!</source>
        <translation>エラー: ディスク容量不足!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Failed to listen on any port. Use -listen=0 if you want this.</source>
        <translation>ポートのリスンに失敗しました。必要であれば -listen=0 を使用してください。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Importing...</source>
        <translation>インポートしています……</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Incorrect or no genesis block found. Wrong datadir for network?</source>
        <translation>不正なブロックあるいは、生成されていないブロックが見つかりました。ネットワークの datadir が間違っていませんか?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid -onion address: &apos;%s&apos;</source>
        <translation>無効な -onion アドレス：&apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Not enough file descriptors available.</source>
        <translation>使用可能なファイルディスクリプタが不足しています。</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Prune cannot be configured with a negative value.</source>
        <translation>剪定値は負の値に設定できません。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune mode is incompatible with -txindex.</source>
        <translation>剪定モードは-txindexと互換性がありません。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Signing token transaction failed</source>
        <translation>署名トークントランザクションが失敗しました</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>User Agent comment (%s) contains unsafe characters.</source>
        <translation>ユーザエージェントのコメント (%s) には安全でない文字が含まれています。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying blocks...</source>
        <translation>ブロックの検証中...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying wallet...</source>
        <translation>ウォレットの検証中...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Waiting for Genesis Block...</source>
        <translation>ジェネシスブロックを待っています...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet %s resides outside data directory %s</source>
        <translation>財布 %s はデータ・ディレクトリ%sの外にあります</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Error: Listening for incoming connections failed (listen returned error %s)</source>
        <translation>エラー: 内向きの接続をリッスンするのに失敗しました (エラー %s が返却されました)</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>The transaction amount is too small to send after the fee has been deducted</source>
        <translation>手数料差引後のトランザクションの金額が小さすぎるため、送金できません。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit &lt;https://www.openssl.org/&gt; and cryptographic software written by Eric Young and UPnP software written by Thomas Bernard.</source>
        <translation>この製品はOpenSSLプロジェクトにより開発されたソフトウェアをOpenSSLツールキットとして利用しています &lt;https://www.openssl.org/&gt;。また、Eric Young氏により開発された暗号ソフトウェア、Thomas Bernard氏により書かれたUPnPソフトウェアを用いています。</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Activating best chain...</source>
        <translation>最優良のチェインを有効化しています...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cannot resolve -whitebind address: &apos;%s&apos;</source>
        <translation>-whitebind アドレス &apos;%s&apos; を解決できません</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copyright (C) 2015-%i The Bitcoin Unlimited Developers</source>
        <translation>Copyright (C) 2015-%i Bitcoin Unlimited 開発者</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Information</source>
        <translation>情報</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid netmask specified in -whitelist: &apos;%s&apos;</source>
        <translation>-whitelist に対する無効なネットマスクです: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Need to specify a port with -whitebind: &apos;%s&apos;</source>
        <translation>-whitebind を用いてポートを指定する必要があります: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Signing transaction failed</source>
        <translation>取引の署名に失敗しました</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Starting txindex</source>
        <translation>txindex の開始</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction amount is too small to pay the fee</source>
        <translation>トランザクションの金額が小さすぎて手数料を支払えません</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is experimental software.</source>
        <translation>これは実験的なソフトウェアです。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amount too small</source>
        <translation>取引の額が小さ過ぎます</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amounts must be positive</source>
        <translation>取引の額は0より大きくしてください</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction has %d outputs. Maximum outputs allowed is %d</source>
        <translation>トランザクションには %d 個の出力があります。 許可される最大出力は %d です</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction of %d bytes is too large. Maximum allowed is %d bytes</source>
        <translation>%d バイトのトランザクションが大きすぎます。 許可される最大値は %d バイトです</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction too large for fee policy</source>
        <translation>手数料ポリシーに対してトランザクションが大きすぎます</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer (bind returned error %s)</source>
        <translation>このコンピュータの %s にバインドすることができません (バインドが返したエラーは %s)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location line="-42"/>
        <source>Loading addresses...</source>
        <translation>アドレスを読み込んでいます...</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Invalid -proxy address: &apos;%s&apos;</source>
        <translation>無効な -proxy アドレス: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Unknown network specified in -onlynet: &apos;%s&apos;</source>
        <translation>-onlynet で指定された &apos;%s&apos; は未知のネットワークです</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Cannot resolve -bind address: &apos;%s&apos;</source>
        <translation>-bind のアドレス &apos;%s&apos; を解決できません</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -externalip address: &apos;%s&apos;</source>
        <translation>-externalip のアドレス &apos;%s&apos; を解決できません</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Loading block index...</source>
        <translation>ブロック インデックスを読み込んでいます...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet...</source>
        <translation>ウォレットを読み込んでいます...</translation>
    </message>
    <message>
        <location line="-38"/>
        <source>Cannot downgrade wallet</source>
        <translation>ウォレットのダウングレードはできません</translation>
    </message>
    <message>
        <location line="-131"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The %s developers</source>
        <translation>%s 開発者</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bitcoin Bitcoin XT and Bitcoin Unlimited</source>
        <translation>Bitcoin Bitcoin XT and Bitcoin Unlimited</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-wallet.maxTxFee is set very high! Fees this large could be paid on a single transaction.</source>
        <translation>-wallet.maxTxFee が非常に高く設定されています! これほど大きな手数料は、1 回の取引で支払うことができます。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>-wallet.payTxFee is set very high! This is the transaction fee you will pay if you send a transaction.</source>
        <translation>-wallet.payTxFee は非常に高く設定されています! これは、トランザクションを送信する場合に支払うトランザクション手数料です。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot obtain a lock on data directory %s. %s is probably already running.</source>
        <translation>データ ディレクトリ %s のロックを取得できません。 %s はおそらく既に実行されています。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Could not locate RPC credentials. No authentication cookie could be found, and no rpcpassword is set in the configuration file (%s)</source>
        <translation>RPC クレデンシャルが見つかりませんでした。 認証 Cookie が見つからず、rpcpassword が構成ファイル (%s) に設定されていません</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; contained invalid data - see debug.log</source>
        <translation>配置構成ファイル &apos;%s&apos; に無効なデータが含まれていました - debug.log を参照してください</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading %s: You can&apos;t enable HD on a already existing non-HD wallet</source>
        <translation>%s のロード中にエラーが発生しました: 既存の非 HD ウォレットで HD を有効にすることはできません</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error reading %s! All keys read correctly, but transaction data or address book entries might be missing or incorrect.</source>
        <translation>%s の読み取りエラー! すべてのキーが正しく読み取られますが、トランザクション データまたはアドレス帳のエントリが欠落しているか正しくない可能性があります。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error reading from the coin database.
Details: %s

Do you want to reindex on the next restart?</source>
        <translation>コイン データベースからの読み込み中にエラーが発生しました。
詳細: %s

次回の再起動時にインデックスを再作成しますか?</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Failed to listen on all P2P ports. Failing as requested by -bindallorfail.</source>
        <translation>すべての P2P ポートでリッスンできませんでした。 -bindallorfail の要求どおりに失敗しました。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fee: %ld is larger than configured maximum allowed fee of : %ld.  To change, set &apos;wallet.maxTxFee&apos;.</source>
        <translation>手数料: %ld は、構成されている最大許容手数料: %ld を超えています。 変更するには、「wallet.maxTxFee」を設定します。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.maxTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least the minrelay fee of %s to prevent stuck transactions)</source>
        <translation>-wallet.maxTxFee=&lt;amount&gt; の無効な金額: &apos;%u&apos; (スタック トランザクションを防ぐには、少なくとも %s の最小リレー料金である必要があります)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.payTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least %s)</source>
        <translation>-wallet.payTxFee=&lt;amount&gt; の無効な金額: &apos;%u&apos; (少なくとも %s である必要があります)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please check that your computer&apos;s date and time are correct! If your clock is wrong, %s will not work properly.</source>
        <translation>コンピュータの日付と時刻が正しいことを確認してください。 時計が間違っていると、%s は正しく動作しません。</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Reducing -maxconnections from %d to %d because of file descriptor limitations (unix) or winsocket fd_set limitations (windows). If you are a windows user there is a hard upper limit of 1024 which cannot be changed by adjusting the node&apos;s configuration.</source>
        <translation>ファイル記述子の制限 (UNIX) または winsocket fd_set の制限 (Windows) のため、-net.maxConnections を %d から %d に減らしています。 Windows ユーザーの場合、ノードの構成を調整しても変更できない 1024 のハード上限があります。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Rescans are not possible in pruned mode. You will need to use -resync which will download the whole blockchain again.</source>
        <translation>プルーニング モードでは再スキャンはできません。ブロックチェーン全体を再度ダウンロードする -resync を使用する必要があります。</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Total length of network version string with uacomments added exceeded the maximum length (%i) and have been truncated.  Reduce the number or size of uacomments to avoid truncation.</source>
        <translation>uacomments が追加されたネットワーク バージョン文字列の合計の長さが最大長 (%i) を超えたため、切り捨てられました。 切り捨てを避けるために、uacomments の数またはサイズを減らします。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Transaction has %d inputs and %d outputs. Maximum inputs allowed are %d and maximum outputs are %d</source>
        <translation>トランザクションには %d 入力と %d 出力があります。 許可される最大入力は %d で、最大出力は %d です</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction has %d inputs. Maximum inputs allowed is %d. Try reducing inputs by transferring a smaller amount.</source>
        <translation>トランザクションには %d 個の入力があります。 許可される最大入力数は %d です。 より少ない金額を転送して、入力を減らしてみてください。</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Wallet is not password protected. Your funds may be at risk! Goto &quot;Settings&quot; and then select &quot;Encrypt Wallet&quot; to create a password.</source>
        <translation>ウォレットはパスワードで保護されていません。 あなたの資金が危険にさらされる可能性があります！ 「設定」に移動し、「ウォレットの暗号化」を選択してパスワードを作成します。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Could not open deployment configuration CSV file &apos;%s&apos; for reading</source>
        <translation>警告: 配置構成の CSV ファイル &apos;%s&apos; を読み取り用に開けませんでした</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Warning: Unknown block versions being mined! It&apos;s possible unknown rules are in effect</source>
        <translation>警告: 未知のブロック バージョンがマイニングされています! 未知のルールが適用されている可能性があります</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Wallet file corrupt, data salvaged! Original %s saved as %s in %s; if your balance or transactions are incorrect you should restore from a backup.</source>
        <translation>警告: ウォレット ファイルが破損しており、データが回収されました! 元の %s は %s として %s に保存されました。 残高または取引が正しくない場合は、バックアップから復元する必要があります。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You are trying to restore the same wallet which you are trying to replace.</source>
        <translation>交換しようとしているのと同じウォレットを復元しようとしています。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>You are trying to use -wallet.auto but neither -spendzeroconfchange nor -wallet.instant is turned on</source>
        <translation>-wallet.auto を使用しようとしていますが、-spendzeroconfchange も -wallet.instant もオンになっていません</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You can not send free transactions if you have configured a -relay.limitFreeRelay of zero</source>
        <translation>-relay.limitFreeRelay をゼロに設定した場合、無料のトランザクションを送信できません</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You need to rebuild the database using -resync to go back to unpruned mode.  This will redownload the entire blockchain.</source>
        <translation>プルーニングされていないモードに戻すには、-resync を使用してデータベースを再構築する必要があります。これにより、ブロックチェーン全体が再ダウンロードされます。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&quot;Restore Wallet&quot; succeeded and a backup of the previous wallet was saved to: %s.


When you click &quot;OK&quot; Nexa will shutdown to complete the process.</source>
        <translation>「ウォレットの復元」が成功し、以前のウォレットのバックアップが %s に保存されました。


「OK」をクリックすると、Nexa がシャットダウンしてプロセスが完了します。</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>%s </source>
        <translation>%s </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%s corrupt, salvage failed</source>
        <translation>%s が壊れています。サルベージに失敗しました</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-maxtxpool must be at least %d MB</source>
        <translation>-cache.maxTxPool は少なくとも %d MB でなければなりません</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-xthinbloomfiltersize must be at least %d Bytes</source>
        <translation>-xthinbloomfiltersize は少なくとも %d バイトでなければなりません</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot write default address</source>
        <translation>初期値のアドレスを書き込むことができません</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CommitTransaction failed.</source>
        <translation>トランザクションのコミットに失敗しました。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; not found</source>
        <translation>配置構成ファイル &apos;%s&apos; が見つかりません</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading %s</source>
        <translation>%s のロード中にエラーが発生しました</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet corrupted</source>
        <translation>%s のロード中にエラーが発生しました: ウォレットが破損しています</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet requires newer version of %s</source>
        <translation>%s のロード中にエラーが発生しました: ウォレットには新しいバージョンの %s が必要です</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: You can&apos;t disable HD on a already existing HD wallet</source>
        <translation>%s のロード中にエラーが発生しました: 既存の HD ウォレットで HD を無効にすることはできません</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: Keypool ran out, please call keypoolrefill first</source>
        <translation>エラー: キープールが不足しました。最初に keypoolrefill を呼び出してください</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Initialization sanity check failed. %s is shutting down.</source>
        <translation>初期化の健全性チェックに失敗しました。 %s はシャットダウンしています。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds for this token.  Need %d more.</source>
        <translation>このトークンには資金が不足しています。さらに %d 個必要です。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds or funds not confirmed</source>
        <translation>資金が不足している、または資金が確認されていない</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Keypool ran out, please call keypoolrefill first</source>
        <translation>Keypool が不足しています。最初に keypoolrefill を呼び出してください</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading Orphanpool</source>
        <translation>オーファンプールを読み込んでいます</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading TxPool</source>
        <translation>TxPool を読み込んでいます</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading banlist...</source>
        <translation>禁止リストを読み込んでいます...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Opening Block database...</source>
        <translation>ブロック データベースを開いています...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Coins Cache database...</source>
        <translation>Coins Cache データベースを開いています...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Description database...</source>
        <translation>トークン説明データベースを開いています...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Mintage database...</source>
        <translation>トークン ミンテージ データベースを開いています...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening UTXO database...</source>
        <translation>UTXO データベースを開いています...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2009-%i The Bitcoin Core Developers</source>
        <translation>Portions Copyright (C) 2009-%i The Bitcoin Core Developers</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2014-%i The Bitcoin XT Developers</source>
        <translation>Portions Copyright (C) 2014-%i The Bitcoin XT Developers</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Reaccepting Wallet Transactions</source>
        <translation>ウォレット取引の再受付</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rescanning...</source>
        <translation>再スキャン中...</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Turn off auto consolidate and try sending again.</source>
        <translation>自動統合をオフにして、もう一度送信してみてください。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer. %s is probably already running.</source>
        <translation>このコンピュータの %s にバインドできません。 %s はおそらく既に実行されています。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to start RPC services. See debug log for details.</source>
        <translation>RPC サービスを開始できません。 詳細については、デバッグ ログを参照してください。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Upgrading block database...This could take a while.</source>
        <translation>ブロック データベースをアップグレードしています...これには時間がかかる場合があります。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database </source>
        <translation>txindex データベースのアップグレード </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database...</source>
        <translation>txindex データベースをアップグレードしています...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Wallet needed to be rewritten: restart %s to complete</source>
        <translation>ウォレットを書き換える必要がありました: 完了するには %s を再起動してください</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zapping all transactions from wallet...</source>
        <translation>ウォレットからすべてのトランザクションをザッピングします...</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>Done loading</source>
        <translation>読み込み完了</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
</context>
</TS>
