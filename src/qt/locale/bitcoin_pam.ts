<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fil_PH">
<context>
    <name>AddressBookPage</name>
    <message>
        <location filename="../forms/addressbookpage.ui" line="+30"/>
        <source>Right-click to edit address or label</source>
        <translation>I-right click ban alilan ing address o libel</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Create a new address</source>
        <translation>Maglalang kang bayung address</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;New</source>
        <translation>&amp;Bayu</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Copy the currently selected address to the system clipboard</source>
        <translation>Kopyan me ing salukuyan at makipiling address keng system clipboard</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopyan</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Export the data in the current tab to a file</source>
        <translation>L-export ang data sa kasalukuyang tab sa isang file</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Export</source>
        <translation>&amp;L-export</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>C&amp;lose</source>
        <translation>I&amp;sara</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="+90"/>
        <source>&amp;Copy Address</source>
        <translation>&amp;Kopyan ing address</translation>
    </message>
    <message>
        <location filename="../forms/addressbookpage.ui" line="-53"/>
        <source>Delete the currently selected address from the list</source>
        <translation>Ilako ya ing kasalungsungan makapiling address keng listahan</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Delete</source>
        <translation>&amp;Ilako</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="-39"/>
        <source>Choose the address to send coins to</source>
        <translation>Pilinan ing address a magpadalang coins kang</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose the address to receive coins with</source>
        <translation>Pilinan ing address a tumanggap coins a atin</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>C&amp;hoose</source>
        <translation>P&amp;ilinan</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sending addresses</source>
        <translation>Address king pamag-Send</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Receiving addresses</source>
        <translation>Address king pamag-Tanggap</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>These are your Nexa addresses for sending payments. Always check the amount and the receiving address before sending coins.</source>
        <translation>Reni reng kekang Nexa address king pamagpadalang kabayaran. Lawan mulang masalese reng alaga ampo ing address na ning tumanggap bayu ka magpadalang barya.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>These are your Nexa addresses for receiving payments. It is recommended to use a new receiving address for each transaction.</source>
        <translation>Reni reng kekang Nexa addresses keng pamananggap bayad. Rerekomenda mi na gumamit kang bayung address keng balang transaksiyon.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Copy &amp;Label</source>
        <translation>Kopyan ing &amp;Label</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation>&amp;Alilan</translation>
    </message>
    <message>
        <location line="+173"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Comma separated file (*.csv)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Export Address List</source>
        <translation>l-export ang Listahan ng Address</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Exporting Failed</source>
        <translation>Nabigo ang pag-export</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the address list to %1. Please try again.</source>
        <translation>Nagkaroon ng error sa pagsubok na i-save ang listahan ng address sa %1. Pakisubukang muli.</translation>
    </message>
</context>
<context>
    <name>AddressTableModel</name>
    <message>
        <location filename="../addresstablemodel.cpp" line="+159"/>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>Address</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>(no label)</source>
        <translation>(alang label)</translation>
    </message>
</context>
<context>
    <name>AskPassphraseDialog</name>
    <message>
        <location filename="../forms/askpassphrasedialog.ui" line="+26"/>
        <source>Passphrase Dialog</source>
        <translation>Dialogo ning Passphrase</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Enter passphrase</source>
        <translation>Mamalub kang passphrase</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>New passphrase</source>
        <translation>Panibayung passphrase</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Repeat new passphrase</source>
        <translation>Pasibayuan ya ing bayung passphrase</translation>
    </message>
    <message>
        <location filename="../askpassphrasedialog.cpp" line="+47"/>
        <source>Encrypt wallet</source>
        <translation>I-encrypt ye ing wallet</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to unlock the wallet.</source>
        <translation>Ing operasyun a ini kailangan ne ing kekayung wallet passphrase, ban a-unlock ya ing wallet</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unlock wallet</source>
        <translation>Unlock ya ing wallet</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to decrypt the wallet.</source>
        <translation>Ing operasyun a ini kailangan ne ing kekang wallet passphrase ban a-decrypt ne ing wallet.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Decrypt wallet</source>
        <translation>I-decrypt ya ing wallet</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change passphrase</source>
        <translation>Alilan ya ing passphrase</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enter the old passphrase and new passphrase to the wallet.</source>
        <translation>Ilagay ang lumang passphrase at bagong passphrase sa wallet.</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Confirm wallet encryption</source>
        <translation>Kumpirman ya ing wallet encryption</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: If you encrypt your wallet and lose your passphrase, you will &lt;b&gt;LOSE ALL OF YOUR NEXA COINS&lt;/b&gt;!</source>
        <translation>Kapabaluan: Istung in-encrypt me ing kekang wallet at meala ya ing passphrase na, ma-&lt;b&gt;ALA NO NGAN RING KEKANG NEXA COINS&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Are you sure you wish to encrypt your wallet?</source>
        <translation>Siguradu na kang buri meng i-encrypt ing kekang wallet?</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 will close now to finish the encryption process. Remember that encrypting your wallet cannot fully protect your coins from being stolen by malware infecting your computer.</source>
        <translation>Magsasara ngayon ang %1 upang tapusin ang proseso ng pag-encrypt. Tandaan na ang pag-encrypt ng iyong wallet ay hindi maaaring ganap na maprotektahan ang iyong mga barya mula sa pagnanakaw ng malware na nakakahawa sa iyong computer.</translation>
    </message>
    <message>
        <location line="+109"/>
        <location line="+29"/>
        <source>Warning: The Caps Lock key is on!</source>
        <translation>Kapabaluan: Makabuklat ya ing Caps Lock key!</translation>
    </message>
    <message>
        <location line="-140"/>
        <location line="+61"/>
        <source>Wallet encrypted</source>
        <translation>Me-encrypt ne ing wallet</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Enter the new passphrase to the wallet.&lt;br/&gt;Please use a passphrase of &lt;b&gt;ten or more random characters&lt;/b&gt;, or &lt;b&gt;eight or more words&lt;/b&gt;.</source>
        <translation>Ilagay ang bagong passphrase sa wallet.&lt;br/&gt;Mangyaring gumamit ng passphrase na &lt;b&gt;sampu o higit pang random na character&lt;/b&gt;, o &lt;b&gt;walong o higit pang salita&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>IMPORTANT: Any previous backups you have made of your wallet file should be replaced with the newly generated, encrypted wallet file. For security reasons you should no longer keep previous unencrypted backups as your funds will be at risk if someone gains access to them.</source>
        <translation>MAHALAGA: Anumang mga nakaraang backup na ginawa mo sa iyong wallet file ay dapat mapalitan ng bagong nabuo, naka-encrypt na wallet file. Para sa mga kadahilanang pangseguridad, hindi mo na dapat itago ang mga nakaraang hindi naka-encrypt na backup dahil ang iyong mga pondo ay nasa panganib kung may makakakuha ng access sa kanila.</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+8"/>
        <location line="+42"/>
        <location line="+6"/>
        <source>Wallet encryption failed</source>
        <translation>Memali ya ing pamag-encrypt king wallet </translation>
    </message>
    <message>
        <location line="-55"/>
        <source>Wallet encryption failed due to an internal error. Your wallet was not encrypted.</source>
        <translation>Memali ya ing encryption uli na ning ausan dang internal error. E ya me-encrypt ing wallet yu.</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+48"/>
        <source>The supplied passphrases do not match.</source>
        <translation>E la mitutugma ring mibieng passphrase</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Wallet unlock failed</source>
        <translation>Memali ya ing pamag-unlock king wallet </translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+11"/>
        <location line="+19"/>
        <source>The passphrase entered for the wallet decryption was incorrect.</source>
        <translation>E ya istu ing passphrase a pepalub da para king wallet decryption</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Wallet decryption failed</source>
        <translation>Me-mali ya ing pamag-decrypt king wallet</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Wallet passphrase was successfully changed.</source>
        <translation>Mi-alilan ne ing passphrase na ning wallet.</translation>
    </message>
</context>
<context>
    <name>BanTableModel</name>
    <message>
        <location filename="../bantablemodel.cpp" line="+86"/>
        <source>IP/Netmask</source>
        <translation>IP/Netmask</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation>Ahente ng Gumagamit</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Banned Until</source>
        <translation>Pinagbawalan Hanggang</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ban Reason</source>
        <translation>Dahilan ng Pagbawal</translation>
    </message>
</context>
<context>
    <name>BitcoinGUI</name>
    <message>
        <location filename="../nexagui.cpp" line="+351"/>
        <source>Sign &amp;message...</source>
        <translation>I-sign ing &amp;mensayi</translation>
    </message>
    <message>
        <location line="+435"/>
        <source>Synchronizing with network...</source>
        <translation>Mag-sychronize ne king network...</translation>
    </message>
    <message>
        <location line="-556"/>
        <source>&amp;Overview</source>
        <translation>&amp;Overview</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Node</source>
        <translation>Node</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Show general overview of wallet</source>
        <translation>Ipakit ing kabuuang lawe ning wallet</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Send</source>
        <translation>&amp;Ipadala</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Receive</source>
        <translation>&amp;Tumanggap</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Request payments (generates QR codes and %1: URIs)</source>
        <translation>Humiling ng mga pagbabayad (bumubuo ng mga QR code at %1: URI)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Transactions</source>
        <translation>&amp;Transaksion</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse transaction history</source>
        <translation>Lawan ing kasalesayan ning transaksion</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Tokens</source>
        <translation>&amp;Mga token</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse or Send Tokens</source>
        <translation>Mag-browse o Magpadala ng mga Token</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Token History</source>
        <translation>Kasaysayan ng Token</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse Token History</source>
        <translation>I-browse ang Kasaysayan ng Token</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>E&amp;xit</source>
        <translation>L&amp;umwal</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit application</source>
        <translation>Tuknangan ing aplikasyon</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;About %1</source>
        <translation>&amp;Tungkul sa %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about %1</source>
        <translation>Magpakit impormasion tungkul king %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>About &amp;Qt</source>
        <translation>Tungkul sa &amp;Qt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about Qt</source>
        <translation>Magpakit impormasion tungkul king Qt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Options...</source>
        <translation>&amp;Pipamilian...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify configuration options for %1</source>
        <translation>Baguhin ang mga opsyon sa pagsasaayos para sa %1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Unlimited...</source>
        <translation>&amp;Unlimited...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify Bitcoin Unlimited Options</source>
        <translation>Baguhin ang Bitcoin Unlimited na Opsyon</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Encrypt Wallet...</source>
        <translation>I-&amp;Encrypt in Wallet...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Encrypt the private keys that belong to your wallet</source>
        <translation>I-encrypt ang mga pribadong key na kabilang sa iyong wallet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Backup Wallet...</source>
        <translation>I-&amp;Backup ing Wallet...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Restore Wallet...</source>
        <translation>&amp;Ibalik ang Wallet...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Restore wallet from another location</source>
        <translation>Ibalik ang wallet mula sa ibang lokasyon</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Change Passphrase...</source>
        <translation>&amp;Alilan ing Passphrase...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sign messages with your Nexa addresses to prove you own them</source>
        <translation>Mag-sign ng mga mensahe gamit ang iyong mga Nexa address para patunayan na pagmamay-ari mo ang mga ito</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Verify messages to ensure they were signed with specified Nexa addresses</source>
        <translation>I-verify ang mga mensahe upang matiyak na nilagdaan ang mga ito gamit ang mga tinukoy na Nexa address</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Sending addresses...</source>
        <translation>Address king pamag-&amp;Send</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show the list of used sending addresses and labels</source>
        <translation>Ipakita ang listahan ng mga ginamit na address at label sa pagpapadala</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Receiving addresses...</source>
        <translation>Address king pamag-Tanggap</translation>
    </message>
    <message>
        <location line="-127"/>
        <source>Send coins to a Nexa address</source>
        <translation>Magpadalang barya king Nexa address</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Backup wallet to another location</source>
        <translation>I-backup ing wallet king aliwang lugal</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Change the passphrase used for wallet encryption</source>
        <translation>Alilan ya ing passphrase a gagamitan para king wallet encryption</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Debug window</source>
        <translation>I-&amp;Debug ing awang</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open debugging and diagnostic console</source>
        <translation>Ibuklat ing debugging at diagnostic console</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>&amp;Verify message...</source>
        <translation>&amp;Beripikan ing message...</translation>
    </message>
    <message>
        <location line="+522"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="-785"/>
        <source>Wallet</source>
        <translation>Wallet</translation>
    </message>
    <message>
        <location line="+243"/>
        <source>&amp;Show / Hide</source>
        <translation>&amp;Ipalto / Isalikut</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show or hide the main Window</source>
        <translation>Ipalto o isalikut ing pun a awang</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Show the list of used receiving addresses and labels</source>
        <translation>Ipakita ang listahan ng ginamit na mga address at label sa pagtanggap</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open &amp;URI...</source>
        <translation>Buksan ang &amp;URI</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open a %1: URI or payment request</source>
        <translation>Magbukas ng %1: URI o kahilingan sa pagbabayad</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show the %1 help message to get a list with possible Nexa command-line options</source>
        <translation>Ipakita ang %1 na mensahe ng tulong upang makakuha ng listahan na may mga posibleng opsyon sa command-line ng Nexa</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&amp;Settings</source>
        <translation>&amp;Pamag-ayus</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Help</source>
        <translation>&amp;Saup</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Tabs toolbar</source>
        <translation>Gamit para king Tabs</translation>
    </message>
    <message>
        <location line="-86"/>
        <source>&amp;Command-line options</source>
        <translation>Pipamilian command-line</translation>
    </message>
    <message>
        <location line="+197"/>
        <source>%1 client</source>
        <translation>%1 kliyente</translation>
    </message>
    <message numerus="yes">
        <location line="+193"/>
        <source>%n active connection(s) to Nexa network</source>
        <translation>
            <numerusform>%n (mga) aktibong koneksyon sa Nexa network</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Importing blocks from disk...</source>
        <translation>Ini-import ang mga bloke mula sa disk...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindexing blocks on disk...</source>
        <translation>Muling pag-index ng mga bloke sa disk...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No block source available...</source>
        <translation>Walang available na block source...</translation>
    </message>
    <message numerus="yes">
        <location line="+9"/>
        <source>Processed %n block(s) of transaction history.</source>
        <translation>
            <numerusform>Naproseso ang %n (mga) block ng history ng transaksyon.</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+27"/>
        <source>%1 behind</source>
        <translation>%1 sa likod</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Last received block was generated %1 ago.</source>
        <translation>Ing tatauling block a metanggap,  me-generate ya %1 ing milabas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transactions after this will not yet be visible.</source>
        <translation>Ing transaksion kaibat na nini ali yapa magsilbing ipakit.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Error</source>
        <translation>Mali</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning</source>
        <translation>Kapabaluan</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Information</source>
        <translation>&amp;Impormasion</translation>
    </message>
    <message>
        <location line="-86"/>
        <source>Up to date</source>
        <translation>Makatuki ya king aldo</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Catching up...</source>
        <translation>Catching up...</translation>
    </message>
    <message>
        <location line="+164"/>
        <source>Date: %1
</source>
        <translation>Kaaldauan %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Amount: %1
</source>
        <translation>Halaga: %1</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type: %1
</source>
        <translation>Klase %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Label: %1
</source>
        <translation>Label: %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address: %1
</source>
        <translation>Address %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent transaction</source>
        <translation>Mipadalang transaksion</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Incoming transaction</source>
        <translation>Paparatang a transaksion</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>HD key generation is &lt;b&gt;enabled&lt;/b&gt;</source>
        <translation>Ang pagbuo ng HD key ay &lt;b&gt;pinagana&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>HD key generation is &lt;b&gt;disabled&lt;/b&gt;</source>
        <translation>Ang pagbuo ng HD key ay &lt;b&gt;naka-disable&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt;</source>
        <translation>Maka-&lt;b&gt;encrypt&lt;/b&gt; ya ing wallet at kasalukuyan yang maka-&lt;b&gt;unlocked&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;locked&lt;/b&gt;</source>
        <translation>Maka-&lt;b&gt;encrypt&lt;/b&gt; ya ing wallet at kasalukuyan yang maka-&lt;b&gt;locked&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>CoinControlDialog</name>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="+14"/>
        <source>Coin Selection</source>
        <translation>Pagpili ng barya</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Quantity:</source>
        <translation>Dami:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Bytes:</source>
        <translation>Bytes:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Amount:</source>
        <translation>Alaga:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Priority:</source>
        <translation>Priyoridad:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Fee:</source>
        <translation>Bayad:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation>Alikabok:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation>Pagkatapos ng Bayad:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Baguhin:</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>(un)select max inputs</source>
        <translation>(un)piliin ang mga max na input</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Tree mode</source>
        <translation>Tree mode</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>List mode</source>
        <translation>List mode</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Amount</source>
        <translation>Alaga</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with label</source>
        <translation>Natanggap na may label</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with address</source>
        <translation>Natanggap na may address</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Date</source>
        <translation>Kaaldauan</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmations</source>
        <translation>Mga kumpirmasyon</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed</source>
        <translation>Me-kumpirma</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Priority</source>
        <translation>Priyoridad</translation>
    </message>
    <message>
        <location filename="../coincontroldialog.cpp" line="+45"/>
        <source>Copy address</source>
        <translation>Kopyan ing address</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Kopyan ing label</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+29"/>
        <source>Copy amount</source>
        <translation>Kopyan ing alaga</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Copy transaction ID</source>
        <translation>Kopyahin ang transaction ID</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lock unspent</source>
        <translation>I-lock ang hindi nagastos</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unlock unspent</source>
        <translation>I-unlock ang hindi nagastos</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Copy quantity</source>
        <translation>Dami ng kopya</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy fee</source>
        <translation>Bayad sa pagkopya</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Kopyahin pagkatapos ng bayad</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Kopyan ing bytes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Kopyahin ang priyoridad</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation>Kopyahin ang alikabok</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>Kopyahin ang pagbabago</translation>
    </message>
    <message>
        <location line="+352"/>
        <source>highest</source>
        <translation>pinakamataas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>higher</source>
        <translation>mas mataas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>high</source>
        <translation>mataas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium-high</source>
        <translation>katamtaman-mataas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium</source>
        <translation>daluyan</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low-medium</source>
        <translation>mababang-daluyan</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low</source>
        <translation>mababa</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lower</source>
        <translation>mas mababa</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lowest</source>
        <translation>pinakamababa</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>(%1 locked)</source>
        <translation>(%1 naka-lock)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>none</source>
        <translation>wala</translation>
    </message>
    <message>
        <location line="+151"/>
        <source>yes</source>
        <translation>oo</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>no</source>
        <translation>hindi</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>This label turns red if the transaction size is greater than 1000 bytes.</source>
        <translation>Nagiging pula ang label na ito kung ang laki ng transaksyon ay higit sa 1000 bytes.</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+8"/>
        <source>This means a fee of at least %1 per kB is required.</source>
        <translation>Nangangahulugan ito na kailangan ang bayad na hindi bababa sa %1 bawat kB.</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Can vary +/- 1 byte per input.</source>
        <translation>Maaaring mag-iba +/- 1 byte bawat input.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transactions with higher priority are more likely to get included into a block.</source>
        <translation>Ang mga transaksyong may mas mataas na priyoridad ay mas malamang na maisama sa isang bloke.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This label turns red if the priority is smaller than &quot;medium&quot;.</source>
        <translation>Nagiging pula ang label na ito kung ang priyoridad ay mas maliit kaysa sa &quot;medium&quot;.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>This label turns red if any recipient receives an amount smaller than %1.</source>
        <translation>Nagiging pula ang label na ito kung ang sinumang tatanggap ay makakatanggap ng halagang mas maliit sa %1.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Can vary +/- %1 satoshi(s) per input.</source>
        <translation>Maaaring mag-iba +/- %1 (mga) satoshi bawat input.</translation>
    </message>
    <message>
        <location line="+48"/>
        <location line="+64"/>
        <source>(no label)</source>
        <translation>(alang label)</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>change from %1 (%2)</source>
        <translation>pagbabago mula sa %1 (%2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(change)</source>
        <translation>(baguhin)</translation>
    </message>
</context>
<context>
    <name>EditAddressDialog</name>
    <message>
        <location filename="../forms/editaddressdialog.ui" line="+14"/>
        <source>Edit Address</source>
        <translation>Alilan ing Address</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Label</source>
        <translation>&amp;Label</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The label associated with this address list entry</source>
        <translation>Ang label na nauugnay sa entry sa listahan ng address na ito</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Address</source>
        <translation>&amp;Address</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The address associated with this address list entry. This can only be modified for sending addresses.</source>
        <translation>Ang address na nauugnay sa entry sa listahan ng address na ito. Maaari lamang itong baguhin para sa pagpapadala ng mga address.</translation>
    </message>
    <message>
        <location filename="../editaddressdialog.cpp" line="+25"/>
        <source>New receiving address</source>
        <translation>Bayung address king pamagtanggap</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New sending address</source>
        <translation>Bayung address king pamagpadala</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit receiving address</source>
        <translation>Alilan ya ing address king pamagpadala</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit sending address</source>
        <translation>Alilan ya ing address king pamagpadala</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>The entered address &quot;%1&quot; is already in the address book.</source>
        <translation>Ing pepalub yung address &quot;%1&quot; ati na yu king aklat dareng address</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>The entered address &quot;%1&quot; is not a valid Nexa address.</source>
        <translation>Ing pepalub yung address &quot;%1&quot; ali ya katanggap-tanggap a Nexa address.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not unlock wallet.</source>
        <translation>Ali ya bisang mag-unlock ing wallet</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New key generation failed.</source>
        <translation>Memali ya ing pamangaua king key</translation>
    </message>
</context>
<context>
    <name>EditTokenDialog</name>
    <message>
        <location filename="../forms/edittokendialog.ui" line="+14"/>
        <source>Edit Token</source>
        <translation>I-edit ang Token</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Group ID</source>
        <translation>Group ID</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The group ID of the token to be edited.</source>
        <translation>Ang group ID ng token na ie-edit.</translation>
    </message>
    <message>
        <location filename="../edittokendialog.cpp" line="+25"/>
        <source>Add new token</source>
        <translation>Magdagdag ng bagong token</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove existing token</source>
        <translation>Alisin ang umiiral na token</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Token Dialog Error! Please notify a developer</source>
        <translation>Token Dialog Error! Mangyaring abisuhan ang isang developer</translation>
    </message>
    <message>
        <location line="+23"/>
        <location line="+20"/>
        <source>Group ID required.</source>
        <translation>Kinakailangan ang Group ID.</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Failed to add token: Invalid Group ID.</source>
        <translation>Nabigong magdagdag ng token: Invalid Group ID.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Failed to add token.</source>
        <translation>Nabigong magdagdag ng token.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Failed to remove token.</source>
        <translation>Nabigong alisin ang token.</translation>
    </message>
</context>
<context>
    <name>FreespaceChecker</name>
    <message>
        <location filename="../intro.cpp" line="+75"/>
        <source>A new data directory will be created.</source>
        <translation>Gagawa ng bagong direktoryo ng data.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>name</source>
        <translation>pangalan</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Directory already exists. Add %1 if you intend to create a new directory here.</source>
        <translation>Umiiral na ang direktoryo. Magdagdag ng %1 kung balak mong lumikha ng bagong direktoryo dito.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Path already exists, and is not a directory.</source>
        <translation>Umiiral na ang path, at hindi ito isang direktoryo.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot create data directory here.</source>
        <translation>Hindi makalikha ng direktoryo ng data dito.</translation>
    </message>
</context>
<context>
    <name>HelpMessageDialog</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+39"/>
        <source>version</source>
        <translation>bersion</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+2"/>
        <source>(%1-bit)</source>
        <translation>(%1-bit)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>About %1</source>
        <translation>Tungkul sa %1</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Command-line options</source>
        <translation>Pipamilian command-line</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Usage:</source>
        <translation>Pamanggamit:</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>command-line options</source>
        <translation>pipamilian command-line</translation>
    </message>
</context>
<context>
    <name>Intro</name>
    <message>
        <location filename="../forms/intro.ui" line="+14"/>
        <source>Welcome</source>
        <translation>Malaus ka</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Welcome to %1.</source>
        <translation>Maligayang pagdating sa %1.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>As this is the first time the program is launched, you can choose where %1 will store its data.</source>
        <translation>Dahil ito ang unang pagkakataon na inilunsad ang programa, maaari mong piliin kung saan iimbak ng %1 ang data nito.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 will download and store a copy of the Nexa block chain. At least %2GB of data will be stored in this directory, and it will grow over time. The wallet will also be stored in this directory.</source>
        <translation>Ang %1 ay magda-download at mag-imbak ng kopya ng Nexa block chain. Hindi bababa sa %2GB ng data ang maiimbak sa direktoryong ito, at lalago ito sa paglipas ng panahon. Ang pitaka ay maiimbak din sa direktoryong ito.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Use the default data directory</source>
        <translation>Gamitin ang default na direktoryo ng data</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use a custom data directory:</source>
        <translation>Gumamit ng custom na direktoryo ng data:</translation>
    </message>
    <message>
        <location filename="../intro.cpp" line="+81"/>
        <source>Error: Specified data directory &quot;%1&quot; cannot be created.</source>
        <translation>Error: Hindi malikha ang tinukoy na direktoryo ng data na &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error</source>
        <translation>Mali</translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%n GB of free space available</source>
        <translation>
            <numerusform>%n GB ng libreng espasyo na available</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>(of %n GB needed)</source>
        <translation>
            <numerusform>(ng %n GB ang kailangan)</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessThanValidator</name>
    <message>
        <location filename="../unlimiteddialog.cpp" line="+486"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>Hindi maaaring blangko ang mga parameter sa paghubog ng upstream na trapiko</translation>
    </message>
</context>
<context>
    <name>ModalOverlay</name>
    <message>
        <location filename="../forms/modaloverlay.ui" line="+14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet. This means that recent transactions will not be visible, and the balance will not be up-to-date until this process has completed.</source>
        <translation>Maaaring luma na ang ipinapakitang impormasyon. Awtomatikong nagsi-synchronize ang iyong pitaka sa network ng Nexa pagkatapos maitatag ang isang koneksyon, ngunit hindi pa nakumpleto ang prosesong ito. Nangangahulugan ito na ang mga kamakailang transaksyon ay hindi makikita, at ang balanse ay hindi magiging up-to-date hanggang sa makumpleto ang prosesong ito.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Spending coins may not be possible during that phase!</source>
        <translation>Maaaring hindi posible ang paggastos ng mga barya sa yugtong iyon!</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Amount of blocks left</source>
        <translation>Dami ng mga bloke na natitira</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+26"/>
        <source>unknown...</source>
        <translation>e miya balu...</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Last block time</source>
        <translation>Huling block time</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Progress</source>
        <translation>Pag-unlad</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>~</source>
        <translation>~</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Progress increase per Hour</source>
        <translation>Pagtaas ng progreso kada Oras</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+20"/>
        <source>calculating...</source>
        <translation>pagkalkula...</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Estimated time left until synced</source>
        <translation>Tinantyang oras ang natitira hanggang sa ma-sync</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Hide</source>
        <translation>Tago</translation>
    </message>
    <message>
        <location filename="../modaloverlay.cpp" line="+140"/>
        <source>Unknown. Reindexing (%1)...</source>
        <translation>Hindi kilala. Muling ini-index (%1)...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Reindexing...</source>
        <translation>Hindi kilala. Muling ini-index...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown. Syncing Headers (%1)...</source>
        <translation>Hindi kilala. Sini-sync ang mga Header (%1)...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Syncing Headers...</source>
        <translation>Hindi kilala. Sini-sync ang mga Header...</translation>
    </message>
</context>
<context>
    <name>OpenURIDialog</name>
    <message>
        <location filename="../forms/openuridialog.ui" line="+14"/>
        <source>Open URI</source>
        <translation>Buksan ang URI</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open payment request from URI or file</source>
        <translation>Buksan ang kahilingan sa pagbabayad mula sa URI o file</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>URI:</source>
        <translation>URI:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Select payment request file</source>
        <translation>Piliin ang file ng kahilingan sa pagbabayad</translation>
    </message>
    <message>
        <location filename="../openuridialog.cpp" line="+40"/>
        <source>Select payment request file to open</source>
        <translation>Piliin ang file ng kahilingan sa pagbabayad na bubuksan</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../forms/optionsdialog.ui" line="+14"/>
        <source>Options</source>
        <translation>Pipamilian</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Main</source>
        <translation>&amp;Pun</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Automatically start %1 after logging in to the system.</source>
        <translation>Awtomatikong simulan ang %1 pagkatapos mag-log in sa system.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Start %1 on system login</source>
        <translation>&amp;Simulan ang %1 sa system login</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a, one time only, full database reindex on the next startup.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Awtomatikong magpasimula ng, isang beses lang, buong database reindex sa susunod na startup.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindex on startup</source>
        <translation>I-reindex sa pagsisimula</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Number of script &amp;verification threads</source>
        <translation>Bilang ng mga thread ng script at pagpapatunay</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>(0 = auto, &lt;0 = leave that many cores free)</source>
        <translation>(0 = auto, &lt;0 = iwanang libre ang maraming core)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a full blockchain resynchronization on the next startup (one time only).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Awtomatikong magpasimula ng buong blockchain resynchronization sa susunod na startup (isang beses lang).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Resynchronize block data on startup</source>
        <translation>Muling i-synchronize ang block data sa startup</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>W&amp;allet</source>
        <translation>W&amp;allet</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Expert</source>
        <translation>Dalubhasa</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Whether to show coin control features or not.</source>
        <translation>Kung magpapakita ng mga feature ng coin control o hindi.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Enable coin &amp;control features</source>
        <translation>Paganahin ang mga feature ng coin at control</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>If you disable the spending of unconfirmed change, the change from a transaction cannot be used until that transaction has at least one confirmation. This also affects how your balance is computed.</source>
        <translation>Kung hindi mo pinagana ang paggastos ng hindi nakumpirmang pagbabago, ang pagbabago mula sa isang transaksyon ay hindi magagamit hanggang sa ang transaksyong iyon ay may kahit isang kumpirmasyon. Naaapektuhan din nito kung paano kinukuwenta ang iyong balanse.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Spend unconfirmed change</source>
        <translation>&amp;Gumastos ng hindi kumpirmadong pagbabago</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Instant Transactions is enabled you can spend unconfirmed transactions immediately.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Kapag pinagana ang Instant Transactions, maaari kang gumastos kaagad ng mga hindi nakumpirmang transaksyon.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Instant Transactions</source>
        <translation>&amp;Mga Instant na Transaksyon</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When creating and sending transactions, auto consolidate will, if required, automatically create a chain of transactions which have inputs no greater than the consensus input limit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Kapag gumagawa at nagpapadala ng mga transaksyon, ang auto consolidate, kung kinakailangan, ay awtomatikong gagawa ng chain ng mga transaksyon na may mga input na hindi hihigit sa consensus input limit.&lt;/p&gt;&lt;/body &gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Auto Consolidate</source>
        <translation>Awtomatiko pagsamahin</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>&amp;Rescan wallet on startup</source>
        <translation>&amp;I-scan muli ang wallet sa startup</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Token Whitelist is enabled you can control what tokens appear in your wallet.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Kapag pinagana ang Token Whitelist, makokontrol mo kung anong mga token ang lalabas sa iyong wallet.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Token Whitelist</source>
        <translation>&amp;Token Whitelist</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Network</source>
        <translation>&amp;Network</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Automatically open the Nexa client port on the router. This only works when your router supports UPnP and it is enabled.</source>
        <translation>Ibuklat yang antimanu ing Nexa client port king router. Gagana yamu ini istung ing router mu susuporta yang UPnP at magsilbi ya.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Map port using &amp;UPnP</source>
        <translation>Mapa ng ning port gamit ing &amp;UPnP</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Accept connections from outside.</source>
        <translation>Tanggapin ang mga koneksyon mula sa labas.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Allow incoming connections</source>
        <translation>Payagan ang mga papasok na koneksyon</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Connect to the Nexa network through a SOCKS5 proxy.</source>
        <translation>Kumonekta sa Nexa network sa pamamagitan ng SOCKS5 proxy.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Connect through SOCKS5 proxy (default proxy):</source>
        <translation>&amp;Kumonekta sa pamamagitan ng SOCKS5 proxy (default proxy):</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+187"/>
        <source>Proxy &amp;IP:</source>
        <translation>Proxy &amp;IP:</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>IP address of the proxy (e.g. IPv4: 127.0.0.1 / IPv6: ::1)</source>
        <translation>IP address ng proxy (hal. IPv4: 127.0.0.1 / IPv6: ::1)</translation>
    </message>
    <message>
        <location line="-180"/>
        <location line="+187"/>
        <source>&amp;Port:</source>
        <translation>&amp;Port:</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>Port of the proxy (e.g. 9050)</source>
        <translation>Port na ning proxy(e.g. 9050)</translation>
    </message>
    <message>
        <location line="-163"/>
        <source>Used for reaching peers via:</source>
        <translation>Ginagamit para maabot ang mga kapantay sa pamamagitan ng:</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+23"/>
        <location line="+23"/>
        <source>Shows if the supplied default SOCKS5 proxy is used to reach peers via this network type.</source>
        <translation>Ipinapakita kung ang ibinigay na default na SOCKS5 proxy ay ginagamit upang maabot ang mga kapantay sa pamamagitan ng ganitong uri ng network.</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tor</source>
        <translation>Tor</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Connect to the Nexa network through a separate SOCKS5 proxy for Tor hidden services.</source>
        <translation>Kumonekta sa Nexa network sa pamamagitan ng isang hiwalay na SOCKS5 proxy para sa Tor hidden services.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Use separate SOCKS5 proxy to reach peers via Tor hidden services:</source>
        <translation>Gumamit ng hiwalay na SOCKS5 proxy para maabot ang mga kapantay sa pamamagitan ng Tor hidden services:</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>&amp;Window</source>
        <translation>&amp;Awang</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show only a tray icon after minimizing the window.</source>
        <translation>Ipakit mu ing tray icon kaibat meng pelatian ing awang.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Minimize to the tray instead of the taskbar</source>
        <translation>&amp;Latian ya ing tray kesa king taskbar</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Minimize instead of exit the application when the window is closed. When this option is enabled, the application will be closed only after selecting Exit in the menu.</source>
        <translation>I-minimize sa halip na lumabas sa application kapag sarado ang window. Kapag pinagana ang opsyong ito, isasara lang ang application pagkatapos piliin ang Exit sa menu.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>M&amp;inimize on close</source>
        <translation>P&amp;alatian istung isara</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Display</source>
        <translation>&amp;Ipalto</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>User Interface &amp;language:</source>
        <translation>Amanu na ning user interface:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The user interface language can be set here. This setting will take effect after restarting %1.</source>
        <translation>Maaaring itakda ang wika ng user interface dito. Magkakabisa ang setting na ito pagkatapos i-restart ang %1.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Unit to show amounts in:</source>
        <translation>Ing &amp;Unit a ipakit king alaga ning:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Choose the default subdivision unit to show in the interface and when sending coins.</source>
        <translation>Pilinan ing default subdivision unit a ipalto o ipakit king interface at istung magpadala kang barya.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+13"/>
        <source>Third party URLs (e.g. a block explorer) that appear in the transactions tab as context menu items. %s in the URL is replaced by transaction hash. Multiple URLs are separated by vertical bar |.</source>
        <translation>Mga third party na URL (hal. isang block explorer) na lumalabas sa tab ng mga transaksyon bilang mga item sa menu ng konteksto. Ang %s sa URL ay pinalitan ng hash ng transaksyon. Ang maramihang mga URL ay pinaghihiwalay ng vertical bar |.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Third party transaction URLs</source>
        <translation>Mga URL ng transaksyon ng third party</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Active command-line options that override above options:</source>
        <translation>Mga aktibong opsyon sa command-line na nag-o-override sa mga opsyon sa itaas:</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Reset all client options to default.</source>
        <translation>I-reset ang lahat ng mga opsyon ng kliyente sa default.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>&amp;I-reset ang Mga Opsyon</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>I-&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../optionsdialog.cpp" line="+135"/>
        <source>default</source>
        <translation>default</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>none</source>
        <translation>wala</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Confirm options reset</source>
        <translation>Kumpirmahin ang pag-reset ng mga opsyon</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+27"/>
        <source>Client restart required to activate changes.</source>
        <translation>Kinakailangan ang pag-restart ng kliyente upang maisaaktibo ang mga pagbabago.</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>Isasara ang kliyente. Gusto mo bang magpatuloy?</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>This change would require a client restart.</source>
        <translation>Ang pagbabagong ito ay mangangailangan ng pag-restart ng kliyente.</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>The supplied proxy address is invalid.</source>
        <translation>Ing milageng proxy address eya katanggap-tanggap.</translation>
    </message>
</context>
<context>
    <name>OverviewPage</name>
    <message>
        <location filename="../forms/overviewpage.ui" line="+14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Balances</source>
        <translation>Mga balanse</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+386"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet.</source>
        <translation>Ing makaltong impormasion mapalyaring luma ne. Ing kekang wallet otomatiku yang mag-synchronize keng Nexa network istung mekakonekta ne king network, oneng ing prosesung ini ali ya pa kumpletu.</translation>
    </message>
    <message>
        <location line="-333"/>
        <source>Unconfirmed transactions to watch-only addresses</source>
        <translation>Mga hindi kumpirmadong transaksyon sa mga watch-only na address</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Mined balance in watch-only addresses that has not yet matured</source>
        <translation>Namimina ang balanse sa mga address na panoorin lamang na hindi pa matured</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Current total balance in watch-only addresses</source>
        <translation>Kasalukuyang kabuuang balanse sa mga watch-only na address</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Watch-only:</source>
        <translation>Panoorin lamang:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Available:</source>
        <translation>Panoorin lamang:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Your current spendable balance</source>
        <translation>Ing kekang kasalungsungan balanse a malyari mung gastusan</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Your current balance in watch-only addresses</source>
        <translation>Ang iyong kasalukuyang balanse sa mga watch-only na address</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Pending:</source>
        <translation>Nakabinbin:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Spendable:</source>
        <translation>Magastos:</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Recent transactions</source>
        <translation>Mga kamakailang transaksyon</translation>
    </message>
    <message>
        <location line="-292"/>
        <source>Total of transactions that have yet to be confirmed, and do not yet count toward the spendable balance</source>
        <translation>Ing kabuuan dareng transaksion a kasalungsungan ali pa me-kumpirma, at kasalungsungan ali pa mebilang kareng kekang balanseng malyari mung gastusan</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Immature:</source>
        <translation>Immature:</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Mined balance that has not yet matured</source>
        <translation>Reng me-minang balanse a epa meg-matured</translation>
    </message>
    <message>
        <location line="-16"/>
        <source>Total:</source>
        <translation>Kabuuan:</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Your current total balance</source>
        <translation>Ing kekang kasalungsungan kabuuang balanse</translation>
    </message>
</context>
<context>
    <name>PaymentServer</name>
    <message>
        <location filename="../paymentserver.cpp" line="+375"/>
        <location line="+240"/>
        <location line="+47"/>
        <location line="+126"/>
        <location line="+15"/>
        <location line="+15"/>
        <source>Payment request error</source>
        <translation></translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Cannot start click-to-pay handler</source>
        <translation></translation>
    </message>
    <message>
        <location line="+95"/>
        <location line="+13"/>
        <location line="+10"/>
        <source>URI handling</source>
        <translation></translation>
    </message>
    <message>
        <location line="-23"/>
        <source>Payment request fetch URL is invalid: %1</source>
        <translation></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Invalid payment address %1</source>
        <translation></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>URI cannot be parsed! This can be caused by an invalid Nexa address or malformed URI parameters.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Payment request file handling</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Payment request file cannot be read! This can be caused by an invalid payment request file.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+65"/>
        <location line="+11"/>
        <location line="+35"/>
        <location line="+12"/>
        <location line="+21"/>
        <location line="+98"/>
        <source>Payment request rejected</source>
        <translation></translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Payment request network doesn&apos;t match client network.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Payment request expired.</source>
        <translation>Nag-expire ang kahilingan sa pagbabayad.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Payment request is not initialized.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Unverified payment requests to custom payment scripts are unsupported.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+21"/>
        <source>Invalid payment request.</source>
        <translation></translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Requested payment amount of %1 is too small (considered dust).</source>
        <translation></translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Refund from %1</source>
        <translation></translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Payment request %1 is too large (%2 bytes, allowed %3 bytes).</source>
        <translation></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error communicating with %1: %2</source>
        <translation></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Payment request cannot be parsed!</source>
        <translation></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Bad response from server %1</source>
        <translation></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Network request error</source>
        <translation></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Payment acknowledged</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PeerTableModel</name>
    <message>
        <location filename="../peertablemodel.cpp" line="+120"/>
        <source>Node/Service</source>
        <translation>Node/Serbisyo</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation>Ahente ng Gumagamit</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ping Time</source>
        <translation>Oras ng Ping</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../nexaunits.cpp" line="+187"/>
        <source>Amount</source>
        <translation>Alaga</translation>
    </message>
    <message>
        <location filename="../guiutil.cpp" line="+135"/>
        <source>Enter a NEXA address (e.g. %1)</source>
        <translation>Maglagay ng NEXA address (hal. %1)</translation>
    </message>
    <message>
        <location line="+831"/>
        <source>%1 d</source>
        <translation>%1 d</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 h</source>
        <translation>%1 h</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 m</source>
        <translation>%1 m</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+68"/>
        <source>%1 s</source>
        <translation>%1 s</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>None</source>
        <translation>Wala</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 ms</source>
        <translation>%1 ms</translation>
    </message>
    <message numerus="yes">
        <location line="+19"/>
        <source>%n seconds(s)</source>
        <translation>
            <numerusform>%n segundo</numerusform>
            <numerusform>%n segundo</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n minutes(s)</source>
        <translation>
            <numerusform>%n minuto</numerusform>
            <numerusform>%n minuto</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n hour(s)</source>
        <translation>
            <numerusform>%n oras</numerusform>
            <numerusform>%n oras</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n day(s)</source>
        <translation>
            <numerusform>%n araw</numerusform>
            <numerusform>%n araw</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <location line="+8"/>
        <source>%n week(s)</source>
        <translation>
            <numerusform>%n linggo</numerusform>
            <numerusform>%n linggo</numerusform>
        </translation>
    </message>
    <message>
        <location line="-2"/>
        <source>%1 and %2</source>
        <translation>%1 at %2</translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%n year(s)</source>
        <translation>
            <numerusform>%n taon</numerusform>
            <numerusform>%n taon</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>QRImageWidget</name>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+36"/>
        <source>&amp;Save Image...</source>
        <translation>&amp;I-save ang Larawan...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy Image</source>
        <translation>&amp;Kopyahin ang Larawan</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Save QR Code</source>
        <translation>I-save ang QR Code</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>PNG Image (*.png)</source>
        <translation>PNG na Larawan (*.png)</translation>
    </message>
</context>
<context>
    <name>RPCConsole</name>
    <message>
        <location filename="../forms/debugwindow.ui" line="+239"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+17"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+19"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+1753"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+23"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location line="-2715"/>
        <source>Client version</source>
        <translation>Bersion ning Cliente</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>&amp;Information</source>
        <translation>&amp;Impormasion</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Debug window</source>
        <translation>I-Debug ing awang</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Startup time</source>
        <translation>Oras ning umpisa</translation>
    </message>
    <message>
        <location line="+474"/>
        <source>Network</source>
        <translation>Network</translation>
    </message>
    <message>
        <location line="-460"/>
        <source>Number of connections</source>
        <translation>Bilang dareng koneksion</translation>
    </message>
    <message>
        <location line="-41"/>
        <location line="+2390"/>
        <source>User Agent</source>
        <translation>Ahente ng Gumagamit</translation>
    </message>
    <message>
        <location line="-2380"/>
        <source>Using BerkeleyDB version</source>
        <translation>Gamit ang bersyon ng BerkeleyDB</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Datadir</source>
        <translation>Direktoryo ng data</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Name</source>
        <translation>Pangalan</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Last block time (time since)</source>
        <translation>Huling block time (oras simula)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Last block size</source>
        <translation>Huling sukat ng bloke</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Tx pool</source>
        <translation>Mga transaksyon sa Tx pool</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Orphan pool</source>
        <translation>Mga transaksyon sa Orphan pool</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Messages in CAPD pool</source>
        <translation>Mga mensahe sa CAPD pool</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - usage</source>
        <translation>Tx pool - paggamit</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - txns per second</source>
        <translation>Tx pool - txns bawat segundo</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (Totals)</source>
        <translation>XThin (Mga Kabuuan)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (24-Hour Averages)</source>
        <translation>XThin (24-Oras na Average)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (Totals)</source>
        <translation>Compact (Mga Kabuuan)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (24-Hour Averages)</source>
        <translation>Compact (24-Oras na Average)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (Totals)</source>
        <translation>Graphene (Mga Kabuuan)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (24-Hour Averages)</source>
        <translation>Graphene (24-Oras na Average)</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Open the %1 debug log file from the current data directory. This can take a few seconds for large log files.</source>
        <translation>Buksan ang %1 debug log file mula sa kasalukuyang direktoryo ng data. Maaaring tumagal ito ng ilang segundo para sa malalaking log file.</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Block Propagation</source>
        <translation>Block Propagation</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Transaction Pools</source>
        <translation>Mga Pool ng Transaksyon</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Block chain</source>
        <translation>Block chain</translation>
    </message>
    <message>
        <location line="+476"/>
        <location line="+1694"/>
        <source>Received</source>
        <translation>Natanggap</translation>
    </message>
    <message>
        <location line="-1614"/>
        <location line="+1591"/>
        <source>Sent</source>
        <translation>Ipinadala</translation>
    </message>
    <message>
        <location line="-1550"/>
        <source>&amp;Transaction Rate</source>
        <translation>&amp;Rate ng Transaksyon</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Instantaneous Rate (1s)</source>
        <translation>Agad na Rate (1s)</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+536"/>
        <source>Peak</source>
        <translation>Tuktok</translation>
    </message>
    <message>
        <location line="-463"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Runtime</source>
        <translation>Runtime</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>24-Hours</source>
        <translation>24-Oras</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Displayed</source>
        <translation>Ipinakita</translation>
    </message>
    <message>
        <location line="-769"/>
        <location line="+536"/>
        <source>Average</source>
        <translation>Katamtaman</translation>
    </message>
    <message>
        <location line="-265"/>
        <source>Smoothed Rate (60s)</source>
        <translation>Smoothed Rate (60s)</translation>
    </message>
    <message>
        <location line="+539"/>
        <source>&amp;Peers</source>
        <translation>&amp;Mga kapantay</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Banned peers</source>
        <translation>Pinagbawalan ang mga kapantay</translation>
    </message>
    <message>
        <location line="+57"/>
        <location filename="../rpcconsole.cpp" line="+311"/>
        <location line="+856"/>
        <source>Select a peer to view detailed information.</source>
        <translation>Pumili ng peer para tingnan ang detalyadong impormasyon.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Whitelisted</source>
        <translation>Whitelisted</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Direction</source>
        <translation>Direksyon</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Version</source>
        <translation>Bersion</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Services</source>
        <translation>Mga serbisyo</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Starting Block</source>
        <translation>Panimulang Block</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Headers</source>
        <translation>Mga naka-sync na Header</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Blocks</source>
        <translation>Mga Naka-sync na Block</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Ban Score</source>
        <translation>Ban Score</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Connection Time</source>
        <translation>Oras ng Koneksyon</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Send</source>
        <translation>Huling Pagpapadala</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Receive</source>
        <translation>Huling Pagtanggap</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Ping Time</source>
        <translation>Oras ng Ping</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>The duration of a currently outstanding ping.</source>
        <translation>Ang tagal ng kasalukuyang natitirang ping.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ping Wait</source>
        <translation>Ping Wait</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Time Offset</source>
        <translation>Time Offset</translation>
    </message>
    <message>
        <location line="-2650"/>
        <source>Current number of blocks</source>
        <translation>Kasalungsungan bilang dareng blocks</translation>
    </message>
    <message>
        <location line="+126"/>
        <source>&amp;Open</source>
        <translation>&amp;Ibuklat</translation>
    </message>
    <message>
        <location line="+423"/>
        <source>General</source>
        <translation>Heneral</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Console</source>
        <translation>&amp;Console</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Decrease font size</source>
        <translation>Bawasan ang laki ng font</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Increase font size</source>
        <translation>Palakihin ang laki ng font</translation>
    </message>
    <message>
        <location line="+127"/>
        <source>&amp;Network Traffic</source>
        <translation>&amp;Trapiko ng Network</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>&amp;Clear</source>
        <translation>&amp;I-Clear</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Totals</source>
        <translation>Kabuuan:</translation>
    </message>
    <message>
        <location line="-704"/>
        <source>Debug log file</source>
        <translation>Debug log file</translation>
    </message>
    <message>
        <location line="+541"/>
        <source>Clear console</source>
        <translation>I-Clear ing console</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-736"/>
        <source>&amp;Disconnect Node</source>
        <translation>&amp;Idiskonekta ang Node</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Ban Node for</source>
        <translation>Ban Node para sa</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>1 &amp;hour</source>
        <translation>1 &amp;oras</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;day</source>
        <translation>1 &amp;araw</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;week</source>
        <translation>1 &amp;linggo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;year</source>
        <translation>1 &amp;taon</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>&amp;Unban Node</source>
        <translation>I-&amp;unban ang Node</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Welcome to the %1 RPC console.</source>
        <translation>Maligayang pagdating sa %1 RPC console.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Use up and down arrows to navigate history, and &lt;b&gt;Ctrl-L&lt;/b&gt; to clear screen.</source>
        <translation>Gamitan me ing patas at pababang arrow para alibut me ing kasalesayan, at &lt;b&gt;Ctrl-L&lt;/b&gt; ban I-clear ya ing screen.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type &lt;b&gt;help&lt;/b&gt; for an overview of available commands.</source>
        <translation>I-type ing &lt;b&gt;help&lt;/b&gt; ban akit la reng ati at magsilbing commands.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>In:</source>
        <translation>Sa:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Out:</source>
        <translation>Palabas:</translation>
    </message>
    <message>
        <location line="+90"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <source>Disabled</source>
        <translation>Hindi pinagana</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>%1 B</source>
        <translation>%1 B</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>(node id: %1)</source>
        <translation>(node ​​id: %1)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>via %1</source>
        <translation>via %1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>never</source>
        <translation>hindi kailanman</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Inbound</source>
        <translation>Papasok</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Outbound</source>
        <translation>Papalabas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Oo</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Hindi</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+6"/>
        <source>Unknown</source>
        <translation>E miya balu</translation>
    </message>
</context>
<context>
    <name>ReceiveCoinsDialog</name>
    <message>
        <location filename="../forms/receivecoinsdialog.ui" line="+100"/>
        <source>&amp;Request payment</source>
        <translation>&amp;Humiling ng bayad</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Clear all fields of the form.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>I-Clear</translation>
    </message>
    <message>
        <location line="-77"/>
        <location line="+120"/>
        <source>An optional amount to request. Leave this empty or zero to not request a specific amount.</source>
        <translation>Isang opsyonal na halaga na hihilingin. Iwanan itong walang laman o zero para hindi humiling ng partikular na halaga.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Amount:</source>
        <translation>&amp;Alaga:</translation>
    </message>
    <message>
        <location line="-116"/>
        <location line="+106"/>
        <source>An optional label to associate with the new receiving address.</source>
        <translation>Isang opsyonal na label na iuugnay sa bagong receiving address.</translation>
    </message>
    <message>
        <location line="-103"/>
        <source>&amp;Label:</source>
        <translation>&amp;Label:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Reuse one of the previously used receiving addresses. Reusing addresses has security and privacy issues. Do not use this unless re-generating a payment request made before.</source>
        <translation>Muling gamitin ang isa sa mga dating ginamit na receiving address. Ang muling paggamit ng mga address ay may mga isyu sa seguridad at privacy. Huwag gamitin ito maliban kung muling bubuo ng kahilingan sa pagbabayad na ginawa noon.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>R&amp;euse an existing receiving address (not recommended)</source>
        <translation>Gumamit muli ng umiiral nang tumatanggap na address (hindi inirerekomenda)</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Use this form to request payments. All fields are &lt;b&gt;optional&lt;/b&gt;.</source>
        <translation>Gamitin ang form na ito para humiling ng mga pagbabayad. Lahat ng mga field ay &lt;b&gt;opsyonal&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="-103"/>
        <location line="+110"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened. Note: The message will not be sent with the payment over the Nexa network.</source>
        <translation>Isang opsyonal na mensahe upang ilakip sa kahilingan sa pagbabayad, na ipapakita kapag binuksan ang kahilingan. Tandaan: Ang mensahe ay hindi ipapadala kasama ang pagbabayad sa network ng Nexa.</translation>
    </message>
    <message>
        <location line="-107"/>
        <source>&amp;Message:</source>
        <translation>&amp;Mensayi</translation>
    </message>
    <message>
        <location line="+153"/>
        <source>Requested payments history</source>
        <translation>Kasaysayan ng mga hiniling na pagbabayad</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Show the selected request (does the same as double clicking an entry)</source>
        <translation>Ipakita ang napiling kahilingan (katulad ng pag-double click sa isang entry)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show</source>
        <translation>Ipakita</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Remove the selected entries from the list</source>
        <translation>Alisin ang mga napiling entry mula sa listahan</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove</source>
        <translation>Alisin</translation>
    </message>
    <message>
        <location filename="../receivecoinsdialog.cpp" line="+50"/>
        <source>Copy URI</source>
        <translation>Buksan ang URI</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Kopyan ing label</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy message</source>
        <translation>Kopyahin ang mensahe</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Kopyan ing alaga</translation>
    </message>
</context>
<context>
    <name>ReceiveRequestDialog</name>
    <message>
        <location filename="../forms/receiverequestdialog.ui" line="+29"/>
        <source>QR Code</source>
        <translation>QR Code</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Copy &amp;URI</source>
        <translation>Kopyan ing &amp;URI</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Copy &amp;Address</source>
        <translation>&amp;Kopyan ing address</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Save Image...</source>
        <translation>&amp;I-save ang Larawan...</translation>
    </message>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+78"/>
        <source>Request payment to %1</source>
        <translation>Humiling ng bayad sa %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment information</source>
        <translation>Impormasyon sa Pagbabayad</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address</source>
        <translation>Address</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount</source>
        <translation>Alaga</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Message</source>
        <translation>Mensayi</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Resulting URI too long, try to reduce the text for label / message.</source>
        <translation>Masyadong mahaba ang nagreresultang URI, subukang bawasan ang text para sa label / mensahe.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error encoding URI into QR Code.</source>
        <translation>Masyadong mahaba ang nagreresultang URI, subukang bawasan ang text para sa label / mensahe.</translation>
    </message>
</context>
<context>
    <name>RecentRequestsTableModel</name>
    <message>
        <location filename="../recentrequeststablemodel.cpp" line="+30"/>
        <source>Date</source>
        <translation>Kaaldauan</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message</source>
        <translation>Mensayi</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>(no message)</source>
        <translation>(walang Mensahe)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>(no amount)</source>
        <translation>(walang halaga)</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Amount</source>
        <translation>Alaga</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>(no label)</source>
        <translation>(alang label)</translation>
    </message>
</context>
<context>
    <name>SendCoinsDialog</name>
    <message>
        <location filename="../forms/sendcoinsdialog.ui" line="+14"/>
        <location filename="../sendcoinsdialog.cpp" line="+626"/>
        <source>Send Coins</source>
        <translation>Magpadalang Barya</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Coin Control Features</source>
        <translation>Mga Tampok ng Coin Control</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Select specific coins you want to use in this transaction</source>
        <translation>Pumili ng mga partikular na barya na gusto mong gamitin sa transaksyong ito</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Inputs...</source>
        <translation>Mga input...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>automatically selected</source>
        <translation>awtomatikong napili</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Insufficient funds!</source>
        <translation>Kulang a pondo</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Quantity:</source>
        <translation>Dami:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Bytes:</source>
        <translation>Bytes:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Amount:</source>
        <translation>Alaga:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Priority:</source>
        <translation>Priyoridad:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Fee:</source>
        <translation>Bayad:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation>Alikabok:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation>Pagkatapos ng Bayad:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Baguhin:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>If this is activated, but the change address is empty or invalid, change will be sent to a newly generated address.</source>
        <translation>Kung ito ay isinaaktibo, ngunit ang pagbabago ng address ay walang laman o hindi wasto, ang pagbabago ay ipapadala sa isang bagong nabuong address.</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+16"/>
        <source>Custom change address</source>
        <translation>Pasadyang pagbabago ng address</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>Transaction Fee:</source>
        <translation>Bayad king Transaksion:</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Choose your transaction fee.</source>
        <translation>Piliin ang iyong bayad sa transaksyon.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose...</source>
        <translation>Pilinan...</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>collapse fee-settings</source>
        <translation>pagbagsak ng mga setting ng bayad</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hide</source>
        <translation>Tago</translation>
    </message>
    <message>
        <location line="+48"/>
        <location line="+16"/>
        <source>If the custom fee is set to 1000 satoshis and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 satoshis in fee, while &quot;total at least&quot; pays 1000 satoshis. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation>Kung ang custom na bayarin ay itinakda sa 1000 satoshis at ang transaksyon ay 250 bytes lamang, ang &quot;bawat kilobyte&quot; ay magbabayad lamang ng 250 satoshis bilang bayad, habang ang &quot;kabuuang hindi bababa sa&quot; ay nagbabayad ng 1000 satoshis. Para sa mga transaksyong mas malaki sa isang kilobyte, parehong nagbabayad ng kilobyte.</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>per kilobyte</source>
        <translation>bawat kilobyte</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>total at least</source>
        <translation>kabuuang hindi bababa sa</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Custom fee amount</source>
        <translation>Pasadyang halaga ng bayad</translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+13"/>
        <source>Paying only the minimum fee is just fine as long as there is less transaction volume than space in the blocks. But be aware that this can end up in a never confirming transaction once there is more demand for nexa transactions than the network can process.</source>
        <translation>Ang pagbabayad lamang ng pinakamababang bayad ay ayos lang hangga&apos;t may mas kaunting dami ng transaksyon kaysa sa espasyo sa mga bloke. Ngunit magkaroon ng kamalayan na ito ay maaaring mauwi sa isang transaksyong hindi kailanman nagkukumpirma kapag may higit na pangangailangan para sa mga transaksyong nexa kaysa sa maaaring iproseso ng network.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>(read the tooltip)</source>
        <translation>(basahin ang tooltip)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Use the recommended fee amount.  You can select a faster or slower confirmation time by moving the &quot;Confirmation Time&quot; slider.</source>
        <translation>Gamitin ang inirekumendang halaga ng bayad. Maaari kang pumili ng mas mabilis o mas mabagal na oras ng pagkumpirma sa pamamagitan ng paggalaw sa slider ng &quot;Oras ng Pagkumpirma.&quot;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Recommended:</source>
        <translation>Inirerekomenda:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Choose a custom fee amount</source>
        <translation>Pumili ng custom na halaga ng bayad</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Custom:</source>
        <translation>Custom:</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>(Smart fee not initialized yet. This usually takes a few blocks...)</source>
        <translation>(Hindi pa nasisimulan ang smart fee. Karaniwan itong tumatagal ng ilang bloke...)</translation>
    </message>
    <message>
        <location line="+29"/>
        <location line="+33"/>
        <source>Transaction confirmation time</source>
        <translation>Oras ng pagkumpirma ng transaksyon</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Confirmation time:</source>
        <translation>Oras ng pagkumpirma:</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>fast</source>
        <translation>mabilis</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Send as zero-fee transaction if possible</source>
        <translation>Ipadala bilang walang bayad na transaksyon kung maaari</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(confirmation may take longer)</source>
        <translation>(maaaring mas matagal ang kumpirmasyon)</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>Clear all fields of the form.</source>
        <translation>I-clear ang lahat ng field ng form.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Send to multiple recipients at once</source>
        <translation>Misanang magpadala kareng alialiuang tumanggap</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add &amp;Recipient</source>
        <translation>Maglage &amp;Tumanggap</translation>
    </message>
    <message>
        <location line="-17"/>
        <source>Clear &amp;All</source>
        <translation>I-Clear &amp;Eganagana</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Current Balance</source>
        <translation>Kasalukuyang balanse</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Balance:</source>
        <translation>Balanse:</translation>
    </message>
    <message>
        <location line="-87"/>
        <source>Confirm the send action</source>
        <translation>Kumpirman ing aksion king pamagpadala</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;end</source>
        <translation>Ipadala</translation>
    </message>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="-231"/>
        <source>Confirm send coins</source>
        <translation>Kumpirman ing pamagpadalang barya</translation>
    </message>
    <message>
        <location line="-326"/>
        <source>Copy amount</source>
        <translation>Kopyan ing alaga</translation>
    </message>
    <message>
        <location line="-1"/>
        <source>Copy quantity</source>
        <translation>Dami ng kopya</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy fee</source>
        <translation>Bayad sa pagkopya</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Kopyahin pagkatapos ng bayad</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Kopyan ing bytes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Kopyahin ang priyoridad</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation>Kopyahin ang alikabok</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>Kopyahin ang pagbabago</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>&lt;b&gt;Public label:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Public label:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+4"/>
        <source>%1 to %2</source>
        <translation>%1 sa %2</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Are you sure you want to send?</source>
        <translation>Sigurado ka bang gusto mong magpadala?</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>added as transaction fee</source>
        <translation>idinagdag bilang bayad sa transaksyon</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Total Amount %1</source>
        <translation>Kabuuang Halaga %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>or</source>
        <translation>o</translation>
    </message>
    <message>
        <location line="+187"/>
        <source>The recipient address is not valid. Please recheck.</source>
        <translation>Ang address ng tatanggap ay hindi wasto. Mangyaring suriin muli.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount to pay must be larger than 0.</source>
        <translation>Ing alaga na ning bayaran dapat mung mas matas ya king 0.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount exceeds your balance.</source>
        <translation>Ing alaga mipasobra ya king kekang balanse.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The total exceeds your balance when the %1 transaction fee is included.</source>
        <translation>Ing kabuuan mipasobra ya king kekang balanse istung inabe ya ing %1 a bayad king transaksion </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Duplicate address found: addresses should only be used once each.</source>
        <translation>May nakitang duplicate na address: isang beses lang dapat gamitin ang mga address sa bawat isa.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction creation failed!</source>
        <translation>Nabigo ang paggawa ng transaksyon!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The transaction was rejected! This might happen if some of the coins in your wallet were already spent, such as if you used a copy of wallet.dat and coins were spent in the copy but not marked as spent here.</source>
        <translation>Tinanggihan ang transaksyon! Maaaring mangyari ito kung nagastos na ang ilan sa mga barya sa iyong wallet, gaya ng kung gumamit ka ng kopya ng wallet.dat at ang mga barya ay ginastos sa kopya ngunit hindi minarkahan bilang ginastos dito.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A fee higher than %1 is considered an absurdly high fee.</source>
        <translation>Ang bayad na mas mataas sa %1 ay itinuturing na napakataas na bayad.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Payment request expired.</source>
        <translation>Nag-expire ang kahilingan sa pagbabayad.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Public Label exeeds limit of </source>
        <translation>Ang Pampublikong Label ay lumampas sa limitasyon ng</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Pay only the required fee of %1</source>
        <translation>Bayaran lamang ang kinakailangang bayad na %1</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Warning: Invalid Nexa address</source>
        <translation>Babala: Di-wastong Nexa address</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Warning: Unknown change address</source>
        <translation>Babala: Hindi kilalang pagbabago ng address</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirm custom change address</source>
        <translation>Kumpirmahin ang custom na pagbabago ng address</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The address you selected for change is not part of this wallet. Any or all funds in your wallet may be sent to this address. Are you sure?</source>
        <translation>Ang address na iyong pinili para sa pagbabago ay hindi bahagi ng wallet na ito. Anuman o lahat ng mga pondo sa iyong wallet ay maaaring ipadala sa address na ito. Sigurado ka ba?</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>(no label)</source>
        <translation>(alang label)</translation>
    </message>
</context>
<context>
    <name>SendCoinsEntry</name>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="+86"/>
        <location line="+677"/>
        <location line="+560"/>
        <source>A&amp;mount:</source>
        <translation>A&amp;laga:</translation>
    </message>
    <message>
        <location line="-1224"/>
        <source>Pay &amp;To:</source>
        <translation>Ibayad &amp;kang:</translation>
    </message>
    <message>
        <location line="-55"/>
        <source>Amount to send</source>
        <translation>Halagang ipapadala</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The fee will be deducted from the amount being sent. The recipient will receive less coins than you enter in the amount field. If multiple recipients are selected, the fee is split equally.</source>
        <translation>Ang bayad ay ibabawas mula sa halagang ipinadala. Ang tatanggap ay makakatanggap ng mas kaunting mga barya kaysa sa inilagay mo sa field ng halaga. Kung maraming tatanggap ang napili, ang bayad ay hahatiin nang pantay.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;ubtract fee from amount</source>
        <translation>Ibawas ang bayad sa halaga</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Private Description:</source>
        <translation>Pribadong Paglalarawan:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Private &amp;Label:</source>
        <translation>Markang pribado:</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>The Nexa address to send the payment to</source>
        <translation>Ang address ng Nexa kung saan ipadala ang bayad</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation>Piliin ang dating ginamit na address</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>Idikit ing address menibat king clipboard</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+553"/>
        <location line="+560"/>
        <source>Remove this entry</source>
        <translation>Alisin ang entry na ito</translation>
    </message>
    <message>
        <location line="-1091"/>
        <source>A message that was attached to the coin: URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>Isang mensahe na naka-attach sa coin: URI na maiimbak kasama ng transaksyon para sa iyong sanggunian. Tandaan: Ang mensaheng ito ay hindi ipapadala sa network ng Nexa.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter a private label for this address to add it to the list of used addresses</source>
        <translation>Maglagay ng pribadong label para sa address na ito upang idagdag ito sa listahan ng mga ginamit na address</translation>
    </message>
    <message>
        <location line="+11"/>
        <location filename="../sendcoinsentry.cpp" line="+43"/>
        <source>Enter a public label for this transaction</source>
        <translation>Maglagay ng pampublikong label para sa transaksyong ito</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Public Label:</source>
        <translation>Pampublikong Label:</translation>
    </message>
    <message>
        <location line="+466"/>
        <source>This is an unauthenticated payment request.</source>
        <translation>Ito ay isang hindi napatotohanang kahilingan sa pagbabayad.</translation>
    </message>
    <message>
        <location line="+49"/>
        <location line="+560"/>
        <source>Memo:</source>
        <translation>Memo:</translation>
    </message>
    <message>
        <location line="-53"/>
        <source>This is an authenticated payment request.</source>
        <translation>Ito ay isang napatunayang kahilingan sa pagbabayad.</translation>
    </message>
    <message>
        <location line="-541"/>
        <location line="+556"/>
        <source>Pay To:</source>
        <translation>Ibayad kang:</translation>
    </message>
    <message>
        <location filename="../sendcoinsentry.cpp" line="-12"/>
        <source>A message that was attached to the %1 URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>Isang mensahe na naka-attach sa %1 URI na maiimbak kasama ng transaksyon para sa iyong sanggunian. Tandaan: Ang mensaheng ito ay hindi ipapadala sa network ng Nexa.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Enter a private label for this address to add it to your address book</source>
        <translation>Maglagay ng pribadong label para sa address na ito upang idagdag ito sa iyong address book</translation>
    </message>
</context>
<context>
    <name>ShutdownWindow</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+76"/>
        <source>%1 is shutting down...</source>
        <translation>Nagsasara ang %1...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do not shut down the computer until this window disappears.</source>
        <translation>Huwag isara ang computer hanggang sa mawala ang window na ito.</translation>
    </message>
</context>
<context>
    <name>SignVerifyMessageDialog</name>
    <message>
        <location filename="../forms/signverifymessagedialog.ui" line="+14"/>
        <source>Signatures - Sign / Verify a Message</source>
        <translation>Pirma - Pirman / I-beripika ing mensayi</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sign Message</source>
        <translation>&amp;Pirman ing Mensayi</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You can sign messages/agreements with your addresses to prove you can receive coins sent to them. Be careful not to sign anything vague or random, as phishing attacks may try to trick you into signing your identity over to them. Only sign fully-detailed statements you agree to.</source>
        <translation>Maaari kang pumirma sa mga mensahe/kasunduan gamit ang iyong mga address upang patunayan na makakatanggap ka ng mga barya na ipinadala sa kanila. Mag-ingat na huwag pumirma ng anumang malabo o random, dahil ang mga pag-atake ng phishing ay maaaring subukang linlangin ka na pirmahan ang iyong pagkakakilanlan sa kanila. Pumirma lamang sa mga ganap na detalyadong pahayag na sinasang-ayunan mo.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The Nexa address to sign the message with</source>
        <translation>Ang address ng Nexa kung saan pirmahan ang mensahe</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+210"/>
        <source>Choose previously used address</source>
        <translation>Piliin ang dating ginamit na address</translation>
    </message>
    <message>
        <location line="-200"/>
        <location line="+210"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="-200"/>
        <source>Paste address from clipboard</source>
        <translation>Idikit ing address menibat king clipboard</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter the message you want to sign here</source>
        <translation>Ipalub ing mensayi a buri mung pirman keni</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Signature</source>
        <translation>Pirma</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Copy the current signature to the system clipboard</source>
        <translation>Kopyan ing kasalungsungan pirma king system clipboard</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Sign the message to prove you own this Nexa address</source>
        <translation>Pirman ing mensayi ban patune na keka ya ining Nexa address</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sign &amp;Message</source>
        <translation>Pirman ing &amp;Mensayi</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all sign message fields</source>
        <translation>Ibalik keng dati reng ngan fields keng pamamirmang mensayi</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+143"/>
        <source>Clear &amp;All</source>
        <translation>I-Clear &amp;Eganagana</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>&amp;Verify Message</source>
        <translation>&amp;Beripikan ing Mensayi</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter the receiver&apos;s address, message (ensure you copy line breaks, spaces, tabs, etc. exactly) and signature below to verify the message. Be careful not to read more into the signature than what is in the signed message itself, to avoid being tricked by a man-in-the-middle attack. Note that this only proves the signing party receives with the address, it cannot prove sendership of any transaction!</source>
        <translation>Ilagay ang address, mensahe ng receiver (tiyaking eksaktong kopyahin mo ang mga line break, espasyo, tab, atbp.) at lagda sa ibaba upang i-verify ang mensahe. Mag-ingat na huwag magbasa nang higit pa sa lagda kaysa sa kung ano ang nasa mismong nilagdaang mensahe, upang maiwasang malinlang ng isang man-in-the-middle na pag-atake. Tandaan na ito ay nagpapatunay lamang na natatanggap ng pumirmang partido ang address, hindi nito mapapatunayan ang nagpadala ng anumang transaksyon!</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>The Nexa address the message was signed with</source>
        <translation>Ang address ng Nexa kung saan nilagdaan ang mensahe</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Verify the message to ensure it was signed with the specified Nexa address</source>
        <translation>Beripikan ing mensayi ban asiguradu a me pirma ya ini gamit ing mepiling Nexa address</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify &amp;Message</source>
        <translation>Beripikan ing &amp;Mensayi</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all verify message fields</source>
        <translation>Ibalik king dati reng ngan fields na ning pamag beripikang mensayi</translation>
    </message>
    <message>
        <location filename="../signverifymessagedialog.cpp" line="+40"/>
        <source>Click &quot;Sign Message&quot; to generate signature</source>
        <translation>I-click ing &quot;Pirman ing Mensayi&quot; ban agawa ya ing metung a pirma</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Wallet unlock was cancelled.</source>
        <translation>Me-kansela ya ing pamag-unlock king wallet.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Message signed.</source>
        <translation>Me-pirman ne ing mensayi.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Message verified.</source>
        <translation>Me-beripika ne ing mensayi.</translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="../networkstyle.cpp" line="+19"/>
        <source>[testnet]</source>
        <translation>[testnet]</translation>
    </message>
</context>
<context>
    <name>TokenDescDialog</name>
    <message>
        <location filename="../forms/tokendescdialog.ui" line="+17"/>
        <source>Token details</source>
        <translation>Mga detalye ng token</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This pane shows a detailed description of the token&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ang pane na ito ay nagpapakita ng detalyadong paglalarawan ng token&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>TokenDisplayDialog</name>
    <message>
        <location filename="../forms/tokendisplaydialog.ui" line="+14"/>
        <source>Token Display Manager</source>
        <translation>Tagapamahala ng Token Display</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add a token to the list below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add New Token</source>
        <translation>Magdagdag ng bagong Token</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove a token from the list below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Mag-alis ng token sa listahan sa ibaba&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove Token</source>
        <translation>Alisin ang Token</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Display</source>
        <translation>Ipalto</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ticker</source>
        <translation>Ticker</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Group ID</source>
        <translation>Group ID</translation>
    </message>
    <message>
        <location filename="../tokendisplaydialog.cpp" line="+43"/>
        <source>Copy GroupID</source>
        <translation>Kopyahin ang GroupID</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Token Ticker</source>
        <translation>Kopyahin ang Token Ticker</translation>
    </message>
</context>
<context>
    <name>TokenHistoryView</name>
    <message>
        <location filename="../tokenhistoryview.cpp" line="+78"/>
        <location line="+19"/>
        <source>All</source>
        <translation>Eganagana</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>Aldo iti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>Paruminggung iti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>Bulan a iti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>Milabas a bulan</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>Banuang iti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>Angganan...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>Atanggap kayabe ning</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent</source>
        <translation>Ipinadala</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>Keng sarili mu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mint</source>
        <translation>Mint</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>Aliwa</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter token ID to search</source>
        <translation>Maglagay ng token ID para maghanap</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>Pekaditak a alaga</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy token id</source>
        <translation>Kopyahin ang token id</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Kopyan ing alaga</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction id</source>
        <translation>Kopyahin ang transaction id</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction idem</source>
        <translation>Kopyahin ang transaction idem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy raw transaction</source>
        <translation>Kopyahin ang hilaw na transaksyon</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>Ipakit ing detalye ning transaksion</translation>
    </message>
    <message>
        <location line="+167"/>
        <source>Export Token History</source>
        <translation>I-export ang Kasaysayan ng Token</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Comma separated file (*.csv)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Confirmed</source>
        <translation>Me-kumpirma</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation>Panoorin lamang</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>Kaaldauan</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Klase</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Address</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction ID</source>
        <translation>ID ng Transaksyon</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction Idem</source>
        <translation>Idem ng Transaksyon</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token ID</source>
        <translation>Mga token ID</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token Amount</source>
        <translation>Halaga ng Token</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Failed</source>
        <translation>Nabigo ang pag-export</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the token history to %1.</source>
        <translation>Nagkaroon ng error sa pagsubok na i-save ang kasaysayan ng token sa %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>Matagumpay ang Pag-export</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The token history was successfully saved to %1.</source>
        <translation>Ang kasaysayan ng token ay matagumpay na nai-save sa %1.</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Range:</source>
        <translation>Angga:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>para kang</translation>
    </message>
</context>
<context>
    <name>TokenTableModel</name>
    <message>
        <location filename="../tokentablemodel.cpp" line="+354"/>
        <source>Date</source>
        <translation>Kaaldauan</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>Klase</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Token ID</source>
        <translation>Mga token ID</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Net Amount</source>
        <translation>Alaga dareng eganagana</translation>
    </message>
    <message numerus="yes">
        <location line="+60"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Buksan para sa %n pang (mga) bloke</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open until %1</source>
        <translation>Makabuklat anggang %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Offline</source>
        <translation>Offline</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>Hindi kumpirmado</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>Pagkumpirma (%1 ng %2 inirerekomendang pagkumpirma)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>Me-kumpirma(%1 kumpirmasion)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicted</source>
        <translation>Nagkasalungatan</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation>Dobleng Ginastos</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation>Inabandona</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>Immature (%1 confirmations, magiging available pagkatapos ng %2)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>Ing block a ini ali de atanggap deng aliwa pang nodes ania ali ya magsilbing tanggapan</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>Me-generate ya oneng ali ya metanggap</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation>Atanggap kayabe ning</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>Atanggap menibat kang</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent</source>
        <translation>Ipinadala</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>Kabayaran keka</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mint</source>
        <translation>Mint</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Melt</source>
        <translation>Matunaw</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation>Pampublikong label</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation>Aliwa</translation>
    </message>
    <message>
        <location line="+325"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>Status ning Transaksion: Itapat me babo na ning field a ini ban ipakit dala reng bilang dareng me-kumpirma na</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>Aldo at oras nung kapilan me tanggap ya ing transaksion</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>Klase ning transaksion</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>May kinalaman man o hindi ang isang watch-only na address sa transaksyong ito.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>Layunin/layunin ng transaksyon na tinukoy ng user.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>Alagang milako o miragdag king balanse.</translation>
    </message>
</context>
<context>
    <name>TokensViewDialog</name>
    <message>
        <location filename="../forms/tokensviewdialog.ui" line="+14"/>
        <source>Tokens</source>
        <translation>Mga token</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enable/Disable which tokens in the wallet are to be displayed in QT&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Paganahin/Huwag paganahin kung aling mga token sa wallet ang ipapakita sa QT&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Manage Displayed Tokens</source>
        <translation>Pamahalaan ang mga Ipinapakitang Token</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tokens in Wallet:</source>
        <translation>Mga Token sa Wallet:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tokens Displayed: </source>
        <translation>Mga Token na Ipinapakita:</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Amount: </source>
        <translation>Alaga: </translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The number of tokens to send&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ang bilang ng mga token na ipapadala&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>  Pay To: </source>
        <translation>  Ibayad kang: </translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The Nexa address to send the payment to</source>
        <translation>Ang address ng Nexa kung saan ipadala ang bayad</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation>Piliin ang dating ginamit na address</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>Idikit ing address menibat king clipboard</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remove this entry</source>
        <translation>Alisin ang entry na ito</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Send tokens&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Magpadala ng mga token&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Send</source>
        <translation>Ipadala</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Token ID</source>
        <translation>Mga token ID</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The unique token identifier</source>
        <translation>Ang natatanging token identifier</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>The token group identifier</source>
        <translation>Ang pagkakakilanlan ng pangkat ng token</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Name</source>
        <translation>Pangalan</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The name of the token</source>
        <translation>Ang pangalan ng token</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ticker</source>
        <translation>Ticker</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The token ticker symbol</source>
        <translation>Ang simbolo ng token ticker</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Balance</source>
        <translation>Balanse</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed token balance</source>
        <translation>Nakumpirmang balanse ng token</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pending</source>
        <translation>Nakabinbin</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed token balance</source>
        <translation>Hindi kumpirmadong balanse ng token</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Sub</source>
        <translation>Sub</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>If checked then this item is a subgroup.</source>
        <translation>Kung nilagyan ng check, ang item na ito ay isang subgroup.</translation>
    </message>
    <message>
        <location filename="../tokensviewdialog.cpp" line="+397"/>
        <location line="+7"/>
        <location line="+4"/>
        <source>Data</source>
        <translation>Datos</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>string</source>
        <translation>string</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+4"/>
        <source>num</source>
        <translation>num</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>NaN</source>
        <translation>NaN</translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+84"/>
        <location line="+5"/>
        <location line="+5"/>
        <source>Total Mintage:</source>
        <translation>Kabuuang Paggawa:</translation>
    </message>
    <message>
        <location line="-93"/>
        <location line="+94"/>
        <source>Token mintage is unavailable because the database needs a reindex</source>
        <translation>Hindi available ang token mintage dahil kailangan ng database ng reindex</translation>
    </message>
    <message>
        <location line="-86"/>
        <location line="+43"/>
        <source>Name:</source>
        <translation>Pangalan:</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Ticker:</source>
        <translation>Ticker:</translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+43"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Hash:</source>
        <translation>Hash:</translation>
    </message>
    <message>
        <location line="-39"/>
        <location line="+43"/>
        <source>Decimals:</source>
        <translation>Mga decimal:</translation>
    </message>
    <message>
        <location line="-37"/>
        <location line="+14"/>
        <location line="+62"/>
        <location line="+14"/>
        <source>Current Authorities:</source>
        <translation>Kasalukuyang Awtoridad:</translation>
    </message>
    <message>
        <location line="-88"/>
        <location line="+76"/>
        <source>Mint:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Melt:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Renew:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Rescript:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Subgroup:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+76"/>
        <source>Token authorities are unavailable because the database needs a reindex</source>
        <translation>Ang mga awtoridad ng token ay hindi magagamit dahil ang database ay nangangailangan ng reindex</translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+77"/>
        <source>Balance:</source>
        <translation>Balanse:</translation>
    </message>
    <message>
        <location line="-76"/>
        <location line="+77"/>
        <source>Pending:</source>
        <translation>Nakabinbin:</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Are you sure you want to send?</source>
        <translation>Sigurado ka bang gusto mong magpadala?</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;b&gt;Token(s)&lt;/b&gt;</source>
        <translation>&lt;b&gt;Token(s)&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Confirm send tokens</source>
        <translation>Kumpirmahin ang pagpapadala ng mga token</translation>
    </message>
</context>
<context>
    <name>TrafficGraphWidget</name>
    <message>
        <location filename="../trafficgraphwidget.cpp" line="+74"/>
        <source>KB/s</source>
        <translation>KB/s</translation>
    </message>
</context>
<context>
    <name>TransactionDesc</name>
    <message>
        <location filename="../transactiondesc.cpp" line="+35"/>
        <source>Open until %1</source>
        <translation>Makabuklat anggang %1</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1/offline</source>
        <translation>%1/offline</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/unconfirmed</source>
        <translation>%1/ali me-kumpirma</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 confirmations</source>
        <translation>%1 kumpirmasion</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Status</source>
        <translation>Kabilian</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Date</source>
        <translation>Kaaldauan</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Source</source>
        <translation>Pikuanan</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Generated</source>
        <translation>Megawa</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+13"/>
        <location line="+122"/>
        <source>From</source>
        <translation>Menibat</translation>
    </message>
    <message>
        <location line="-142"/>
        <location line="+21"/>
        <location line="+35"/>
        <location line="+108"/>
        <source>To</source>
        <translation>Para kang</translation>
    </message>
    <message numerus="yes">
        <location line="-217"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Buksan para sa %n pang bloke</numerusform>
            <numerusform>Buksan para sa %n pang mga bloke</numerusform>
        </translation>
    </message>
    <message>
        <location line="+8"/>
        <source>double spent</source>
        <translation>dobleng ginastos</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>abandoned</source>
        <translation>inabandona</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>conflicted</source>
        <translation>nagkasalungatan</translation>
    </message>
    <message numerus="yes">
        <location line="+30"/>
        <source>, broadcast through %n node(s)</source>
        <translation>
            <numerusform>, broadcast sa pamamagitan ng %n node</numerusform>
            <numerusform>, broadcast sa pamamagitan ng %n mga node</numerusform>
        </translation>
    </message>
    <message>
        <location line="+38"/>
        <source>change address</source>
        <translation>baguhin ang address</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>own address</source>
        <translation>sariling address</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+113"/>
        <source>watch-only</source>
        <translation>panoorin lamang</translation>
    </message>
    <message>
        <location line="-110"/>
        <location line="+28"/>
        <location line="+107"/>
        <source>label</source>
        <translation>label</translation>
    </message>
    <message>
        <location line="-116"/>
        <location line="+57"/>
        <location line="+48"/>
        <location line="+71"/>
        <source>Public label:</source>
        <translation>Pampublikong Label:</translation>
    </message>
    <message>
        <location line="-149"/>
        <location line="+41"/>
        <location line="+63"/>
        <location line="+47"/>
        <location line="+256"/>
        <source>Credit</source>
        <translation>Credit</translation>
    </message>
    <message numerus="yes">
        <location line="-404"/>
        <source>matures in %n more block(s)</source>
        <translation>
            <numerusform>mature sa %n pang bloke</numerusform>
            <numerusform>mature sa %n pang mga bloke</numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>not accepted</source>
        <translation>ali metanggap</translation>
    </message>
    <message>
        <location line="+102"/>
        <location line="+31"/>
        <location line="+261"/>
        <source>Debit</source>
        <translation>Debit</translation>
    </message>
    <message>
        <location line="-280"/>
        <source>Total debit</source>
        <translation>Kabuuang debit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Total credit</source>
        <translation>Kabuuang kredito</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction fee</source>
        <translation>Bayad king Transaksion</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Net amount</source>
        <translation>Alaga dareng eganagana</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+13"/>
        <source>Message</source>
        <translation>Mensayi</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Comment</source>
        <translation>Komentu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transaction ID</source>
        <translation>ID ng Transaksyon</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction Idem</source>
        <translation>Idem ng Transaksyon</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction size</source>
        <translation>Laki ng transaksyon</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Merchant</source>
        <translation>Mangangalakal</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Generated coins must mature %1 blocks before they can be spent. When you generated this block, it was broadcast to the network to be added to the block chain. If it fails to get into the chain, its state will change to &quot;not accepted&quot; and it won&apos;t be spendable. This may occasionally happen if another node generates a block within a few seconds of yours.</source>
        <translation>Ang mga nabuong barya ay dapat mag-mature ng %1 na bloke bago sila magastos. Noong nabuo mo ang block na ito, na-broadcast ito sa network upang maidagdag sa block chain. Kung mabibigo itong makapasok sa chain, magbabago ang estado nito sa &quot;hindi tinatanggap&quot; at hindi ito magagastos. Ito ay maaaring mangyari paminsan-minsan kung ang isa pang node ay bubuo ng isang bloke sa loob ng ilang segundo ng sa iyo.</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+33"/>
        <location line="+48"/>
        <source>Token ID</source>
        <translation>Mga token ID</translation>
    </message>
    <message>
        <location line="-76"/>
        <location line="+33"/>
        <location line="+48"/>
        <source>Ticker</source>
        <translation>Ticker</translation>
    </message>
    <message>
        <location line="-80"/>
        <location line="+33"/>
        <location line="+48"/>
        <source>Name</source>
        <translation>Pangalan</translation>
    </message>
    <message>
        <location line="-77"/>
        <location line="+32"/>
        <location line="+48"/>
        <source>Decimals</source>
        <translation>Mga decimal</translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+4"/>
        <source>Melt</source>
        <translation>Matunaw</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+4"/>
        <location line="+56"/>
        <location line="+4"/>
        <source>Mint</source>
        <translation>Mint</translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+4"/>
        <source>Amount Sent</source>
        <translation>Halaga ng Ipinadala</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+4"/>
        <source>Amount Received</source>
        <translation>Halaga na Natanggap</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Debug information</source>
        <translation>Impormasion ning Debug</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transaction</source>
        <translation>Transaksion</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inputs</source>
        <translation>Mga input</translation>
    </message>
    <message>
        <location line="-133"/>
        <location line="+4"/>
        <location line="+30"/>
        <location line="+4"/>
        <location line="+56"/>
        <location line="+4"/>
        <location line="+57"/>
        <source>Amount</source>
        <translation>Alaga</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+3"/>
        <source>true</source>
        <translation>tutu</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+3"/>
        <source>false</source>
        <translation>e tutu</translation>
    </message>
    <message>
        <location line="-533"/>
        <source>, has not been successfully broadcast yet</source>
        <translation>, eya matagumpeng mibalita</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>unknown</source>
        <translation>e miya balu</translation>
    </message>
</context>
<context>
    <name>TransactionDescDialog</name>
    <message>
        <location filename="../forms/transactiondescdialog.ui" line="+14"/>
        <source>Transaction details</source>
        <translation>Detalye ning Transaksion</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>This pane shows a detailed description of the transaction</source>
        <translation>Ining pane a ini magpakit yang detalyadung description ning transaksion</translation>
    </message>
</context>
<context>
    <name>TransactionGraphWidget</name>
    <message>
        <location filename="../transactiongraphwidget.cpp" line="+148"/>
        <source>tps</source>
        <translation>tps</translation>
    </message>
</context>
<context>
    <name>TransactionTableModel</name>
    <message>
        <location filename="../transactiontablemodel.cpp" line="+239"/>
        <source>Date</source>
        <translation>Petsa</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>Klase</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address or Label</source>
        <translation>Address o Label</translation>
    </message>
    <message numerus="yes">
        <location line="+60"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Buksan para sa %n pang (mga) bloke</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open until %1</source>
        <translation>Makabuklat anggang %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Offline</source>
        <translation>Offline</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>Hindi kumpirmado</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>Pagkumpirma (%1 ng %2 inirerekomendang pagkumpirma)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>Me-kumpirma(%1 kumpirmasion)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicted</source>
        <translation>Nagkasalungatan</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation>Dobleng Ginastos</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation>Inabandona</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>Immature (%1 confirmations, magiging available pagkatapos ng %2)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>Ing block a ini ali de atanggap deng aliwa pang nodes ania ali ya magsilbing tanggapan</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>Me-generate ya oneng ali ya metanggap</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Public label</source>
        <translation>Pampublikong label</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation>Aliwa</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>watch-only</source>
        <translation>panoorin lamang:</translation>
    </message>
    <message>
        <location line="-43"/>
        <source>Received with</source>
        <translation>Atanggap kayabe ning</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>Atanggap menibat kang</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent to</source>
        <translation>Mipadala kang</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>Kabayaran keka</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mined</source>
        <translation>Me-mina</translation>
    </message>
    <message>
        <location line="+311"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>Status ning Transaksion: Itapat me babo na ning field a ini ban ipakit dala reng bilang dareng me-kumpirma na</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>Aldo at oras nung kapilan me tanggap ya ing transaksion</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>Klase ning transaksion</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>May kinalaman man o hindi ang isang watch-only na address sa transaksyong ito.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>Layunin/layunin ng transaksyon na tinukoy ng user.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>Alagang milako o miragdag king balanse.</translation>
    </message>
</context>
<context>
    <name>TransactionView</name>
    <message>
        <location filename="../transactionview.cpp" line="+77"/>
        <location line="+19"/>
        <source>All</source>
        <translation>Eganagana</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>Aldo iti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>Paruminggung iti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>Bulan a iti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>Milabas a bulan</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>Banuang iti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>Angganan...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>Atanggap kayabe ning</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent to</source>
        <translation>Mipadala kang</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>Keng sarili mu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mined</source>
        <translation>Me-mina</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>Aliwa</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter address or label to search</source>
        <translation>Magpalub kang address o label para pantunan</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>Pekaditak a alaga</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy address</source>
        <translation>Kopyan ing address</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Kopyan ing label</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Kopyan ing alaga</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction id</source>
        <translation>Kopyahin ang transaction id</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction idem</source>
        <translation>Kopyahin ang transaction idem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy raw transaction</source>
        <translation>Kopyahin ang hilaw na transaksyon</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit label</source>
        <translation>Alilan ing label</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>Ipakit ing detalye ning transaksion</translation>
    </message>
    <message>
        <location line="+176"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Comma separated file (*.csv)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Export Transaction History</source>
        <translation>I-export ang Kasaysayan ng Transaksyon</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Confirmed</source>
        <translation>Me-kumpirma</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation>Panoorin lamang</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>Kaaldauan</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Klase</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Address</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Idem</source>
        <translation>Idem</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Failed</source>
        <translation>Nabigo ang pag-export</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the transaction history to %1.</source>
        <translation>Nagkaroon ng error sa pagsubok na i-save ang kasaysayan ng transaksyon sa %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>Matagumpay ang Pag-export</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction history was successfully saved to %1.</source>
        <translation>Ang kasaysayan ng transaksyon ay matagumpay na nai-save sa %1.</translation>
    </message>
    <message>
        <location line="+98"/>
        <source>Range:</source>
        <translation>Angga:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>para kang</translation>
    </message>
</context>
<context>
    <name>UnitDisplayStatusBarControl</name>
    <message>
        <location filename="../nexagui.cpp" line="+117"/>
        <source>Unit to show amounts in. Click to select another unit.</source>
        <translation>Unit para ipakita ang mga halaga. I-click para pumili ng isa pang unit.</translation>
    </message>
</context>
<context>
    <name>UnlimitedDialog</name>
    <message>
        <location filename="../forms/unlimited.ui" line="+14"/>
        <source>Unlimited</source>
        <translation>Unlimited</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Mining</source>
        <translation>&amp;Pagmimina</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+10"/>
        <source>The largest block that will be mined</source>
        <translation>Ang pinakamalaking bloke na mamimina</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Maximum Generated Block Size (bytes) </source>
        <translation>Maximum na Binuo na Laki ng Block (byte)</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Network</source>
        <translation>&amp;Network</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Bandwidth Restrictions in KBytes/sec (check to enable):</source>
        <translation>Mga Paghihigpit sa Bandwidth sa KBytes/sec (suriin upang paganahin):</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Send</source>
        <translation>Ipadala</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+71"/>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <location line="-35"/>
        <location line="+59"/>
        <source>Average</source>
        <translation>Katamtaman</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Receive</source>
        <translation>Tumanggap</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Active command-line options that override above options:</source>
        <translation>Mga aktibong opsyon sa command-line na nag-o-override sa mga opsyon sa itaas:</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Reset all client options to default.</source>
        <translation>I-reset ang lahat ng mga opsyon ng kliyente sa default.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>&amp;I-reset ang Mga Opsyon</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>I-&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../unlimiteddialog.cpp" line="-345"/>
        <source>Confirm options reset</source>
        <translation>Kumpirmahin ang pag-reset ng mga opsyon</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is a global reset of all settings!</source>
        <translation>Isa itong pandaigdigang pag-reset ng lahat ng setting!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client restart required to activate changes.</source>
        <translation>Kinakailangan ang pag-restart ng kliyente upang maisaaktibo ang mga pagbabago.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>Isasara ang kliyente. Gusto mo bang magpatuloy?</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+39"/>
        <location line="+42"/>
        <location line="+42"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>Hindi maaaring blangko ang mga parameter sa paghubog ng upstream na trapiko</translation>
    </message>
    <message>
        <location line="-108"/>
        <location line="+36"/>
        <location line="+43"/>
        <location line="+41"/>
        <source>Traffic shaping parameters have to be greater than 0.</source>
        <translation>Ang mga parameter ng paghubog ng trapiko ay dapat na higit sa 0.</translation>
    </message>
</context>
<context>
    <name>WalletFrame</name>
    <message>
        <location filename="../walletframe.cpp" line="+31"/>
        <source>No wallet has been loaded.</source>
        <translation>Walang wallet na na-load.</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Token Database Status</source>
        <translation>Katayuan ng Token Database</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token wallet not available because a -reindex is needed. Click &quot;Ok&quot; to perform a one time -reindex on the next startup. Then shutdown Nexa-Qt and restart to complete the process.</source>
        <translation>Hindi available ang token wallet dahil kailangan ang isang -reindex. I-click ang &quot;Ok&quot; para magsagawa ng isang beses -reindex sa susunod na startup. Pagkatapos ay isara ang Nexa-Qt at i-restart upang makumpleto ang proseso.</translation>
    </message>
</context>
<context>
    <name>WalletModel</name>
    <message>
        <location filename="../walletmodel.cpp" line="+300"/>
        <source>Send Coins</source>
        <translation>Magpadalang Barya</translation>
    </message>
</context>
<context>
    <name>WalletView</name>
    <message>
        <location filename="../walletview.cpp" line="+46"/>
        <location line="+20"/>
        <source>&amp;Export</source>
        <translation>&amp;l-export</translation>
    </message>
    <message>
        <location line="-19"/>
        <location line="+20"/>
        <source>Export the data in the current tab to a file</source>
        <translation>I-export ang data sa kasalukuyang tab sa isang file</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Backup Wallet</source>
        <translation>I-Backup ing Wallet...</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+21"/>
        <source>Wallet Data (*.dat)</source>
        <translation>Data ng Wallet (*.dat)</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Backup Failed</source>
        <translation>Nabigo ang pag-backup</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the wallet data to %1.</source>
        <translation>Nagkaroon ng error sa pagsubok na i-save ang data ng pitaka sa %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Backup Successful</source>
        <translation>Matagumpay ang Pag-backup</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The wallet data was successfully saved to %1.</source>
        <translation>Ang data ng pitaka ay matagumpay na na-save sa %1.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Restore Wallet</source>
        <translation>Ibalik ang Wallet</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Restore Failed</source>
        <translation>Nabigo ang Pag-restore</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to restore the wallet data to %1.</source>
        <translation>Nagkaroon ng error sa pagsubok na ibalik ang data ng pitaka sa %1.</translation>
    </message>
</context>
<context>
    <name>nexa</name>
    <message>
        <location filename="../nexastrings.cpp" line="+150"/>
        <source>Corrupted block database detected</source>
        <translation>Mekapansin lang me-corrupt a block database</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to rebuild the block database now?</source>
        <translation>Buri meng buuan pasibayu ing block database ngene?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error initializing block database</source>
        <translation>Kamalian king pamag-initialize king block na ning database</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Error opening block database</source>
        <translation>Kamalian king pamag buklat king block database</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error: Disk space is low!</source>
        <translation>Kamalian: Mababa ne ing espasyu king disk!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Failed to listen on any port. Use -listen=0 if you want this.</source>
        <translation>Memali ya ing pamakiramdam kareng gang nanung port. Gamita me ini -listen=0 nung buri me ini.</translation>
    </message>
    <message>
        <location line="-19"/>
        <source>Cannot resolve -whitebind address: &apos;%s&apos;</source>
        <translation>Eya me-resolve ing -whitebind address: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Information</source>
        <translation>&amp;Impormasion</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Warning</source>
        <translation>Kapabaluan</translation>
    </message>
    <message>
        <location line="-42"/>
        <source>Loading addresses...</source>
        <translation>Lo-load da ne ing address...</translation>
    </message>
    <message>
        <location line="-112"/>
        <source>Rescans are not possible in pruned mode. You will need to use -resync which will download the whole blockchain again.</source>
        <translation>Ang mga muling pag-scan ay hindi posible sa pruned mode. Kakailanganin mong gumamit ng -resync na magda-download muli ng buong blockchain.</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>You can not run &quot;-salvagewallet&quot; as an HD wallet.

Please relaunch Nexa with &quot;-usehd=0&quot;.</source>
        <translation>Hindi mo maaaring patakbuhin ang &quot;-salvagewallet&quot; bilang HD wallet.

Pakilunsad muli ang Nexa gamit ang &quot;-usehd=0&quot;.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You need to rebuild the database using -resync to go back to unpruned mode.  This will redownload the entire blockchain.</source>
        <translation>Kailangan mong buuin muli ang database gamit ang -resync para bumalik sa unpruned mode. Ida-download muli nito ang buong blockchain.</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Invalid -proxy address: &apos;%s&apos;</source>
        <translation>Ali katanggap-tanggap a -proxy addresss: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Signing token transaction failed</source>
        <translation>Nabigo ang pagpirma ng transaksyon sa token</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Signing transaction failed</source>
        <translation>Nabigo ang pagpirma ng transaksyon</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Starting txindex</source>
        <translation>Simula txindex</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction amount is too small to pay the fee</source>
        <translation>Masyadong maliit ang halaga ng transaksyon para mabayaran ang bayad</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is experimental software.</source>
        <translation>Ito ay pang-eksperimentong software.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amount too small</source>
        <translation>Masyadong maliit ang halaga ng transaksyon</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amounts must be positive</source>
        <translation>Dapat na positibo ang mga halaga ng transaksyon</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction has %d outputs. Maximum outputs allowed is %d</source>
        <translation>May %d output ang transaksyon. Ang maximum na mga output na pinapayagan ay %d</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction of %d bytes is too large. Maximum allowed is %d bytes</source>
        <translation>Masyadong malaki ang transaksyon ng %d bytes. Ang maximum na pinapayagan ay %d bytes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction too large for fee policy</source>
        <translation>Masyadong malaki ang transaksyon para sa patakaran sa bayad</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Turn off auto consolidate and try sending again.</source>
        <translation>I-off ang auto consolidated at subukang magpadala muli.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to bind to %s on this computer (bind returned error %s)</source>
        <translation>Hindi ma-bind sa %s sa computer na ito (bind return error %s)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to bind to %s on this computer. %s is probably already running.</source>
        <translation>Hindi ma-bind sa %s sa computer na ito. %s ay malamang na tumatakbo na.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to start RPC services. See debug log for details.</source>
        <translation>Hindi masimulan ang mga serbisyo ng RPC. Tingnan ang debug log para sa mga detalye.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown network specified in -onlynet: &apos;%s&apos;</source>
        <translation>E kilalang network ing mepili king -onlynet: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Cannot resolve -bind address: &apos;%s&apos;</source>
        <translation>Eya me-resolve ing -bind address: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -externalip address: &apos;%s&apos;</source>
        <translation>Eya me-resolve ing -externalip address: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Loading block index...</source>
        <translation>Lo-load dane ing block index...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet...</source>
        <translation>Lo-load dane ing wallet...</translation>
    </message>
    <message>
        <location line="-38"/>
        <source>Cannot downgrade wallet</source>
        <translation>Ali ya magsilbing i-downgrade ing wallet</translation>
    </message>
    <message>
        <location line="-131"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The %s developers</source>
        <translation>Ang mga developer ng %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bitcoin Bitcoin XT and Bitcoin Unlimited</source>
        <translation>Bitcoin Bitcoin XT at Bitcoin Unlimited</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-wallet.maxTxFee is set very high! Fees this large could be paid on a single transaction.</source>
        <translation>-wallet.maxTxFee ay nakatakda nang napakataas! Ang mga bayaring ganito kalaki ay maaaring bayaran sa isang transaksyon.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>-wallet.payTxFee is set very high! This is the transaction fee you will pay if you send a transaction.</source>
        <translation>-wallet.payTxFee ay nakatakda nang napakataas! Ito ang bayad sa transaksyon na babayaran mo kung magpadala ka ng transaksyon.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot obtain a lock on data directory %s. %s is probably already running.</source>
        <translation>Hindi makakuha ng lock sa direktoryo ng data %s. %s ay malamang na tumatakbo na.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Could not locate RPC credentials. No authentication cookie could be found, and no rpcpassword is set in the configuration file (%s)</source>
        <translation>Hindi mahanap ang mga kredensyal ng RPC. Walang mahanap na cookie sa pagpapatotoo, at walang rpcpassword na nakatakda sa configuration file (%s)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; contained invalid data - see debug.log</source>
        <translation>Ang deployment configuration file na &apos;%s&apos; ay naglalaman ng di-wastong data - tingnan ang debug.log</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Distributed under the MIT software license, see the accompanying file COPYING or &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</source>
        <translation>Ibinahagi sa ilalim ng lisensya ng MIT software, tingnan ang kasamang file na PAGKopya o &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error loading %s: You can&apos;t enable HD on a already existing non-HD wallet</source>
        <translation>Error sa paglo-load ng %s: Hindi mo maaaring paganahin ang HD sa isang umiiral nang non-HD wallet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error reading %s! All keys read correctly, but transaction data or address book entries might be missing or incorrect.</source>
        <translation>Error sa pagbabasa ng %s! Nabasa nang tama ang lahat ng mga key, ngunit maaaring nawawala o mali ang data ng transaksyon o mga entry sa address book.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error reading from the coin database.
Details: %s

Do you want to reindex on the next restart?</source>
        <translation>Error sa pagbabasa mula sa database ng coin.
Mga Detalye: %s

Gusto mo bang mag-reindex sa susunod na pag-restart?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: Listening for incoming connections failed (listen returned error %s)</source>
        <translation>Error: Nabigo ang pakikinig para sa mga papasok na koneksyon (pakinig ang ibinalik na error %s)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Failed to listen on all P2P ports. Failing as requested by -bindallorfail.</source>
        <translation>Nabigong makinig sa lahat ng P2P port. Nabigo gaya ng hiniling ng -bindallorfail.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fee: %ld is larger than configured maximum allowed fee of : %ld.  To change, set &apos;wallet.maxTxFee&apos;.</source>
        <translation>Bayarin: %ld ay mas malaki kaysa sa naka-configure na maximum na pinapayagang bayad na : %ld. Para baguhin, itakda ang &apos;wallet.maxTxFee&apos;.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.maxTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least the minrelay fee of %s to prevent stuck transactions)</source>
        <translation>Di-wastong halaga para sa -wallet.maxTxFee=&lt;amount&gt;: &apos;%u&apos; (dapat hindi bababa sa minrelay fee na %s para maiwasan ang mga natigil na transaksyon)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.payTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least %s)</source>
        <translation>Di-wastong halaga para sa -wallet.payTxFee=&lt;amount&gt;: &apos;%u&apos; (dapat hindi bababa sa %s)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please check that your computer&apos;s date and time are correct! If your clock is wrong, %s will not work properly.</source>
        <translation>Pakisuri kung tama ang petsa at oras ng iyong computer! Kung mali ang iyong orasan, hindi gagana nang maayos ang %s.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Prune configured below the minimum of %d MiB.  Please use a higher number.</source>
        <translation>Na-configure ang prune sa ibaba ng minimum na %d MiB. Mangyaring gumamit ng mas mataas na numero.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Prune: last wallet synchronisation goes beyond pruned data. You need to -reindex (download the whole blockchain again in case of pruned node)</source>
        <translation>Prune: ang huling pag-synchronize ng wallet ay higit pa sa pruned data. Kailangan mong -reindex (i-download muli ang buong blockchain kung sakaling may pruned node)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reducing -maxconnections from %d to %d because of file descriptor limitations (unix) or winsocket fd_set limitations (windows). If you are a windows user there is a hard upper limit of 1024 which cannot be changed by adjusting the node&apos;s configuration.</source>
        <translation>Binabawasan ang -maxconnections mula %d hanggang %d dahil sa mga limitasyon ng descriptor ng file (unix) o mga limitasyon ng winsocket fd_set (windows). Kung ikaw ay isang gumagamit ng windows mayroong isang hard upper limit na 1024 na hindi mababago sa pamamagitan ng pagsasaayos ng configuration ng node.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The block database contains a block which appears to be from the future. This may be due to your computer&apos;s date and time being set incorrectly. Only rebuild the block database if you are sure that your computer&apos;s date and time are correct</source>
        <translation>Ang database ng bloke ay naglalaman ng isang bloke na lumilitaw na mula sa hinaharap. Maaaring ito ay dahil sa hindi tamang pagkakatakda ng petsa at oras ng iyong computer. Buuin lamang ang block database kung sigurado kang tama ang petsa at oras ng iyong computer</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The transaction amount is too small to send after the fee has been deducted</source>
        <translation>Ang halaga ng transaksyon ay masyadong maliit upang ipadala pagkatapos na ibabawas ang bayad</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>This is a pre-release test build - use at your own risk - do not use for mining or merchant applications</source>
        <translation>Ang halaga ng transportasyon ay masyadong maliit upang ipadala pagkatapos ibabawas ang bayad</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit &lt;https://www.openssl.org/&gt; and cryptographic software written by Eric Young and UPnP software written by Thomas Bernard.</source>
        <translation>Ang halaga ng transportasyon ay masyadong maliit upang ipadala pagkatapos ibabawas ang bayad</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Total length of network version string with uacomments added exceeded the maximum length (%i) and have been truncated.  Reduce the number or size of uacomments to avoid truncation.</source>
        <translation>Ang kabuuang haba ng string ng bersyon ng network na may mga naidagdag na mga komento ay lumampas sa maximum na haba (%i) at naputol. Bawasan ang bilang o laki ng mga komento upang maiwasan ang pagputol.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Transaction has %d inputs and %d outputs. Maximum inputs allowed are %d and maximum outputs are %d</source>
        <translation>May %d input at %d output ang transaksyon. Ang maximum na mga input na pinapayagan ay %d at ang maximum na mga output ay %d</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction has %d inputs. Maximum inputs allowed is %d. Try reducing inputs by transferring a smaller amount.</source>
        <translation>May %d input ang transaksyon. Ang maximum na mga input na pinapayagan ay %d. Subukang bawasan ang mga input sa pamamagitan ng paglilipat ng mas maliit na halaga.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: abnormally high number of blocks generated, %d blocks received in the last %d hours (%d expected)</source>
        <translation>BABALA: abnormal na mataas na bilang ng mga bloke na nabuo, %d block na natanggap sa huling %d oras (%d inaasahan)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: check your network connection, %d blocks received in the last %d hours (%d expected)</source>
        <translation>BABALA: tingnan ang iyong koneksyon sa network, %d block ang natanggap sa huling %d oras (%d inaasahan)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Wallet is not password protected. Your funds may be at risk! Goto &quot;Settings&quot; and then select &quot;Encrypt Wallet&quot; to create a password.</source>
        <translation>Ang wallet ay hindi protektado ng password. Maaaring nasa panganib ang iyong mga pondo! Pumunta sa &quot;Mga Setting&quot; at pagkatapos ay piliin ang &quot;I-encrypt ang Wallet&quot; para gumawa ng password.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Could not open deployment configuration CSV file &apos;%s&apos; for reading</source>
        <translation>Babala: Hindi mabuksan ang configuration ng deployment na CSV file na &apos;%s&apos; para sa pagbabasa</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.</source>
        <translation>Babala: Mukhang hindi lubos na sumasang-ayon ang network! Ang ilang mga minero ay lumilitaw na nakakaranas ng mga isyu.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Unknown block versions being mined! It&apos;s possible unknown rules are in effect</source>
        <translation>Babala: Mga hindi kilalang bersyon ng block na mina! Posibleng ang hindi kilalang mga panuntunan ay may bisa</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Wallet file corrupt, data salvaged! Original %s saved as %s in %s; if your balance or transactions are incorrect you should restore from a backup.</source>
        <translation>Babala: Nasira ang file ng Wallet, na-save ang data! Orihinal na %s na na-save bilang %s sa %s; kung ang iyong balanse o mga transaksyon ay hindi tama dapat mong ibalik mula sa isang backup.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.</source>
        <translation>Babala: Mukhang hindi kami lubos na sumasang-ayon sa aming mga kapantay! Maaaring kailanganin mong mag-upgrade, o maaaring kailanganin ng iba pang mga node na mag-upgrade.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You are trying to restore the same wallet which you are trying to replace.</source>
        <translation>Sinusubukan mong ibalik ang parehong wallet na sinusubukan mong palitan.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>You are trying to use -wallet.auto but neither -spendzeroconfchange nor -wallet.instant is turned on</source>
        <translation>Sinusubukan mong gamitin ang -wallet.auto ngunit hindi naka-on ang -spendzeroconfchange o -wallet.instant</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You can not send free transactions if you have configured a -relay.limitFreeRelay of zero</source>
        <translation>Hindi ka maaaring magpadala ng mga libreng transaksyon kung na-configure mo ang isang -relay.limitFreeRelay na zero</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&quot;Restore Wallet&quot; succeeded and a backup of the previous wallet was saved to: %s.


When you click &quot;OK&quot; Nexa will shutdown to complete the process.</source>
        <translation>Nagtagumpay ang &quot;Ibalik ang Wallet&quot; at na-save ang backup ng nakaraang wallet sa: %s.


Kapag na-click mo ang &quot;OK&quot; ang Nexa ay magsasara upang makumpleto ang proseso.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>%s </source>
        <translation>%s </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%s corrupt, salvage failed</source>
        <translation>%s corrupt, nabigo ang pagsagip</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-maxtxpool must be at least %d MB</source>
        <translation>-cache.maxTxPool ay dapat na hindi bababa sa %d MB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-xthinbloomfiltersize must be at least %d Bytes</source>
        <translation>Ang -xthinbloomfiltersize ay dapat na hindi bababa sa %d Bytes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Activating best chain...</source>
        <translation>Ina-activate ang pinakamahusay na chain...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cannot write default address</source>
        <translation>Eya misulat ing default address</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CommitTransaction failed.</source>
        <translation>Nabigo ang Commit Transaction.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright (C) 2015-%i The Bitcoin Unlimited Developers</source>
        <translation>Copyright (C) 2015-%i The Bitcoin Unlimited Developers</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Deployment configuration file &apos;%s&apos; not found</source>
        <translation>Hindi nakita ang deployment configuration file na &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Error initializing wallet database environment %s!</source>
        <translation>Error sa pagsisimula ng kapaligiran ng database ng wallet %s!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s</source>
        <translation>Error sa paglo-load ng %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet corrupted</source>
        <translation>Error sa paglo-load ng %s: Nasira ang pitaka</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet requires newer version of %s</source>
        <translation>Error sa paglo-load ng %s: Nangangailangan ang Wallet ng mas bagong bersyon ng %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: You can&apos;t disable HD on a already existing HD wallet</source>
        <translation>Error sa paglo-load ng %s: Hindi mo maaaring i-disable ang HD sa isang umiiral nang HD wallet</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error: A fatal internal error occurred, see debug.log for details</source>
        <translation>Error: Isang malalang internal na error ang naganap, tingnan ang debug.log para sa mga detalye</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error: Keypool ran out, please call keypoolrefill first</source>
        <translation>Error: Naubos ang keypool, mangyaring tawagan muna ang keypoolrefill</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Importing...</source>
        <translation>Importing...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Incorrect or no genesis block found. Wrong datadir for network?</source>
        <translation>Mali o walang nahanap na genesis block. Maling datadir para sa network?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Initialization sanity check failed. %s is shutting down.</source>
        <translation>Nabigo ang initialization sanity check. Nagsasara ang %s.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds for this token.  Need %d more.</source>
        <translation>Hindi sapat na pondo para sa token na ito. Kailangan ng %d pa.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds or funds not confirmed</source>
        <translation>Hindi sapat na pondo o hindi nakumpirmang pondo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid -onion address: &apos;%s&apos;</source>
        <translation>Di-wastong -onion address: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid netmask specified in -whitelist: &apos;%s&apos;</source>
        <translation>Di-wastong netmask na tinukoy sa -whitelist: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Keypool ran out, please call keypoolrefill first</source>
        <translation>Naubos ang keypool, mangyaring tawagan muna ang keypoolrefill</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading Orphanpool</source>
        <translation>Nilo-load ang Orphanpool</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading TxPool</source>
        <translation>Nilo-load ang TxPool</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading banlist...</source>
        <translation>Nilo-load ang banlist...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Need to specify a port with -whitebind: &apos;%s&apos;</source>
        <translation>Kailangang tumukoy ng port na may -whitebind: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Not enough file descriptors available.</source>
        <translation>Walang sapat na mga deskriptor ng file na magagamit.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Block database...</source>
        <translation>Binubuksan ang Block database...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Coins Cache database...</source>
        <translation>Binubuksan ang database ng Coins Cache...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Description database...</source>
        <translation>Binubuksan ang database ng Paglalarawan ng Token...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Mintage database...</source>
        <translation>Binubuksan ang database ng Token Mintage...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening UTXO database...</source>
        <translation>Binubuksan ang database ng UTXO...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2009-%i The Bitcoin Core Developers</source>
        <translation>Mga Bahagi Copyright (C) 2009-%i Ang Bitcoin Core Developers</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2014-%i The Bitcoin XT Developers</source>
        <translation>Mga Bahagi Copyright (C) 2014-%i Ang Mga Nag-develop ng Bitcoin XT</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune cannot be configured with a negative value.</source>
        <translation>Hindi maaaring i-configure ang prune na may negatibong halaga.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune mode is incompatible with -txindex.</source>
        <translation>Ang prue mode ay hindi tugma sa -txindex.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pruning blockstore...</source>
        <translation>Pruning blockstore...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reaccepting Wallet Transactions</source>
        <translation>Pagtanggap muli ng mga Transaksyon sa Wallet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rescanning...</source>
        <translation>I-scan deng pasibayu...</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Upgrading block database...This could take a while.</source>
        <translation>Ina-upgrade ang block database...Maaaring magtagal ito.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database </source>
        <translation>Pag-upgrade ng database ng txindex</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database...</source>
        <translation>Pag-upgrade ng database ng txindex...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>User Agent comment (%s) contains unsafe characters.</source>
        <translation>Ang komento ng User Agent (%s) ay naglalaman ng mga hindi ligtas na character.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying blocks...</source>
        <translation>Bine-verify ang mga block...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying wallet...</source>
        <translation>Bine-verify ang wallet...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Waiting for Genesis Block...</source>
        <translation>Naghihintay para sa Genesis Block...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet %s resides outside data directory %s</source>
        <translation>Nasa labas ng direktoryo ng data %s ang Wallet %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet needed to be rewritten: restart %s to complete</source>
        <translation>Kailangang muling isulat ang pitaka: i-restart ang %s upang makumpleto</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zapping all transactions from wallet...</source>
        <translation>I-zapping ang lahat ng transaksyon mula sa wallet...</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>Done loading</source>
        <translation>Yari ne ing pamag-load</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error</source>
        <translation>Mali</translation>
    </message>
</context>
</TS>
