<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>AddressBookPage</name>
    <message>
        <location filename="../forms/addressbookpage.ui" line="+30"/>
        <source>Right-click to edit address or label</source>
        <translation>Rechtsklick zum Bearbeiten der Adresse oder der Bezeichnung</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Create a new address</source>
        <translation>Eine neue Adresse erstellen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;New</source>
        <translation>&amp;Neu</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Copy the currently selected address to the system clipboard</source>
        <translation>Ausgewählte Adresse in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopieren</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>C&amp;lose</source>
        <translation>&amp;Schließen</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="+90"/>
        <source>&amp;Copy Address</source>
        <translation>Adresse &amp;kopieren</translation>
    </message>
    <message>
        <location filename="../forms/addressbookpage.ui" line="-53"/>
        <source>Delete the currently selected address from the list</source>
        <translation>Ausgewählte Adresse aus der Liste entfernen</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Daten der aktuellen Ansicht in eine Datei exportieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Export</source>
        <translation>E&amp;xportieren</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>&amp;Delete</source>
        <translation>&amp;Löschen</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="-39"/>
        <source>Choose the address to send coins to</source>
        <translation>Wählen Sie die Adresse aus, an die Sie coins überweisen möchten</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose the address to receive coins with</source>
        <translation>Wählen Sie die Adresse aus, über die Sie coins empfangen wollen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>C&amp;hoose</source>
        <translation>&amp;Auswählen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sending addresses</source>
        <translation>Zahlungsadressen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Receiving addresses</source>
        <translation>Empfangsadressen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>These are your Nexa addresses for sending payments. Always check the amount and the receiving address before sending coins.</source>
        <translation>Dies sind Ihre Nexa-Adressen zum Tätigen von Überweisungen. Bitte prüfen Sie den Betrag und die Empfangsadresse, bevor Sie coins überweisen.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>These are your Nexa addresses for receiving payments. It is recommended to use a new receiving address for each transaction.</source>
        <translation>Dies sind Ihre Nexa-Adressen zum Empfangen von Zahlungen. Es wird empfohlen für jede Transaktion eine neue Empfangsadresse zu verwenden.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Copy &amp;Label</source>
        <translation>&amp;Bezeichnung kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editieren</translation>
    </message>
    <message>
        <location line="+173"/>
        <source>Export Address List</source>
        <translation>Addressliste exportieren</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Kommagetrennte-Datei (*.csv)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Exporting Failed</source>
        <translation>Exportieren fehlgeschlagen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the address list to %1. Please try again.</source>
        <translation>Beim Speichern der Adressliste nach %1 ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut.</translation>
    </message>
</context>
<context>
    <name>AddressTableModel</name>
    <message>
        <location filename="../addresstablemodel.cpp" line="+159"/>
        <source>Label</source>
        <translation>Bezeichnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>(no label)</source>
        <translation>(keine Bezeichnung)</translation>
    </message>
</context>
<context>
    <name>AskPassphraseDialog</name>
    <message>
        <location filename="../forms/askpassphrasedialog.ui" line="+26"/>
        <source>Passphrase Dialog</source>
        <translation>Passphrasendialog</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Enter passphrase</source>
        <translation>Passphrase eingeben</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>New passphrase</source>
        <translation>Neue Passphrase</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Repeat new passphrase</source>
        <translation>Neue Passphrase bestätigen</translation>
    </message>
    <message>
        <location filename="../askpassphrasedialog.cpp" line="+47"/>
        <source>Encrypt wallet</source>
        <translation>Wallet verschlüsseln</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to unlock the wallet.</source>
        <translation>Dieser Vorgang benötigt Ihre Passphrase, um die Wallet zu entsperren.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unlock wallet</source>
        <translation>Wallet entsperren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to decrypt the wallet.</source>
        <translation>Dieser Vorgang benötigt Ihre Passphrase, um die Wallet zu entschlüsseln.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Decrypt wallet</source>
        <translation>Wallet entschlüsseln</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change passphrase</source>
        <translation>Passphrase ändern</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Confirm wallet encryption</source>
        <translation>Wallet-Verschlüsselung bestätigen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: If you encrypt your wallet and lose your passphrase, you will &lt;b&gt;LOSE ALL OF YOUR NEXA COINS&lt;/b&gt;!</source>
        <translation>Warnung: Wenn Sie Ihre Wallet verschlüsseln und Ihre Passphrase verlieren, werden Sie &lt;b&gt;alle Ihre Nexa coins verlieren&lt;/b&gt;!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Are you sure you wish to encrypt your wallet?</source>
        <translation>Sind Sie sich sicher, dass Sie Ihre Wallet verschlüsseln möchten?</translation>
    </message>
    <message>
        <location line="+119"/>
        <location line="+29"/>
        <source>Warning: The Caps Lock key is on!</source>
        <translation>Warnung: Die Feststelltaste ist aktiviert!</translation>
    </message>
    <message>
        <location line="-140"/>
        <location line="+61"/>
        <source>Wallet encrypted</source>
        <translation>Wallet verschlüsselt</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Enter the new passphrase to the wallet.&lt;br/&gt;Please use a passphrase of &lt;b&gt;ten or more random characters&lt;/b&gt;, or &lt;b&gt;eight or more words&lt;/b&gt;.</source>
        <translation>Geben Sie die neue Passphrase für die Wallet ein.&lt;br&gt;Bitte benutzen Sie eine Passphrase bestehend aus &lt;b&gt;zehn oder mehr zufälligen Zeichen&lt;/b&gt; oder &lt;b&gt;acht oder mehr Wörtern&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Enter the old passphrase and new passphrase to the wallet.</source>
        <translation>Geben Sie die alte und neue Wallet-Passphrase ein.</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>%1 will close now to finish the encryption process. Remember that encrypting your wallet cannot fully protect your coins from being stolen by malware infecting your computer.</source>
        <translation>%1 wird jetzt geschlossen, um den Verschlüsselungsprozess abzuschließen. Denken Sie daran, dass die Verschlüsselung Ihrer Brieftasche Ihre Münzen nicht vollständig vor dem Diebstahl durch Malware schützen kann, die Ihren Computer infiziert.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>IMPORTANT: Any previous backups you have made of your wallet file should be replaced with the newly generated, encrypted wallet file. For security reasons you should no longer keep previous unencrypted backups as your funds will be at risk if someone gains access to them.</source>
        <translation>WICHTIG: Alle vorherigen Sicherungen Ihrer Wallet-Datei sollten durch die neu generierte, verschlüsselte Wallet-Datei ersetzt werden. Aus Sicherheitsgründen sollten Sie frühere unverschlüsselte Backups nicht mehr aufbewahren, da Ihre Gelder gefährdet sind, wenn jemand Zugriff darauf erhält.</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+8"/>
        <location line="+42"/>
        <location line="+6"/>
        <source>Wallet encryption failed</source>
        <translation>Wallet-Verschlüsselung fehlgeschlagen</translation>
    </message>
    <message>
        <location line="-55"/>
        <source>Wallet encryption failed due to an internal error. Your wallet was not encrypted.</source>
        <translation>Die Wallet-Verschlüsselung ist aufgrund eines internen Fehlers fehlgeschlagen. Ihre Wallet wurde nicht verschlüsselt.</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+48"/>
        <source>The supplied passphrases do not match.</source>
        <translation>Die eingegebenen Passphrasen stimmen nicht überein.</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Wallet unlock failed</source>
        <translation>Wallet-Entsperrung fehlgeschlagen</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+11"/>
        <location line="+19"/>
        <source>The passphrase entered for the wallet decryption was incorrect.</source>
        <translation>Die eingegebene Passphrase zur Wallet-Entschlüsselung war nicht korrekt.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Wallet decryption failed</source>
        <translation>Wallet-Entschlüsselung fehlgeschlagen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Wallet passphrase was successfully changed.</source>
        <translation>Die Wallet-Passphrase wurde erfolgreich geändert.</translation>
    </message>
</context>
<context>
    <name>BanTableModel</name>
    <message>
        <location filename="../bantablemodel.cpp" line="+86"/>
        <source>IP/Netmask</source>
        <translation>IP/Netzmaske</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Banned Until</source>
        <translation>Gesperrt bis</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation>User-Agent</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ban Reason</source>
        <translation>Grund des Ausschlusses</translation>
    </message>
</context>
<context>
    <name>BitcoinGUI</name>
    <message>
        <location filename="../nexagui.cpp" line="+351"/>
        <source>Sign &amp;message...</source>
        <translation>Nachricht s&amp;ignieren...</translation>
    </message>
    <message>
        <location line="+435"/>
        <source>Synchronizing with network...</source>
        <translation>Synchronisiere mit Netzwerk...</translation>
    </message>
    <message>
        <location line="-556"/>
        <source>&amp;Overview</source>
        <translation>&amp;Übersicht</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Node</source>
        <translation>Knoten</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Show general overview of wallet</source>
        <translation>Allgemeine Wallet-Übersicht anzeigen</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Request payments (generates QR codes and %1: URIs)</source>
        <translation>Zahlungen anfordern (erzeugt QR-Codes und %1: URIs)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Transactions</source>
        <translation>&amp;Transaktionen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse transaction history</source>
        <translation>Transaktionsverlauf durchsehen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Tokens</source>
        <translation>&amp;Token</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse or Send Tokens</source>
        <translation>Durchsuchen oder senden Sie Token</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Token History</source>
        <translation>&amp;Token-Verlauf</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse Token History</source>
        <translation>Durchsuchen Sie den Token-Verlauf</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>E&amp;xit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit application</source>
        <translation>Anwendung beenden</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;About %1</source>
        <translation>&amp;Über %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about %1</source>
        <translation>Informationen über %1 anzeigen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>About &amp;Qt</source>
        <translation>Über &amp;Qt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about Qt</source>
        <translation>Informationen über Qt anzeigen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Options...</source>
        <translation>&amp;Konfiguration...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify configuration options for %1</source>
        <translation>Konfigurationsoptionen für %1 ändern</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Unlimited...</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify Bitcoin Unlimited Options</source>
        <translation>Bitcoin-Unlimited Optionen ändern</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Encrypt Wallet...</source>
        <translation>Wallet &amp;verschlüsseln...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Backup Wallet...</source>
        <translation>Wallet &amp;sichern...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Restore Wallet...</source>
        <translation>&amp;Wallet wiederherstellen...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Restore wallet from another location</source>
        <translation>Wallet von einem anderen Ort wiederherstellen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Change Passphrase...</source>
        <translation>Passphrase &amp;ändern...</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sending addresses...</source>
        <translation>&amp;Zahlungsadressen...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Receiving addresses...</source>
        <translation>&amp;Empfangsadressen...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open &amp;URI...</source>
        <translation>&amp;URI öffnen...</translation>
    </message>
    <message>
        <location line="+421"/>
        <source>Importing blocks from disk...</source>
        <translation>Importiere Blöcke von Datenträger...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindexing blocks on disk...</source>
        <translation>Reindiziere Blöcke auf Datenträger...</translation>
    </message>
    <message>
        <location line="-554"/>
        <source>Send coins to a Nexa address</source>
        <translation>Coins an eine Nexa-Adresse überweisen</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Backup wallet to another location</source>
        <translation>Eine Wallet-Sicherungskopie erstellen und abspeichern</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Change the passphrase used for wallet encryption</source>
        <translation>Ändert die Passphrase, die für die Wallet-Verschlüsselung benutzt wird</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Debug window</source>
        <translation>&amp;Debugfenster</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open debugging and diagnostic console</source>
        <translation>Debugging- und Diagnosekonsole öffnen</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>&amp;Verify message...</source>
        <translation>Nachricht &amp;verifizieren...</translation>
    </message>
    <message>
        <location line="+522"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="-785"/>
        <source>Wallet</source>
        <translation>Wallet</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>&amp;Send</source>
        <translation>&amp;Überweisen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Receive</source>
        <translation>&amp;Empfangen</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>&amp;Show / Hide</source>
        <translation>&amp;Anzeigen / Verstecken</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show or hide the main Window</source>
        <translation>Das Hauptfenster anzeigen oder verstecken</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Encrypt the private keys that belong to your wallet</source>
        <translation>Verschlüsselt die zu Ihrer Wallet gehörenden privaten Schlüssel</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Sign messages with your Nexa addresses to prove you own them</source>
        <translation>Nachrichten signieren, um den Besitz Ihrer Nexa-Adressen zu beweisen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Verify messages to ensure they were signed with specified Nexa addresses</source>
        <translation>Nachrichten verifizieren, um sicherzustellen, dass diese mit den angegebenen Nexa-Adressen signiert wurden</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&amp;Settings</source>
        <translation>&amp;Einstellungen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Tabs toolbar</source>
        <translation>Registerkartenleiste</translation>
    </message>
    <message>
        <location line="-95"/>
        <source>Show the list of used sending addresses and labels</source>
        <translation>Liste verwendeter Zahlungsadressen und Bezeichnungen anzeigen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show the list of used receiving addresses and labels</source>
        <translation>Liste verwendeter Empfangsadressen und Bezeichnungen anzeigen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Command-line options</source>
        <translation>&amp;Kommandozeilenoptionen</translation>
    </message>
    <message numerus="yes">
        <location line="+390"/>
        <source>%n active connection(s) to Nexa network</source>
        <translation>
            <numerusform>%n aktive Verbindung zum Nexa-Netzwerk</numerusform>
            <numerusform>%n aktive Verbindungen zum Nexa-Netzwerk</numerusform>
        </translation>
    </message>
    <message>
        <location line="+34"/>
        <source>No block source available...</source>
        <translation>Keine Blockquelle verfügbar...</translation>
    </message>
    <message numerus="yes">
        <location line="+9"/>
        <source>Processed %n block(s) of transaction history.</source>
        <translation>
            <numerusform>%n Block des Transaktionsverlaufs verarbeitet.</numerusform>
            <numerusform>%n Blöcke des Transaktionsverlaufs verarbeitet.</numerusform>
        </translation>
    </message>
    <message>
        <location line="+27"/>
        <source>%1 behind</source>
        <translation>%1 im Rückstand</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Last received block was generated %1 ago.</source>
        <translation>Der letzte empfangene Block ist %1 alt.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transactions after this will not yet be visible.</source>
        <translation>Transaktionen hiernach werden noch nicht angezeigt.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Information</source>
        <translation>Hinweis</translation>
    </message>
    <message>
        <location line="-86"/>
        <source>Up to date</source>
        <translation>Auf aktuellem Stand</translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Open a %1: URI or payment request</source>
        <translation>Öffnen Sie %1: URI oder Zahlungsanforderung</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show the %1 help message to get a list with possible Nexa command-line options</source>
        <translation>Zeigen Sie die %1-Hilfemeldung an, um eine Liste mit möglichen Nexa-Befehlszeilenoptionen zu erhalten</translation>
    </message>
    <message>
        <location line="+194"/>
        <source>%1 client</source>
        <translation>%1 Kunde</translation>
    </message>
    <message>
        <location line="+268"/>
        <source>Catching up...</source>
        <translation>Hole auf...</translation>
    </message>
    <message>
        <location line="+164"/>
        <source>Date: %1
</source>
        <translation>Datum: %1
</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Amount: %1
</source>
        <translation>Betrag: %1
</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type: %1
</source>
        <translation>Typ: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Label: %1
</source>
        <translation>Bezeichnung: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address: %1
</source>
        <translation>Adresse: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent transaction</source>
        <translation>Gesendete Transaktion</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Incoming transaction</source>
        <translation>Eingehende Transaktion</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>HD key generation is &lt;b&gt;enabled&lt;/b&gt;</source>
        <translation>HD-Schlüsselgenerierung ist &lt;b&gt;aktiviert&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>HD key generation is &lt;b&gt;disabled&lt;/b&gt;</source>
        <translation>Die Generierung von HD-Schlüsseln ist &lt;b&gt;deaktiviert&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt;</source>
        <translation>Wallet ist &lt;b&gt;verschlüsselt&lt;/b&gt; und aktuell &lt;b&gt;entsperrt&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;locked&lt;/b&gt;</source>
        <translation>Wallet ist &lt;b&gt;verschlüsselt&lt;/b&gt; und aktuell &lt;b&gt;gesperrt&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>CoinControlDialog</name>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="+14"/>
        <source>Coin Selection</source>
        <translation>Münzauswahl (&quot;Coin Control&quot;)</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Quantity:</source>
        <translation>Anzahl:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Bytes:</source>
        <translation>Byte:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Amount:</source>
        <translation>Betrag:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Priority:</source>
        <translation>Priorität:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Fee:</source>
        <translation>Gebühr:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation>Staub:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation>Abzüglich Gebühr:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Wechselgeld:</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>(un)select max inputs</source>
        <translation>Alles (de)selektieren</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Tree mode</source>
        <translation>Baumansicht</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>List mode</source>
        <translation>Listenansicht</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Amount</source>
        <translation>Betrag</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with label</source>
        <translation>Empfangen über Bezeichnung</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with address</source>
        <translation>Empfangen über Adresse</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmations</source>
        <translation>Bestätigungen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed</source>
        <translation>Bestätigt</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Priority</source>
        <translation>Priorität</translation>
    </message>
    <message>
        <location filename="../coincontroldialog.cpp" line="+45"/>
        <source>Copy address</source>
        <translation>Adresse kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Bezeichnung kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+29"/>
        <source>Copy amount</source>
        <translation>Betrag kopieren</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Copy transaction ID</source>
        <translation>Transaktions-ID kopieren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lock unspent</source>
        <translation>Nicht ausgegebenen Betrag sperren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unlock unspent</source>
        <translation>Nicht ausgegebenen Betrag entsperren</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Copy quantity</source>
        <translation>Anzahl kopieren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy fee</source>
        <translation>Gebühr kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Abzüglich Gebühr kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Byte kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Priorität kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation>Staub kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>Wechselgeld kopieren</translation>
    </message>
    <message>
        <location line="+352"/>
        <source>highest</source>
        <translation>am höchsten</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>higher</source>
        <translation>höher</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>high</source>
        <translation>hoch</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium-high</source>
        <translation>mittel-hoch</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium</source>
        <translation>mittel</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low-medium</source>
        <translation>niedrig-mittel</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low</source>
        <translation>niedrig</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lower</source>
        <translation>niedriger</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lowest</source>
        <translation>am niedrigsten</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>(%1 locked)</source>
        <translation>(%1 gesperrt)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>none</source>
        <translation>keine</translation>
    </message>
    <message>
        <location line="+167"/>
        <source>This label turns red if the transaction size is greater than 1000 bytes.</source>
        <translation>Diese Bezeichnung wird rot, wenn die Transaktion größer als 1000 Byte ist.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>This label turns red if the priority is smaller than &quot;medium&quot;.</source>
        <translation>Diese Bezeichnung wird rot, wenn die Priorität niedriger als &quot;mittel&quot; ist.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>This label turns red if any recipient receives an amount smaller than %1.</source>
        <translation>Diese Bezeichnung wird rot, wenn irgendein Empfänger einen Betrag kleiner als %1 erhält.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Can vary +/- %1 satoshi(s) per input.</source>
        <translation>Kann pro Eingabe um +/- %1 Satoshi(s) abweichen.</translation>
    </message>
    <message>
        <location line="-41"/>
        <source>yes</source>
        <translation>ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>no</source>
        <translation>nein</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+8"/>
        <source>This means a fee of at least %1 per kB is required.</source>
        <translation>Das bedeutet, dass eine Gebühr von mindestens %1 pro kB erforderlich ist.</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Can vary +/- 1 byte per input.</source>
        <translation>Kann um +/- 1 Byte pro Eingabe variieren.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transactions with higher priority are more likely to get included into a block.</source>
        <translation>Transaktionen mit höherer Priorität haben eine größere Chance in einen Block aufgenommen zu werden.</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+64"/>
        <source>(no label)</source>
        <translation>(keine Bezeichnung)</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>change from %1 (%2)</source>
        <translation>Wechselgeld von %1 (%2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(change)</source>
        <translation>(Wechselgeld)</translation>
    </message>
</context>
<context>
    <name>EditAddressDialog</name>
    <message>
        <location filename="../forms/editaddressdialog.ui" line="+14"/>
        <source>Edit Address</source>
        <translation>Adresse bearbeiten</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Label</source>
        <translation>&amp;Bezeichnung</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The label associated with this address list entry</source>
        <translation>Bezeichnung, die dem Adresslisteneintrag zugeordnet ist</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>The address associated with this address list entry. This can only be modified for sending addresses.</source>
        <translation>Adresse, die dem Adresslisteneintrag zugeordnet ist. Diese kann nur bei Zahlungsadressen verändert werden.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>&amp;Address</source>
        <translation>&amp;Adresse</translation>
    </message>
    <message>
        <location filename="../editaddressdialog.cpp" line="+25"/>
        <source>New receiving address</source>
        <translation>Neue Empfangsadresse</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New sending address</source>
        <translation>Neue Zahlungsadresse</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit receiving address</source>
        <translation>Empfangsadresse bearbeiten</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit sending address</source>
        <translation>Zahlungsadresse bearbeiten</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>The entered address &quot;%1&quot; is already in the address book.</source>
        <translation>Die eingegebene Adresse &quot;%1&quot; befindet sich bereits im Adressbuch.</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>The entered address &quot;%1&quot; is not a valid Nexa address.</source>
        <translation>Die eingegebene Adresse &quot;%1&quot; ist keine gültige Nexa-Adresse.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not unlock wallet.</source>
        <translation>Wallet konnte nicht entsperrt werden.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New key generation failed.</source>
        <translation>Erzeugung eines neuen Schlüssels fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>EditTokenDialog</name>
    <message>
        <location filename="../forms/edittokendialog.ui" line="+14"/>
        <source>Edit Token</source>
        <translation>Token bearbeiten</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Group ID</source>
        <translation>Gruppen-ID</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The group ID of the token to be edited.</source>
        <translation>Die Gruppen-ID des zu bearbeitenden Tokens.</translation>
    </message>
    <message>
        <location filename="../edittokendialog.cpp" line="+25"/>
        <source>Add new token</source>
        <translation>Neues Token hinzufügen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove existing token</source>
        <translation>Vorhandenes Token entfernen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Token Dialog Error! Please notify a developer</source>
        <translation>Token-Dialogfehler! Bitte benachrichtigen Sie einen Entwickler</translation>
    </message>
    <message>
        <location line="+23"/>
        <location line="+20"/>
        <source>Group ID required.</source>
        <translation>Gruppen-ID erforderlich.</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Failed to add token: Invalid Group ID.</source>
        <translation>Token konnte nicht hinzugefügt werden: Ungültige Gruppen-ID.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Failed to add token.</source>
        <translation>Token konnte nicht hinzugefügt werden.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Failed to remove token.</source>
        <translation>Token konnte nicht entfernt werden.</translation>
    </message>
</context>
<context>
    <name>FreespaceChecker</name>
    <message>
        <location filename="../intro.cpp" line="+75"/>
        <source>A new data directory will be created.</source>
        <translation>Es wird ein neues Datenverzeichnis angelegt.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Directory already exists. Add %1 if you intend to create a new directory here.</source>
        <translation>Verzeichnis existiert bereits. Fügen Sie %1 an, wenn Sie beabsichtigen hier ein neues Verzeichnis anzulegen.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Path already exists, and is not a directory.</source>
        <translation>Pfad existiert bereits und ist kein Verzeichnis.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot create data directory here.</source>
        <translation>Datenverzeichnis kann hier nicht angelegt werden.</translation>
    </message>
</context>
<context>
    <name>HelpMessageDialog</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+39"/>
        <source>version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+2"/>
        <source>(%1-bit)</source>
        <translation>(%1-Bit)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>About %1</source>
        <translation>Über %1</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Command-line options</source>
        <translation>Kommandozeilenoptionen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Usage:</source>
        <translation>Benutzung:</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>command-line options</source>
        <translation>Kommandozeilenoptionen</translation>
    </message>
</context>
<context>
    <name>Intro</name>
    <message>
        <location filename="../forms/intro.ui" line="+14"/>
        <source>Welcome</source>
        <translation>Willkommen</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Welcome to %1.</source>
        <translation>Willkommen bei %1.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>As this is the first time the program is launched, you can choose where %1 will store its data.</source>
        <translation>Da dies das erste Mal ist, dass das Programm gestartet wird, können Sie auswählen, wo %1 seine Daten speichern soll.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 will download and store a copy of the Nexa block chain. At least %2GB of data will be stored in this directory, and it will grow over time. The wallet will also be stored in this directory.</source>
        <translation>%1 wird eine Kopie der Nexa-Blockchain herunterladen und speichern. Mindestens %2 GB an Daten werden in diesem Verzeichnis gespeichert, und es wird mit der Zeit wachsen. Die Brieftasche wird auch in diesem Verzeichnis gespeichert.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Use the default data directory</source>
        <translation>Standard-Datenverzeichnis verwenden</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use a custom data directory:</source>
        <translation>Ein benutzerdefiniertes Datenverzeichnis verwenden:</translation>
    </message>
    <message>
        <location filename="../intro.cpp" line="+81"/>
        <source>Error: Specified data directory &quot;%1&quot; cannot be created.</source>
        <translation>Fehler: Angegebenes Datenverzeichnis &quot;%1&quot; kann nicht angelegt werden.</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%n GB of free space available</source>
        <translation>
            <numerusform>%n GB freier Speicherplatz verfügbar</numerusform>
            <numerusform>%n GB freier Speicherplatz verfügbar</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>(of %n GB needed)</source>
        <translation>
            <numerusform>(von benötigtem %n GB)</numerusform>
            <numerusform>(von benötigten %n GB)</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessThanValidator</name>
    <message>
        <location filename="../unlimiteddialog.cpp" line="+486"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>Upstream-Traffic-Shaping-Parameter dürfen nicht leer sein</translation>
    </message>
</context>
<context>
    <name>ModalOverlay</name>
    <message>
        <location filename="../forms/modaloverlay.ui" line="+14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet. This means that recent transactions will not be visible, and the balance will not be up-to-date until this process has completed.</source>
        <translation>Die angezeigten Informationen können veraltet sein. Ihre Brieftasche synchronisiert sich automatisch mit dem Nexa-Netzwerk, nachdem eine Verbindung hergestellt wurde, aber dieser Vorgang ist noch nicht abgeschlossen. Dies bedeutet, dass die letzten Transaktionen nicht sichtbar sind und der Saldo nicht auf dem neuesten Stand ist, bis dieser Vorgang abgeschlossen ist.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Spending coins may not be possible during that phase!</source>
        <translation>Das Ausgeben von Münzen ist in dieser Phase möglicherweise nicht möglich!</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Amount of blocks left</source>
        <translation>Blöcke übrig</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+26"/>
        <source>unknown...</source>
        <translation>Unbekannt...</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Last block time</source>
        <translation>Letzte Blockzeit</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Progress</source>
        <translation>Fortschritt</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>~</source>
        <translation>~</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Progress increase per Hour</source>
        <translation>Fortschrittssteigerung pro Stunde</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+20"/>
        <source>calculating...</source>
        <translation>rechnen...</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Estimated time left until synced</source>
        <translation>Geschätzte verbleibende Zeit bis zur Synchronisierung</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Hide</source>
        <translation>Ausblenden</translation>
    </message>
    <message>
        <location filename="../modaloverlay.cpp" line="+140"/>
        <source>Unknown. Reindexing (%1)...</source>
        <translation>Unbekannt. Neuindizierung (%1)...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Reindexing...</source>
        <translation>Unbekannt. Neuindizierung...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown. Syncing Headers (%1)...</source>
        <translation>Unbekannt. Header werden synchronisiert (%1)...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Syncing Headers...</source>
        <translation>Unbekannt. Kopfzeilen synchronisieren...</translation>
    </message>
</context>
<context>
    <name>OpenURIDialog</name>
    <message>
        <location filename="../forms/openuridialog.ui" line="+14"/>
        <source>Open URI</source>
        <translation>URI öffnen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open payment request from URI or file</source>
        <translation>Zahlungsanforderung über URI oder aus Datei öffnen</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>URI:</source>
        <translation>URI:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Select payment request file</source>
        <translation>Zahlungsanforderungsdatei auswählen</translation>
    </message>
    <message>
        <location filename="../openuridialog.cpp" line="+40"/>
        <source>Select payment request file to open</source>
        <translation>Zu öffnende Zahlungsanforderungsdatei auswählen</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../forms/optionsdialog.ui" line="+14"/>
        <source>Options</source>
        <translation>Konfiguration</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Main</source>
        <translation>&amp;Allgemein</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Number of script &amp;verification threads</source>
        <translation>Anzahl an Skript-&amp;Verifizierungs-Threads</translation>
    </message>
    <message>
        <location line="+227"/>
        <source>Allow incoming connections</source>
        <translation>Erlaubt eingehende Verbindungen</translation>
    </message>
    <message>
        <location line="+44"/>
        <location line="+187"/>
        <source>IP address of the proxy (e.g. IPv4: 127.0.0.1 / IPv6: ::1)</source>
        <translation>IP-Adresse des Proxies (z.B. IPv4: 127.0.0.1 / IPv6: ::1)</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Minimize instead of exit the application when the window is closed. When this option is enabled, the application will be closed only after selecting Exit in the menu.</source>
        <translation>Minimiert die Anwendung anstatt sie zu beenden wenn das Fenster geschlossen wird. Wenn dies aktiviert ist, müssen Sie die Anwendung über &quot;Beenden&quot; im Menü schließen.</translation>
    </message>
    <message>
        <location line="+80"/>
        <location line="+13"/>
        <source>Third party URLs (e.g. a block explorer) that appear in the transactions tab as context menu items. %s in the URL is replaced by transaction hash. Multiple URLs are separated by vertical bar |.</source>
        <translation>Externe URLs (z.B. ein Block-Explorer), die im Kontextmenü des Transaktionsverlaufs eingefügt werden. In der URL wird %s durch den Transaktionshash ersetzt. Bei Angabe mehrerer URLs müssen diese durch &quot;|&quot; voneinander getrennt werden.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Third party transaction URLs</source>
        <translation>Externe Transaktions-URLs</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Active command-line options that override above options:</source>
        <translation>Aktive Kommandozeilenoptionen, die obige Konfiguration überschreiben:</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Reset all client options to default.</source>
        <translation>Setzt die Clientkonfiguration auf Standardwerte zurück.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>Konfiguration &amp;zurücksetzen</translation>
    </message>
    <message>
        <location line="-504"/>
        <source>&amp;Network</source>
        <translation>&amp;Netzwerk</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>(0 = auto, &lt;0 = leave that many cores free)</source>
        <translation>(0 = automatisch, &lt;0 = so viele Kerne frei lassen)</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>W&amp;allet</source>
        <translation>W&amp;allet</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Expert</source>
        <translation>Erweiterte Wallet-Optionen</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Enable coin &amp;control features</source>
        <translation>&quot;&amp;Münzkontrolle Funktionen aktivieren</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>If you disable the spending of unconfirmed change, the change from a transaction cannot be used until that transaction has at least one confirmation. This also affects how your balance is computed.</source>
        <translation>Wenn Sie das Ausgeben von unbestätigtem Wechselgeld deaktivieren, kann das Wechselgeld einer Transaktion nicht verwendet werden, bis es mindestens eine Bestätigung erhalten hat. Dies wirkt sich auf die Berechnung des Kontostands aus.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Spend unconfirmed change</source>
        <translation>&amp;Unbestätigtes Wechselgeld darf ausgegeben werden</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Instant Transactions is enabled you can spend unconfirmed transactions immediately.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wenn Soforttransaktionen aktiviert sind, können Sie unbestätigte Transaktionen sofort ausgeben.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Instant Transactions</source>
        <translation>&amp;Sofortige Transaktionen</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When creating and sending transactions, auto consolidate will, if required, automatically create a chain of transactions which have inputs no greater than the consensus input limit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Beim Erstellen und Senden von Transaktionen erstellt die automatische Konsolidierung bei Bedarf automatisch eine Kette von Transaktionen, deren Eingaben nicht größer als das Konsenseingabelimit sind.&lt;/p&gt;&lt;/body &gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Auto Consolidate</source>
        <translation>Automatisch konsolidieren</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>&amp;Rescan wallet on startup</source>
        <translation>&amp;Scannen Sie die Brieftasche beim Start erneut</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Token Whitelist is enabled you can control what tokens appear in your wallet.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wenn die Token-Whitelist aktiviert ist, können Sie steuern, welche Token in Ihrer Brieftasche erscheinen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Token Whitelist</source>
        <translation>&amp;Token-Whitelist</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Automatically open the Nexa client port on the router. This only works when your router supports UPnP and it is enabled.</source>
        <translation>Automatisch den Nexa-Clientport auf dem Router öffnen. Dies funktioniert nur, wenn Ihr Router UPnP unterstützt und dies aktiviert ist.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Map port using &amp;UPnP</source>
        <translation>Portweiterleitung via &amp;UPnP</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Accept connections from outside.</source>
        <translation>Akzeptieren Sie Verbindungen von außen.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Connect to the Nexa network through a SOCKS5 proxy.</source>
        <translation>Über einen SOCKS5-Proxy mit dem Nexa-Netzwerk verbinden.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Connect through SOCKS5 proxy (default proxy):</source>
        <translation>Über einen SOCKS5-Proxy &amp;verbinden (Standardproxy):</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+187"/>
        <source>Proxy &amp;IP:</source>
        <translation>Proxy-&amp;IP:</translation>
    </message>
    <message>
        <location line="-155"/>
        <location line="+187"/>
        <source>&amp;Port:</source>
        <translation>&amp;Port:</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>Port of the proxy (e.g. 9050)</source>
        <translation>Port des Proxies (z.B. 9050)</translation>
    </message>
    <message>
        <location line="-163"/>
        <source>Used for reaching peers via:</source>
        <translation>Benutzt um Gegenstellen zu erreichen über:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tor</source>
        <translation>Tor</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Connect to the Nexa network through a separate SOCKS5 proxy for Tor hidden services.</source>
        <translation>Über einen separaten SOCKS5 Proxy für Tor Services mit dem Nexat Netzwerk verbinden.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Use separate SOCKS5 proxy to reach peers via Tor hidden services:</source>
        <translation>Separaten SOCKS5-Proxy verwenden, um Gegenstellen über versteckte Tor-Dienste zu erreichen:</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>&amp;Window</source>
        <translation>&amp;Programmfenster</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show only a tray icon after minimizing the window.</source>
        <translation>Nur ein Symbol im Infobereich anzeigen, nachdem das Programmfenster minimiert wurde.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Minimize to the tray instead of the taskbar</source>
        <translation>In den Infobereich anstatt in die Taskleiste &amp;minimieren</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>M&amp;inimize on close</source>
        <translation>Beim Schließen m&amp;inimieren</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Display</source>
        <translation>Anzei&amp;ge</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>User Interface &amp;language:</source>
        <translation>&amp;Sprache der Benutzeroberfläche:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The user interface language can be set here. This setting will take effect after restarting %1.</source>
        <translation>Hier kann die Sprache der Benutzeroberfläche eingestellt werden. Diese Einstellung wird nach dem Neustart von %1 wirksam.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Unit to show amounts in:</source>
        <translation>&amp;Einheit der Beträge:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Choose the default subdivision unit to show in the interface and when sending coins.</source>
        <translation>Wählen Sie die standardmäßige Untereinheit, die in der Benutzeroberfläche und beim Überweisen von coins angezeigt werden soll.</translation>
    </message>
    <message>
        <location line="-518"/>
        <source>Whether to show coin control features or not.</source>
        <translation>Legt fest, ob die Münzkontrolle-Funktionen angezeigt werden.</translation>
    </message>
    <message>
        <location line="-137"/>
        <source>Automatically start %1 after logging in to the system.</source>
        <translation>%1 automatisch starten, nachdem Sie sich beim System angemeldet haben.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Start %1 on system login</source>
        <translation>&amp;Starte %1 bei der Systemanmeldung</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a, one time only, full database reindex on the next startup.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Initiieren Sie automatisch eine einmalige vollständige Neuindizierung der Datenbank beim nächsten Start.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindex on startup</source>
        <translation>Beim Start neu indizieren</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a full blockchain resynchronization on the next startup (one time only).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Initiieren Sie beim nächsten Start automatisch eine vollständige Blockchain-Neusynchronisierung (nur einmal).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Resynchronize block data on startup</source>
        <translation>Blockdaten beim Start neu synchronisieren</translation>
    </message>
    <message>
        <location line="+277"/>
        <location line="+23"/>
        <location line="+23"/>
        <source>Shows if the supplied default SOCKS5 proxy is used to reach peers via this network type.</source>
        <translation>Zeigt an, ob der mitgelieferte Standard-SOCKS5-Proxy verwendet wird, um Peers über diesen Netzwerktyp zu erreichen.</translation>
    </message>
    <message>
        <location line="+387"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>A&amp;bbrechen</translation>
    </message>
    <message>
        <location filename="../optionsdialog.cpp" line="+135"/>
        <source>default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>none</source>
        <translation>keine</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Confirm options reset</source>
        <translation>Zurücksetzen der Konfiguration bestätigen</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+27"/>
        <source>Client restart required to activate changes.</source>
        <translation>Clientneustart nötig, um die Änderungen zu aktivieren.</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>Client wird beendet. Möchten Sie den Vorgang fortsetzen?</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>This change would require a client restart.</source>
        <translation>Diese Änderung würde einen Clientneustart benötigen.</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>The supplied proxy address is invalid.</source>
        <translation>Die eingegebene Proxyadresse ist ungültig.</translation>
    </message>
</context>
<context>
    <name>OverviewPage</name>
    <message>
        <location filename="../forms/overviewpage.ui" line="+14"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location line="+59"/>
        <location line="+386"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet.</source>
        <translation>Die angezeigten Informationen sind möglicherweise nicht mehr aktuell. Ihre Wallet wird automatisch synchronisiert, nachdem eine Verbindung zum Nexa-Netzwerk hergestellt wurde. Dieser Prozess ist jedoch derzeit noch nicht abgeschlossen.</translation>
    </message>
    <message>
        <location line="-139"/>
        <source>Watch-only:</source>
        <translation>Beobachtet:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Available:</source>
        <translation>Verfügbar:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Your current spendable balance</source>
        <translation>Ihr aktuell verfügbarer Kontostand</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Pending:</source>
        <translation>Ausstehend:</translation>
    </message>
    <message>
        <location line="-236"/>
        <source>Total of transactions that have yet to be confirmed, and do not yet count toward the spendable balance</source>
        <translation>Betrag aus unbestätigten Transaktionen, der noch nicht im aktuell verfügbaren Kontostand enthalten ist</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Immature:</source>
        <translation>Unreif:</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Mined balance that has not yet matured</source>
        <translation>Erarbeiteter Betrag der noch nicht gereift ist</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Balances</source>
        <translation>Kontostände</translation>
    </message>
    <message>
        <location line="+161"/>
        <source>Total:</source>
        <translation>Gesamtbetrag:</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Your current total balance</source>
        <translation>Aktueller Gesamtbetrag aus obigen Kategorien</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Your current balance in watch-only addresses</source>
        <translation>Ihr aktueller Kontostand beobachteter Adressen</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Spendable:</source>
        <translation>Verfügbar:</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Recent transactions</source>
        <translation>Letzte Transaktionen</translation>
    </message>
    <message>
        <location line="-317"/>
        <source>Unconfirmed transactions to watch-only addresses</source>
        <translation>Unbestätigte Transaktionen von beobachteten Adressen</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Mined balance in watch-only addresses that has not yet matured</source>
        <translation>Erarbeiteter Betrag in beobachteten Adressen der noch nicht gereift ist</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Current total balance in watch-only addresses</source>
        <translation>Aktueller Gesamtbetrag in beobachteten Adressen aus obigen Kategorien</translation>
    </message>
</context>
<context>
    <name>PaymentServer</name>
    <message>
        <location filename="../paymentserver.cpp" line="+470"/>
        <location line="+13"/>
        <location line="+10"/>
        <source>URI handling</source>
        <translation>URI-Verarbeitung</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Invalid payment address %1</source>
        <translation>Ungültige Zahlungsadresse %1</translation>
    </message>
    <message>
        <location line="+113"/>
        <location line="+11"/>
        <location line="+35"/>
        <location line="+12"/>
        <location line="+21"/>
        <location line="+98"/>
        <source>Payment request rejected</source>
        <translation>Zahlungsanforderung abgelehnt</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Payment request network doesn&apos;t match client network.</source>
        <translation>Netzwerk der Zahlungsanforderung stimmt nicht mit dem Client-Netzwerk überein.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Payment request is not initialized.</source>
        <translation>Zahlungsanforderung ist nicht initialisiert.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Requested payment amount of %1 is too small (considered dust).</source>
        <translation>Angeforderter Zahlungsbetrag in Höhe von %1 ist zu niedrig und wurde als &quot;Dust&quot; eingestuft.</translation>
    </message>
    <message>
        <location line="-288"/>
        <location line="+240"/>
        <location line="+47"/>
        <location line="+126"/>
        <location line="+15"/>
        <location line="+15"/>
        <source>Payment request error</source>
        <translation>fehlerhafte Zahlungsanforderung</translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Cannot start click-to-pay handler</source>
        <translation>Click-to-Pay-Handler kann nicht gestartet werden</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Payment request fetch URL is invalid: %1</source>
        <translation>Abruf-URL der Zahlungsanforderung ist ungültig: %1</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>URI cannot be parsed! This can be caused by an invalid Nexa address or malformed URI parameters.</source>
        <translation>URI kann nicht analysiert werden! Dies kann durch eine ungültige Nexa-Adresse oder fehlerhafte URI-Parameter verursacht werden.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Payment request file handling</source>
        <translation>Zahlungsanforderungsdatei-Verarbeitung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Payment request file cannot be read! This can be caused by an invalid payment request file.</source>
        <translation>Zahlungsanforderungsdatei kann nicht gelesen werden! Dies kann durch eine ungültige Zahlungsanforderungsdatei verursacht werden.</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Payment request expired.</source>
        <translation>Zahlungsanforderung abgelaufen.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Unverified payment requests to custom payment scripts are unsupported.</source>
        <translation>Unverifizierte Zahlungsanforderungen an benutzerdefinierte Zahlungsskripte werden nicht unterstützt.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+21"/>
        <source>Invalid payment request.</source>
        <translation>Ungültige Zahlungsanforderung.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Refund from %1</source>
        <translation>Rücküberweisung von %1</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Payment request %1 is too large (%2 bytes, allowed %3 bytes).</source>
        <translation>Zahlungsanforderung %1 ist zu groß (%2 Byte, erlaubt sind %3 Byte).</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error communicating with %1: %2</source>
        <translation>Kommunikationsfehler mit %1: %2</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Payment request cannot be parsed!</source>
        <translation>Zahlungsanforderung kann nicht verarbeitet werden!</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Bad response from server %1</source>
        <translation>Fehlerhafte Antwort vom Server: %1</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Payment acknowledged</source>
        <translation>Zahlung bestätigt</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Network request error</source>
        <translation>fehlerhafte Netzwerkanfrage</translation>
    </message>
</context>
<context>
    <name>PeerTableModel</name>
    <message>
        <location filename="../peertablemodel.cpp" line="+120"/>
        <source>User Agent</source>
        <translation>User-Agent</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Node/Service</source>
        <translation>Knoten/Dienst</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ping Time</source>
        <translation>Pingzeit</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../nexaunits.cpp" line="+187"/>
        <source>Amount</source>
        <translation>Betrag</translation>
    </message>
    <message>
        <location filename="../guiutil.cpp" line="+135"/>
        <source>Enter a NEXA address (e.g. %1)</source>
        <translation>Geben Sie eine NEXA-Adresse ein (z. B. %1)</translation>
    </message>
    <message>
        <location line="+831"/>
        <source>%1 d</source>
        <translation>%1 d</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 h</source>
        <translation>%1 h</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 m</source>
        <translation>%1 m</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+68"/>
        <source>%1 s</source>
        <translation>%1 s</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>None</source>
        <translation>Keine</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 ms</source>
        <translation>%1 ms</translation>
    </message>
    <message numerus="yes">
        <location line="+19"/>
        <source>%n seconds(s)</source>
        <translation>
            <numerusform>%n Sekunde</numerusform>
            <numerusform>%n Sekunden</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n minutes(s)</source>
        <translation>
            <numerusform>%n Minute</numerusform>
            <numerusform>%n Minuten</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n hour(s)</source>
        <translation>
            <numerusform>%n Stunde</numerusform>
            <numerusform>%n Stunden</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n day(s)</source>
        <translation>
            <numerusform>%n Tag</numerusform>
            <numerusform>%n Tage</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <location line="+8"/>
        <source>%n week(s)</source>
        <translation>
            <numerusform>%n Woche</numerusform>
            <numerusform>%n Wochen</numerusform>
        </translation>
    </message>
    <message>
        <location line="-2"/>
        <source>%1 and %2</source>
        <translation>%1 und %2</translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%n year(s)</source>
        <translation>
            <numerusform>%n Jahr</numerusform>
            <numerusform>%n Jahre</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>QRImageWidget</name>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+36"/>
        <source>&amp;Save Image...</source>
        <translation>Grafik &amp;speichern...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy Image</source>
        <translation>Grafik &amp;kopieren</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Save QR Code</source>
        <translation>QR-Code speichern</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>PNG Image (*.png)</source>
        <translation>PNG-Grafik (*.png)</translation>
    </message>
</context>
<context>
    <name>RPCConsole</name>
    <message>
        <location filename="../forms/debugwindow.ui" line="+239"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+17"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+19"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+1753"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+23"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location line="-2715"/>
        <source>Client version</source>
        <translation>Clientversion</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>&amp;Information</source>
        <translation>&amp;Information</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Debug window</source>
        <translation>Debugfenster</translation>
    </message>
    <message>
        <location line="+620"/>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location line="-587"/>
        <source>Using BerkeleyDB version</source>
        <translation>Verwendete BerkeleyDB-Version</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Startup time</source>
        <translation>Startzeit</translation>
    </message>
    <message>
        <location line="+474"/>
        <source>Network</source>
        <translation>Netzwerk</translation>
    </message>
    <message>
        <location line="-467"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Number of connections</source>
        <translation>Anzahl Verbindungen</translation>
    </message>
    <message>
        <location line="+415"/>
        <source>Block chain</source>
        <translation>Blockkette</translation>
    </message>
    <message>
        <location line="-408"/>
        <source>Current number of blocks</source>
        <translation>Aktuelle Anzahl Blöcke</translation>
    </message>
    <message>
        <location line="+884"/>
        <location line="+1694"/>
        <source>Received</source>
        <translation>Empfangen</translation>
    </message>
    <message>
        <location line="-1614"/>
        <location line="+1591"/>
        <source>Sent</source>
        <translation>Übertragen</translation>
    </message>
    <message>
        <location line="-417"/>
        <source>&amp;Peers</source>
        <translation>&amp;Gegenstellen</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Banned peers</source>
        <translation>Gesperrte Peers</translation>
    </message>
    <message>
        <location line="+57"/>
        <location filename="../rpcconsole.cpp" line="+311"/>
        <location line="+856"/>
        <source>Select a peer to view detailed information.</source>
        <translation>Gegenstelle auswählen, um detaillierte Informationen zu erhalten.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Whitelisted</source>
        <translation>Zugelassene</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Direction</source>
        <translation>Richtung</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Starting Block</source>
        <translation>Start Block</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Headers</source>
        <translation>Synchronisierte Kopfdaten</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Blocks</source>
        <translation>Synchronisierte Blöcke</translation>
    </message>
    <message>
        <location line="-2488"/>
        <location line="+2390"/>
        <source>User Agent</source>
        <translation>User-Agent</translation>
    </message>
    <message>
        <location line="-2370"/>
        <source>Datadir</source>
        <translation>Datadir</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Last block time (time since)</source>
        <translation>Letzte Blockzeit (Zeit seit)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Last block size</source>
        <translation>Letzte Blockgröße</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Tx pool</source>
        <translation>Transaktionen im Tx-Pool</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Orphan pool</source>
        <translation>Transaktionen im Orphan-Pool</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Messages in CAPD pool</source>
        <translation>Nachrichten im CAPD-Pool</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - usage</source>
        <translation>Tx-Pool - Nutzung</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - txns per second</source>
        <translation>Tx-Pool - txns pro Sekunde</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (Totals)</source>
        <translation>XThin (Summen)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (24-Hour Averages)</source>
        <translation>XThin (24-Stunden-Durchschnittswerte)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (Totals)</source>
        <translation>Compact (Summe)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (24-Hour Averages)</source>
        <translation>Compact (24-Stunden-Durchschnitt)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (Totals)</source>
        <translation>Graphene (Gesamt)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (24-Hour Averages)</source>
        <translation>Graphene (24-Stunden-Durchschnitt)</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Open the %1 debug log file from the current data directory. This can take a few seconds for large log files.</source>
        <translation>Öffnen Sie die Debug-Protokolldatei %1 aus dem aktuellen Datenverzeichnis. Dies kann bei großen Protokolldateien einige Sekunden dauern.</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Block Propagation</source>
        <translation>Ausbreitung blockieren</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Transaction Pools</source>
        <translation>Transaktionspools</translation>
    </message>
    <message>
        <location line="+246"/>
        <source>Decrease font size</source>
        <translation>Schriftgröße verkleinern</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Increase font size</source>
        <translation>Schriftgröße vergrößern</translation>
    </message>
    <message>
        <location line="+380"/>
        <source>&amp;Transaction Rate</source>
        <translation>&amp;Transaktionsrate</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Instantaneous Rate (1s)</source>
        <translation>Momentane Rate (1s)</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+536"/>
        <source>Peak</source>
        <translation>Spitze</translation>
    </message>
    <message>
        <location line="-463"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Runtime</source>
        <translation>Laufzeit</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>24-Hours</source>
        <translation>24 Stunden</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Displayed</source>
        <translation>Angezeigt</translation>
    </message>
    <message>
        <location line="-769"/>
        <location line="+536"/>
        <source>Average</source>
        <translation>Durchschnitt</translation>
    </message>
    <message>
        <location line="-265"/>
        <source>Smoothed Rate (60s)</source>
        <translation>Geglättete Rate (60 s)</translation>
    </message>
    <message>
        <location line="+769"/>
        <source>Services</source>
        <translation>Dienste</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Ban Score</source>
        <translation>Sperrpunktzahl</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Connection Time</source>
        <translation>Verbindungsdauer</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Send</source>
        <translation>Letzte Übertragung</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Receive</source>
        <translation>Letzter Empfang</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Ping Time</source>
        <translation>Pingzeit</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>The duration of a currently outstanding ping.</source>
        <translation>Die Laufzeit eines aktuell ausstehenden Ping.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ping Wait</source>
        <translation>Ping Wartezeit</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Time Offset</source>
        <translation>Zeitversatz</translation>
    </message>
    <message>
        <location line="-2524"/>
        <source>&amp;Open</source>
        <translation>&amp;Öffnen</translation>
    </message>
    <message>
        <location line="+431"/>
        <source>&amp;Console</source>
        <translation>&amp;Konsole</translation>
    </message>
    <message>
        <location line="+195"/>
        <source>&amp;Network Traffic</source>
        <translation>&amp;Netzwerkauslastung</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>&amp;Clear</source>
        <translation>&amp;Zurücksetzen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Totals</source>
        <translation>Gesamtbetrag</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-530"/>
        <source>In:</source>
        <translation>eingehend:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Out:</source>
        <translation>ausgehend:</translation>
    </message>
    <message>
        <location filename="../forms/debugwindow.ui" line="-704"/>
        <source>Debug log file</source>
        <translation>Debugprotokolldatei</translation>
    </message>
    <message>
        <location line="+541"/>
        <source>Clear console</source>
        <translation>Konsole zurücksetzen</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-207"/>
        <source>&amp;Disconnect Node</source>
        <translation>Knoten &amp;trennen</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Ban Node for</source>
        <translation>Knoten gebannt für</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>1 &amp;hour</source>
        <translation>1 &amp;Stunde</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;day</source>
        <translation>1 &amp;Tag</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;week</source>
        <translation>1 &amp;Woche</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;year</source>
        <translation>1 &amp;Jahr</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>&amp;Unban Node</source>
        <translation>&amp;Node entsperren</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Welcome to the %1 RPC console.</source>
        <translation>Willkommen bei der %1 RPC-Konsole.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Use up and down arrows to navigate history, and &lt;b&gt;Ctrl-L&lt;/b&gt; to clear screen.</source>
        <translation>Pfeiltaste hoch und runter, um den Verlauf durchzublättern und &lt;b&gt;Strg-L&lt;/b&gt;, um die Konsole zurückzusetzen.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type &lt;b&gt;help&lt;/b&gt; for an overview of available commands.</source>
        <translation>Bitte &lt;b&gt;help&lt;/b&gt; eingeben, um eine Übersicht verfügbarer Befehle zu erhalten.</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <source>Disabled</source>
        <translation>Behinderte</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>%1 B</source>
        <translation>%1 B</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>(node id: %1)</source>
        <translation>(Knotenkennung: %1)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>via %1</source>
        <translation>über %1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>never</source>
        <translation>nie</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Inbound</source>
        <translation>eingehend</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Outbound</source>
        <translation>ausgehend</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+6"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
</context>
<context>
    <name>ReceiveCoinsDialog</name>
    <message>
        <location filename="../forms/receivecoinsdialog.ui" line="+166"/>
        <source>&amp;Amount:</source>
        <translation>&amp;Betrag:</translation>
    </message>
    <message>
        <location line="-113"/>
        <source>&amp;Label:</source>
        <translation>&amp;Bezeichnung:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>&amp;Message:</source>
        <translation>&amp;Nachricht:</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Reuse one of the previously used receiving addresses. Reusing addresses has security and privacy issues. Do not use this unless re-generating a payment request made before.</source>
        <translation>Eine der bereits verwendeten Empfangsadressen wiederverwenden. Addressen wiederzuverwenden birgt Sicherheits- und Datenschutzrisiken. Außer zum Neuerstellen einer bereits erzeugten Zahlungsanforderung sollten Sie dies nicht nutzen.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>R&amp;euse an existing receiving address (not recommended)</source>
        <translation>Vorhandene Empfangsadresse &amp;wiederverwenden (nicht empfohlen)</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+110"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened. Note: The message will not be sent with the payment over the Nexa network.</source>
        <translation>Eine optionale Nachricht, die an die Zahlungsanforderung angehängt wird. Sie wird angezeigt, wenn die Anforderung geöffnet wird. Hinweis: Diese Nachricht wird nicht mit der Zahlung über das Nexa-Netzwerk gesendet.</translation>
    </message>
    <message>
        <location line="-136"/>
        <location line="+106"/>
        <source>An optional label to associate with the new receiving address.</source>
        <translation>Eine optionale Bezeichnung, die der neuen Empfangsadresse zugeordnet wird.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Use this form to request payments. All fields are &lt;b&gt;optional&lt;/b&gt;.</source>
        <translation>Verwenden Sie dieses Formular, um Zahlungen anzufordern. Alle Felder sind &lt;b&gt;optional&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="-136"/>
        <location line="+120"/>
        <source>An optional amount to request. Leave this empty or zero to not request a specific amount.</source>
        <translation>Ein optional angeforderte Betrag. Lassen Sie dieses Feld leer oder setzen Sie es auf 0, um keinen spezifischen Betrag anzufordern.</translation>
    </message>
    <message>
        <location line="-46"/>
        <source>Clear all fields of the form.</source>
        <translation>Alle Formularfelder zurücksetzen.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Requested payments history</source>
        <translation>Verlauf der angeforderten Zahlungen</translation>
    </message>
    <message>
        <location line="-132"/>
        <source>&amp;Request payment</source>
        <translation>&amp;Zahlung anfordern</translation>
    </message>
    <message>
        <location line="+157"/>
        <source>Show the selected request (does the same as double clicking an entry)</source>
        <translation>Ausgewählte Zahlungsanforderungen anzeigen (entspricht einem Doppelklick auf einen Eintrag)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show</source>
        <translation>Anzeigen</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Remove the selected entries from the list</source>
        <translation>Ausgewählte Einträge aus der Liste entfernen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="../receivecoinsdialog.cpp" line="+50"/>
        <source>Copy URI</source>
        <translation>URI kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Bezeichnung kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy message</source>
        <translation>Nachricht kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Betrag kopieren</translation>
    </message>
</context>
<context>
    <name>ReceiveRequestDialog</name>
    <message>
        <location filename="../forms/receiverequestdialog.ui" line="+29"/>
        <source>QR Code</source>
        <translation>QR-Code</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Copy &amp;URI</source>
        <translation>&amp;URI kopieren</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Copy &amp;Address</source>
        <translation>&amp;Addresse kopieren</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Save Image...</source>
        <translation>Grafik &amp;speichern...</translation>
    </message>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+78"/>
        <source>Request payment to %1</source>
        <translation>Zahlung anfordern an %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment information</source>
        <translation>Zahlungsinformationen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount</source>
        <translation>Betrag</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Label</source>
        <translation>Bezeichnung</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Message</source>
        <translation>Nachricht</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Resulting URI too long, try to reduce the text for label / message.</source>
        <translation>Resultierende URI ist zu lang, bitte den Text für Bezeichnung/Nachricht kürzen.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error encoding URI into QR Code.</source>
        <translation>Beim Enkodieren der URI in den QR-Code ist ein Fehler aufgetreten.</translation>
    </message>
</context>
<context>
    <name>RecentRequestsTableModel</name>
    <message>
        <location filename="../recentrequeststablemodel.cpp" line="+30"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Label</source>
        <translation>Bezeichnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message</source>
        <translation>Nachricht</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Amount</source>
        <translation>Betrag</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>(no label)</source>
        <translation>(keine Bezeichnung)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>(no message)</source>
        <translation>(keine Nachricht)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>(no amount)</source>
        <translation>(kein Betrag)</translation>
    </message>
</context>
<context>
    <name>SendCoinsDialog</name>
    <message>
        <location filename="../forms/sendcoinsdialog.ui" line="+14"/>
        <location filename="../sendcoinsdialog.cpp" line="+626"/>
        <source>Send Coins</source>
        <translation>Coins überweisen</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Coin Control Features</source>
        <translation>Münzkontrolle Funktionen</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Select specific coins you want to use in this transaction</source>
        <translation>Wählen Sie bestimmte Münzen aus, die Sie für diese Transaktion verwenden möchten</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Inputs...</source>
        <translation>Eingaben...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>automatically selected</source>
        <translation>automatisch ausgewählt</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Insufficient funds!</source>
        <translation>Unzureichender Kontostand!</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Quantity:</source>
        <translation>Anzahl:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Bytes:</source>
        <translation>Byte:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Amount:</source>
        <translation>Betrag:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Priority:</source>
        <translation>Priorität:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Fee:</source>
        <translation>Gebühr:</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>After Fee:</source>
        <translation>Abzüglich Gebühr:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Wechselgeld:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>If this is activated, but the change address is empty or invalid, change will be sent to a newly generated address.</source>
        <translation>Wenn dies aktivert, und die Wechselgeld-Adresse leer oder ungültig ist, wird das Wechselgeld einer neu erzeugten Adresse gutgeschrieben.</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+16"/>
        <source>Custom change address</source>
        <translation>Benutzerdefinierte Wechselgeld-Adresse</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>Transaction Fee:</source>
        <translation>Transaktionsgebühr:</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Choose your transaction fee.</source>
        <translation>Wählen Sie Ihre Transaktionsgebühr.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose...</source>
        <translation>Auswählen...</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>collapse fee-settings</source>
        <translation>Transaktionsgebühreneinstellungen ausblenden</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>per kilobyte</source>
        <translation>pro Kilobyte</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+16"/>
        <source>If the custom fee is set to 1000 satoshis and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 satoshis in fee, while &quot;total at least&quot; pays 1000 satoshis. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation>Wenn die benutzerdefinierte Gebühr 1000 Satoshis beträgt und die Transaktion nur 250 Byte groß ist, wird bei Auswahl von &quot;pro Kilobyte&quot; eine Gebühr in Höhe von 250 Satoshis, bei Auswahl von &quot;Mindestbetrag&quot; eine Gebühr in Höhe von 1000 Satoshis bezahlt. Bei Transaktionen die Größer als ein Kilobyte sind, werden bei beiden Optionen die Gebühren pro Kilobyte bezahlt.</translation>
    </message>
    <message>
        <location line="-64"/>
        <source>Hide</source>
        <translation>Ausblenden</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>total at least</source>
        <translation>Mindestbetrag</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Custom fee amount</source>
        <translation>Benutzerdefinierter Gebührenbetrag</translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+13"/>
        <source>Paying only the minimum fee is just fine as long as there is less transaction volume than space in the blocks. But be aware that this can end up in a never confirming transaction once there is more demand for nexa transactions than the network can process.</source>
        <translation>Nur die minimale Gebühr zu bezahlen ist so lange in Ordnung, wie weniger Transaktionsvolumen als Platz in den Blöcken vorhanden ist. Aber Vorsicht, diese Option kann dazu führen, dass Transaktionen nicht bestätigt werden, wenn mehr Bedarf an Nexa-Transaktionen besteht als das Netzwerk verarbeiten kann.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>(read the tooltip)</source>
        <translation>(den Hinweistext lesen)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Use the recommended fee amount.  You can select a faster or slower confirmation time by moving the &quot;Confirmation Time&quot; slider.</source>
        <translation>Verwenden Sie den empfohlenen Gebührenbetrag. Sie können eine schnellere oder langsamere Bestätigungszeit auswählen, indem Sie den Schieberegler „Bestätigungszeit“ verschieben.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Recommended:</source>
        <translation>Empfehlungen:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Choose a custom fee amount</source>
        <translation>Wählen Sie einen benutzerdefinierten Gebührenbetrag</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Custom:</source>
        <translation>Benutzerdefiniert:</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>(Smart fee not initialized yet. This usually takes a few blocks...)</source>
        <translation>(Intelligente Gebührenlogik ist noch nicht verfügbar. Normalerweise dauert dies einige Blöcke lang...)</translation>
    </message>
    <message>
        <location line="+29"/>
        <location line="+33"/>
        <source>Transaction confirmation time</source>
        <translation>Zeitpunkt der Transaktionsbestätigung</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Confirmation time:</source>
        <translation>Bestätigungszeit:</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>fast</source>
        <translation>schnell</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Send as zero-fee transaction if possible</source>
        <translation>Wenn möglich als gebührenfreie Transaktion senden</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(confirmation may take longer)</source>
        <translation>(Bestätigung kann länger dauern)</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Send to multiple recipients at once</source>
        <translation>An mehrere Empfänger auf einmal überweisen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add &amp;Recipient</source>
        <translation>Empfänger &amp;hinzufügen</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Clear all fields of the form.</source>
        <translation>Alle Formularfelder zurücksetzen.</translation>
    </message>
    <message>
        <location line="-880"/>
        <source>Dust:</source>
        <translation>&quot;Dust&quot;:</translation>
    </message>
    <message>
        <location line="+883"/>
        <source>Clear &amp;All</source>
        <translation>&amp;Zurücksetzen</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Current Balance</source>
        <translation>Aktueller Kontostand</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Balance:</source>
        <translation>Kontostand:</translation>
    </message>
    <message>
        <location line="-87"/>
        <source>Confirm the send action</source>
        <translation>Überweisung bestätigen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;end</source>
        <translation>&amp;Überweisen</translation>
    </message>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="-231"/>
        <source>Confirm send coins</source>
        <translation>Überweisung bestätigen</translation>
    </message>
    <message>
        <location line="-52"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+4"/>
        <source>%1 to %2</source>
        <translation>%1 an %2</translation>
    </message>
    <message>
        <location line="-289"/>
        <source>Copy quantity</source>
        <translation>Anzahl kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Betrag kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy fee</source>
        <translation>Gebühr kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Abzüglich Gebühr kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Byte kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Priorität kopieren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy change</source>
        <translation>Wechselgeld kopieren</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>&lt;b&gt;Public label:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Öffentliches Label:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Total Amount %1</source>
        <translation>Gesamtbetrag %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>or</source>
        <translation>oder</translation>
    </message>
    <message>
        <location line="+190"/>
        <source>The amount to pay must be larger than 0.</source>
        <translation>Der zu zahlende Betrag muss größer als 0 sein.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount exceeds your balance.</source>
        <translation>Der angegebene Betrag übersteigt Ihren Kontostand.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The total exceeds your balance when the %1 transaction fee is included.</source>
        <translation>Der angegebene Betrag übersteigt aufgrund der Transaktionsgebühr in Höhe von %1 Ihren Kontostand.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction creation failed!</source>
        <translation>Transaktionserstellung fehlgeschlagen!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The transaction was rejected! This might happen if some of the coins in your wallet were already spent, such as if you used a copy of wallet.dat and coins were spent in the copy but not marked as spent here.</source>
        <translation>Die Transaktion wurde abgelehnt! Dies kann passieren, wenn einige coins aus Ihrer Wallet bereits ausgegeben wurden. Beispielsweise weil Sie eine Kopie Ihrer wallet.dat genutzt, die coins dort ausgegeben haben und dies daher in der derzeit aktiven Wallet nicht vermerkt ist.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A fee higher than %1 is considered an absurdly high fee.</source>
        <translation>Eine höhere Gebühr als %1 wird als unsinnig hohe Gebühr angesehen.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Payment request expired.</source>
        <translation>Zahlungsanforderung abgelaufen.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Public Label exeeds limit of </source>
        <translation>Das öffentliche Label überschreitet das Limit von </translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Pay only the required fee of %1</source>
        <translation>Nur die notwendige Gebühr in Höhe von %1 zahlen</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>The recipient address is not valid. Please recheck.</source>
        <translation>Die Zahlungsadresse ist ungültig, bitte nochmals überprüfen.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Duplicate address found: addresses should only be used once each.</source>
        <translation>Doppelte Adresse entdeckt: Adressen dürfen jeweils nur einmal vorkommen.</translation>
    </message>
    <message>
        <location line="+252"/>
        <source>Warning: Invalid Nexa address</source>
        <translation>Warnung: Ungültige Nexa-Adresse</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Confirm custom change address</source>
        <translation>Bestätigen Sie die benutzerdefinierte Änderungsadresse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The address you selected for change is not part of this wallet. Any or all funds in your wallet may be sent to this address. Are you sure?</source>
        <translation>Die Adresse, die Sie zum Ändern ausgewählt haben, ist nicht Teil dieser Brieftasche. Einige oder alle Gelder in Ihrer Brieftasche können an diese Adresse gesendet werden. Bist du dir sicher?</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>(no label)</source>
        <translation>(keine Bezeichnung)</translation>
    </message>
    <message>
        <location line="-28"/>
        <source>Warning: Unknown change address</source>
        <translation>Warnung: Unbekannte Wechselgeld-Adresse</translation>
    </message>
    <message>
        <location line="-777"/>
        <source>Copy dust</source>
        <translation>&quot;Dust&quot; kopieren</translation>
    </message>
    <message>
        <location line="+291"/>
        <source>Are you sure you want to send?</source>
        <translation>Wollen Sie die Überweisung ausführen?</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>added as transaction fee</source>
        <translation>als Transaktionsgebühr hinzugefügt</translation>
    </message>
</context>
<context>
    <name>SendCoinsEntry</name>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="+86"/>
        <location line="+677"/>
        <location line="+560"/>
        <source>A&amp;mount:</source>
        <translation>Betra&amp;g:</translation>
    </message>
    <message>
        <location line="-1224"/>
        <source>Pay &amp;To:</source>
        <translation>E&amp;mpfänger:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Choose previously used address</source>
        <translation>Bereits verwendete Adresse auswählen</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>Private Description:</source>
        <translation>Private Beschreibung:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Private &amp;Label:</source>
        <translation>Privates &amp;Label:</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>The Nexa address to send the payment to</source>
        <translation>Die Zahlungsadresse der Überweisung</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>Adresse aus der Zwischenablage einfügen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+553"/>
        <location line="+560"/>
        <source>Remove this entry</source>
        <translation>Diesen Eintrag entfernen</translation>
    </message>
    <message>
        <location line="-1091"/>
        <source>A message that was attached to the coin: URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>Eine Nachricht, die an die Münze angehängt wurde: URI, die mit der Transaktion zu Referenzzwecken gespeichert wird. Hinweis: Diese Nachricht wird nicht über das Nexa-Netzwerk gesendet.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter a private label for this address to add it to the list of used addresses</source>
        <translation>Geben Sie eine private Bezeichnung für diese Adresse ein, um sie der Liste der verwendeten Adressen hinzuzufügen</translation>
    </message>
    <message>
        <location line="+11"/>
        <location filename="../sendcoinsentry.cpp" line="+43"/>
        <source>Enter a public label for this transaction</source>
        <translation>Geben Sie ein öffentliches Label für diese Transaktion ein</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Public Label:</source>
        <translation>Öffentliches Label:</translation>
    </message>
    <message>
        <location line="-180"/>
        <source>The fee will be deducted from the amount being sent. The recipient will receive less coins than you enter in the amount field. If multiple recipients are selected, the fee is split equally.</source>
        <translation>Die Gebühr wird vom zu überweisenden Betrag abgezogen. Der Empfänger wird also weniger coins erhalten, als Sie im Betrags-Feld eingegeben haben. Falls mehrere Empfänger ausgewählt wurden, wird die Gebühr gleichmäßig verteilt.</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Amount to send</source>
        <translation>Betrag zu senden</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>S&amp;ubtract fee from amount</source>
        <translation>Gebühr vom Betrag ab&amp;ziehen</translation>
    </message>
    <message>
        <location line="+643"/>
        <source>This is an unauthenticated payment request.</source>
        <translation>Dies ist keine beglaubigte Zahlungsanforderung.</translation>
    </message>
    <message>
        <location line="+556"/>
        <source>This is an authenticated payment request.</source>
        <translation>Dies ist eine beglaubigte Zahlungsanforderung.</translation>
    </message>
    <message>
        <location filename="../sendcoinsentry.cpp" line="-12"/>
        <source>A message that was attached to the %1 URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>Eine an die &quot;%1&quot;-URI angefügte Nachricht, die zusammen mit der Transaktion gespeichert wird. Hinweis: Diese Nachricht wird nicht über das Nexa-Netzwerk gesendet.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Enter a private label for this address to add it to your address book</source>
        <translation>Geben Sie eine private Bezeichnung für diese Adresse ein, um sie Ihrem Adressbuch hinzuzufügen</translation>
    </message>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="-541"/>
        <location line="+556"/>
        <source>Pay To:</source>
        <translation>Empfänger:</translation>
    </message>
    <message>
        <location line="-522"/>
        <location line="+560"/>
        <source>Memo:</source>
        <translation>Memo:</translation>
    </message>
</context>
<context>
    <name>ShutdownWindow</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+76"/>
        <source>%1 is shutting down...</source>
        <translation>%1 wird heruntergefahren...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do not shut down the computer until this window disappears.</source>
        <translation>Fahren Sie den Computer nicht herunter, bevor dieses Fenster verschwindet.</translation>
    </message>
</context>
<context>
    <name>SignVerifyMessageDialog</name>
    <message>
        <location filename="../forms/signverifymessagedialog.ui" line="+14"/>
        <source>Signatures - Sign / Verify a Message</source>
        <translation>Signaturen - eine Nachricht signieren / verifizieren</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sign Message</source>
        <translation>Nachricht &amp;signieren</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You can sign messages/agreements with your addresses to prove you can receive coins sent to them. Be careful not to sign anything vague or random, as phishing attacks may try to trick you into signing your identity over to them. Only sign fully-detailed statements you agree to.</source>
        <translation>Sie können Nachrichten/Vereinbarungen mit Hilfe Ihrer Adressen signieren, um zu beweisen, dass Sie coins empfangen können, die an diese Adressen überwiesen werden. Seien Sie vorsichtig und signieren Sie nichts Vages oder Willkürliches, um Ihre Indentität vor Phishingangriffen zu schützen. Signieren Sie nur vollständig-detaillierte Aussagen, mit denen Sie auch einverstanden sind.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The Nexa address to sign the message with</source>
        <translation>Die Nexa-Adresse mit der die Nachricht signiert wird</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+210"/>
        <source>Choose previously used address</source>
        <translation>Bereits verwendete Adresse auswählen</translation>
    </message>
    <message>
        <location line="-200"/>
        <location line="+210"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="-200"/>
        <source>Paste address from clipboard</source>
        <translation>Adresse aus der Zwischenablage einfügen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter the message you want to sign here</source>
        <translation>Zu signierende Nachricht hier eingeben</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Signature</source>
        <translation>Signatur</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Copy the current signature to the system clipboard</source>
        <translation>Aktuelle Signatur in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Sign the message to prove you own this Nexa address</source>
        <translation>Die Nachricht signieren, um den Besitz dieser Nexa-Adresse zu beweisen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sign &amp;Message</source>
        <translation>&amp;Nachricht signieren</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all sign message fields</source>
        <translation>Alle &quot;Nachricht signieren&quot;-Felder zurücksetzen</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+143"/>
        <source>Clear &amp;All</source>
        <translation>&amp;Zurücksetzen</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>&amp;Verify Message</source>
        <translation>Nachricht &amp;verifizieren</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter the receiver&apos;s address, message (ensure you copy line breaks, spaces, tabs, etc. exactly) and signature below to verify the message. Be careful not to read more into the signature than what is in the signed message itself, to avoid being tricked by a man-in-the-middle attack. Note that this only proves the signing party receives with the address, it cannot prove sendership of any transaction!</source>
        <translation>Geben Sie die Zahlungsadresse des Empfängers, Nachricht (achten Sie darauf Zeilenumbrüche, Leerzeichen, Tabulatoren usw. exakt zu kopieren) und Signatur unten ein, um die Nachricht zu verifizieren. Vorsicht, interpretieren Sie nicht mehr in die Signatur hinein, als in der signierten Nachricht selber enthalten ist, um nicht von einem Man-in-the-middle-Angriff hinters Licht geführt zu werden. Beachten Sie dass dies nur beweißt, dass die signierende Partei über diese Adresse Überweisungen empfangen kann!</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>The Nexa address the message was signed with</source>
        <translation>Die Nexa-Adresse mit der die Nachricht signiert wurde</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Verify the message to ensure it was signed with the specified Nexa address</source>
        <translation>Die Nachricht verifizieren, um sicherzustellen, dass diese mit der angegebenen Nexa-Adresse signiert wurde</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify &amp;Message</source>
        <translation>&amp;Nachricht verifizieren</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all verify message fields</source>
        <translation>Alle &quot;Nachricht verifizieren&quot;-Felder zurücksetzen</translation>
    </message>
    <message>
        <location filename="../signverifymessagedialog.cpp" line="+40"/>
        <source>Click &quot;Sign Message&quot; to generate signature</source>
        <translation>Auf &quot;Nachricht signieren&quot; klicken, um die Signatur zu erzeugen</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Wallet unlock was cancelled.</source>
        <translation>Wallet-Entsperrung wurde abgebrochen.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Message signed.</source>
        <translation>Nachricht signiert.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Message verified.</source>
        <translation>Nachricht verifiziert.</translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="../networkstyle.cpp" line="+19"/>
        <source>[testnet]</source>
        <translation>[Testnetz]</translation>
    </message>
</context>
<context>
    <name>TokenDescDialog</name>
    <message>
        <location filename="../forms/tokendescdialog.ui" line="+17"/>
        <source>Token details</source>
        <translation>Details zum Token</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This pane shows a detailed description of the token&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dieser Bereich zeigt eine detaillierte Beschreibung des Tokens&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>TokenDisplayDialog</name>
    <message>
        <location filename="../forms/tokendisplaydialog.ui" line="+14"/>
        <source>Token Display Manager</source>
        <translation>Token-Anzeige-Manager</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add a token to the list below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fügen Sie der Liste unten ein Token hinzu&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add New Token</source>
        <translation>Neues Token Hinzufügen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove a token from the list below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Entfernen Sie ein Token aus der Liste unten&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove Token</source>
        <translation>Token entfernen</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Display</source>
        <translation>Anzeige</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ticker</source>
        <translation>Ticker</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Group ID</source>
        <translation>Gruppen-ID</translation>
    </message>
    <message>
        <location filename="../tokendisplaydialog.cpp" line="+43"/>
        <source>Copy GroupID</source>
        <translation>Gruppen-ID kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Token Ticker</source>
        <translation>Token-Ticker kopieren</translation>
    </message>
</context>
<context>
    <name>TokenHistoryView</name>
    <message>
        <location filename="../tokenhistoryview.cpp" line="+78"/>
        <location line="+19"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>Heute</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>Diese Woche</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>Diesen Monat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>Letzten Monat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>Dieses Jahr</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>Zeitraum...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>Empfangen über</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent</source>
        <translation>Übertragen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>Eigenüberweisung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mint</source>
        <translation>Geprägt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>Andere</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter token ID to search</source>
        <translation>Geben Sie die Token-ID ein, um zu suchen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>Minimaler Betrag</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy token id</source>
        <translation>Token-ID kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Betrag kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction id</source>
        <translation>Transaktions-id kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction idem</source>
        <translation>Transaktionsidem kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy raw transaction</source>
        <translation>Kopiere rohe Transaktion</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>Transaktionsdetails anzeigen</translation>
    </message>
    <message>
        <location line="+167"/>
        <source>Export Token History</source>
        <translation>Token-Verlauf exportieren</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Kommagetrennte-Datei (*.csv)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Confirmed</source>
        <translation>Bestätigt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation>Beobachtet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>Bezeichnung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction ID</source>
        <translation>Transaktions-ID</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction Idem</source>
        <translation>Transaktion Idem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token ID</source>
        <translation>Token ID</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token Amount</source>
        <translation>Token-Betrag</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Failed</source>
        <translation>Exportieren fehlgeschlagen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the token history to %1.</source>
        <translation>Beim Versuch, den Token-Verlauf unter %1 zu speichern, ist ein Fehler aufgetreten.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>Exportieren erfolgreich</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The token history was successfully saved to %1.</source>
        <translation>Der Token-Verlauf wurde erfolgreich unter %1 gespeichert.</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Range:</source>
        <translation>Zeitraum:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>bis</translation>
    </message>
</context>
<context>
    <name>TokenTableModel</name>
    <message>
        <location filename="../tokentablemodel.cpp" line="+354"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Token ID</source>
        <translation>Token ID</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Net Amount</source>
        <translation>Netto-Betrag</translation>
    </message>
    <message numerus="yes">
        <location line="+60"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Offen für %n weiteren Block</numerusform>
            <numerusform>Offen für %n weitere Blöcke</numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open until %1</source>
        <translation>Offen bis %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Offline</source>
        <translation>Offline</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>Unbestätigt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>Wird bestätigt (%1 von %2 empfohlenen Bestätigungen)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>Bestätigt (%1 Bestätigungen)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicted</source>
        <translation>in Konflikt stehend</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation>Doppelt ausgegeben</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation>Verlassen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>Unreif (%1 Bestätigungen, wird verfügbar sein nach %2)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>Dieser Block wurde von keinem anderen Knoten empfangen und wird wahrscheinlich nicht angenommen werden!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>Erzeugt, jedoch nicht angenommen</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation>Empfangen über</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>Empfangen von</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent</source>
        <translation>Übertragen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>Eigenüberweisung</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mint</source>
        <translation>Geprägt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Melt</source>
        <translation>Schmelzen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation>Öffentliches Etikett</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation>Andere</translation>
    </message>
    <message>
        <location line="+325"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>Transaktionsstatus, fahren Sie mit der Maus über dieses Feld, um die Anzahl der Bestätigungen zu sehen.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>Datum und Uhrzeit zu der die Transaktion empfangen wurde.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>Art der Transaktion.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>Zeigt an, ob eine beobachtete Adresse in diese Transaktion involviert ist.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>Benutzerdefinierte Absicht bzw. Verwendungszweck der Transaktion.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>Der Betrag, der dem Kontostand abgezogen oder hinzugefügt wurde.</translation>
    </message>
</context>
<context>
    <name>TokensViewDialog</name>
    <message>
        <location filename="../forms/tokensviewdialog.ui" line="+14"/>
        <source>Tokens</source>
        <translation>Token</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enable/Disable which tokens in the wallet are to be displayed in QT&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Aktivieren/Deaktivieren, welche Token im Wallet in QT angezeigt werden sollen&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Manage Displayed Tokens</source>
        <translation>Angezeigte Token verwalten</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tokens in Wallet:</source>
        <translation>Token im Wallet:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tokens Displayed: </source>
        <translation>Angezeigte Token:</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Amount: </source>
        <translation>Betrag: </translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The number of tokens to send&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Die Anzahl der zu sendenden Token&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>  Pay To: </source>
        <translation>  Empfänger: </translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The Nexa address to send the payment to</source>
        <translation>Die Zahlungsadresse der Überweisung</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation>Bereits verwendete Adresse auswählen</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>Adresse aus der Zwischenablage einfügen</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remove this entry</source>
        <translation>Diesen Eintrag entfernen</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Send tokens&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Token senden&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Send</source>
        <translation>Senden</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Token ID</source>
        <translation>Token ID</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The unique token identifier</source>
        <translation>Die eindeutige Token-ID</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>The token group identifier</source>
        <translation>Der Tokengruppenbezeichner</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The name of the token</source>
        <translation>Der Name des Tokens</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ticker</source>
        <translation>Ticker</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The token ticker symbol</source>
        <translation>Das Token-Tickersymbol</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Balance</source>
        <translation>Kontostand</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed token balance</source>
        <translation>Bestätigter Token-Saldo</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pending</source>
        <translation>Ausstehend</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed token balance</source>
        <translation>Unbestätigter Token-Saldo</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Sub</source>
        <translation>Sub</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>If checked then this item is a subgroup.</source>
        <translation>Wenn diese Option aktiviert ist, handelt es sich bei diesem Element um eine Untergruppe.</translation>
    </message>
    <message>
        <location filename="../tokensviewdialog.cpp" line="+397"/>
        <location line="+7"/>
        <location line="+4"/>
        <source>Data</source>
        <translation>Daten</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>string</source>
        <translation>Zeichenfolge</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+4"/>
        <source>num</source>
        <translation>Num</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>NaN</source>
        <translation>NaN</translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+84"/>
        <location line="+5"/>
        <location line="+5"/>
        <source>Total Mintage:</source>
        <translation>Gesamtauflage:</translation>
    </message>
    <message>
        <location line="-93"/>
        <location line="+94"/>
        <source>Token mintage is unavailable because the database needs a reindex</source>
        <translation>Die Token-Mintage ist nicht verfügbar, da die Datenbank eine -reindex</translation>
    </message>
    <message>
        <location line="-86"/>
        <location line="+43"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Ticker:</source>
        <translation>Ticker:</translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+43"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Hash:</source>
        <translation>Hash:</translation>
    </message>
    <message>
        <location line="-39"/>
        <location line="+43"/>
        <source>Decimals:</source>
        <translation>Dezimalstellen:</translation>
    </message>
    <message>
        <location line="-37"/>
        <location line="+14"/>
        <location line="+62"/>
        <location line="+14"/>
        <source>Current Authorities:</source>
        <translation>Aktuelle Behörden:</translation>
    </message>
    <message>
        <location line="-88"/>
        <location line="+76"/>
        <source>Mint:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Melt:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Renew:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Rescript:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Subgroup:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+76"/>
        <source>Token authorities are unavailable because the database needs a reindex</source>
        <translation>Token-Autoritäten sind nicht verfügbar, da die Datenbank neu indiziert werden muss</translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+77"/>
        <source>Balance:</source>
        <translation>Kontostand:</translation>
    </message>
    <message>
        <location line="-76"/>
        <location line="+77"/>
        <source>Pending:</source>
        <translation>Ausstehend:</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Are you sure you want to send?</source>
        <translation>Wollen Sie die Überweisung ausführen?</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;b&gt;Token(s)&lt;/b&gt;</source>
        <translation>&lt;b&gt;Token&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Confirm send tokens</source>
        <translation>Bestätigen Sie das Senden von Token</translation>
    </message>
</context>
<context>
    <name>TrafficGraphWidget</name>
    <message>
        <location filename="../trafficgraphwidget.cpp" line="+74"/>
        <source>KB/s</source>
        <translation>KB/s</translation>
    </message>
</context>
<context>
    <name>TransactionDesc</name>
    <message>
        <location filename="../transactiondesc.cpp" line="+35"/>
        <source>Open until %1</source>
        <translation>Offen bis %1</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>conflicted</source>
        <translation>in Konflikt stehend</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/offline</source>
        <translation>%1/offline</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/unconfirmed</source>
        <translation>%1/unbestätigt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 confirmations</source>
        <translation>%1 Bestätigungen</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message numerus="yes">
        <location line="+7"/>
        <source>, broadcast through %n node(s)</source>
        <translation>
            <numerusform>, über %n Knoten übertragen</numerusform>
            <numerusform>, über %n Knoten übertragen</numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Source</source>
        <translation>Quelle</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Generated</source>
        <translation>Erzeugt</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+13"/>
        <location line="+122"/>
        <source>From</source>
        <translation>Von</translation>
    </message>
    <message>
        <location line="-142"/>
        <location line="+21"/>
        <location line="+35"/>
        <location line="+108"/>
        <source>To</source>
        <translation>An</translation>
    </message>
    <message>
        <location line="-209"/>
        <source>double spent</source>
        <translation>doppelt ausgegeben</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>abandoned</source>
        <translation>verlassen</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>change address</source>
        <translation>Adresse ändern</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>own address</source>
        <translation>eigene Adresse</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+113"/>
        <source>watch-only</source>
        <translation>beobachtet</translation>
    </message>
    <message>
        <location line="-110"/>
        <location line="+28"/>
        <location line="+107"/>
        <source>label</source>
        <translation>Bezeichnung</translation>
    </message>
    <message>
        <location line="-116"/>
        <location line="+57"/>
        <location line="+48"/>
        <location line="+71"/>
        <source>Public label:</source>
        <translation>Öffentliches Label:</translation>
    </message>
    <message>
        <location line="-149"/>
        <location line="+41"/>
        <location line="+63"/>
        <location line="+47"/>
        <location line="+256"/>
        <source>Credit</source>
        <translation>Gutschrift</translation>
    </message>
    <message numerus="yes">
        <location line="-404"/>
        <source>matures in %n more block(s)</source>
        <translation>
            <numerusform>reift noch %n weiteren Block</numerusform>
            <numerusform>reift noch %n weitere Blöcke</numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>not accepted</source>
        <translation>nicht angenommen</translation>
    </message>
    <message>
        <location line="+102"/>
        <location line="+31"/>
        <location line="+261"/>
        <source>Debit</source>
        <translation>Belastung</translation>
    </message>
    <message>
        <location line="-280"/>
        <source>Total debit</source>
        <translation>Gesamtbelastung</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Total credit</source>
        <translation>Gesamtgutschrift</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction fee</source>
        <translation>Transaktionsgebühr</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Net amount</source>
        <translation>Nettobetrag</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+13"/>
        <source>Message</source>
        <translation>Nachricht</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transaction ID</source>
        <translation>Transaktions-ID</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction Idem</source>
        <translation>Transaktion Idem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction size</source>
        <translation>Transaktionsgröße</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Merchant</source>
        <translation>Händler</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Generated coins must mature %1 blocks before they can be spent. When you generated this block, it was broadcast to the network to be added to the block chain. If it fails to get into the chain, its state will change to &quot;not accepted&quot; and it won&apos;t be spendable. This may occasionally happen if another node generates a block within a few seconds of yours.</source>
        <translation>Erzeugte Nexas müssen %1 Blöcke lang reifen, bevor sie ausgegeben werden können. Als Sie diesen Block erzeugten, wurde er an das Netzwerk übertragen, um ihn der Blockkette hinzuzufügen. Falls dies fehlschlägt wird der Status in &quot;nicht angenommen&quot; geändert und Sie werden keine Nexas gutgeschrieben bekommen. Das kann gelegentlich passieren, wenn ein anderer Knoten einen Block fast zeitgleich erzeugt.</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+33"/>
        <location line="+48"/>
        <source>Token ID</source>
        <translation>Token ID</translation>
    </message>
    <message>
        <location line="-76"/>
        <location line="+33"/>
        <location line="+48"/>
        <source>Ticker</source>
        <translation>Ticker</translation>
    </message>
    <message>
        <location line="-80"/>
        <location line="+33"/>
        <location line="+48"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="-77"/>
        <location line="+32"/>
        <location line="+48"/>
        <source>Decimals</source>
        <translation>Dezimalstellen</translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+4"/>
        <source>Melt</source>
        <translation>Schmelzen</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+4"/>
        <location line="+56"/>
        <location line="+4"/>
        <source>Mint</source>
        <translation>Geprägt</translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+4"/>
        <source>Amount Sent</source>
        <translation>Gesendeter Betrag</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+4"/>
        <source>Amount Received</source>
        <translation>Erhaltener Betrag</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Debug information</source>
        <translation>Debuginformationen</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transaction</source>
        <translation>Transaktion</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inputs</source>
        <translation>Eingaben</translation>
    </message>
    <message>
        <location line="-133"/>
        <location line="+4"/>
        <location line="+30"/>
        <location line="+4"/>
        <location line="+56"/>
        <location line="+4"/>
        <location line="+57"/>
        <source>Amount</source>
        <translation>Betrag</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+3"/>
        <source>true</source>
        <translation>wahr</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+3"/>
        <source>false</source>
        <translation>falsch</translation>
    </message>
    <message>
        <location line="-533"/>
        <source>, has not been successfully broadcast yet</source>
        <translation>, wurde noch nicht erfolgreich übertragen</translation>
    </message>
    <message numerus="yes">
        <location line="-40"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Offen für %n weiteren Block</numerusform>
            <numerusform>Offen für %n weitere Blöcke</numerusform>
        </translation>
    </message>
    <message>
        <location line="+73"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
</context>
<context>
    <name>TransactionDescDialog</name>
    <message>
        <location filename="../forms/transactiondescdialog.ui" line="+14"/>
        <source>Transaction details</source>
        <translation>Transaktionsdetails</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>This pane shows a detailed description of the transaction</source>
        <translation>Dieser Bereich zeigt eine detaillierte Beschreibung der Transaktion an</translation>
    </message>
</context>
<context>
    <name>TransactionGraphWidget</name>
    <message>
        <location filename="../transactiongraphwidget.cpp" line="+148"/>
        <source>tps</source>
        <translation>tps</translation>
    </message>
</context>
<context>
    <name>TransactionTableModel</name>
    <message>
        <location filename="../transactiontablemodel.cpp" line="+239"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>Unreif (%1 Bestätigungen, wird verfügbar sein nach %2)</translation>
    </message>
    <message numerus="yes">
        <location line="-29"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Offen für %n weiteren Block</numerusform>
            <numerusform>Offen für %n weitere Blöcke</numerusform>
        </translation>
    </message>
    <message>
        <location line="-60"/>
        <source>Address or Label</source>
        <translation>Adresse oder Etikett</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Open until %1</source>
        <translation>Offen bis %1</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>Bestätigt (%1 Bestätigungen)</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>Dieser Block wurde von keinem anderen Knoten empfangen und wird wahrscheinlich nicht angenommen werden!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>Erzeugt, jedoch nicht angenommen</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Offline</source>
        <translation>Offline</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>Unbestätigt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>Wird bestätigt (%1 von %2 empfohlenen Bestätigungen)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Conflicted</source>
        <translation>in Konflikt stehend</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation>Doppelt ausgegeben</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation>Verlassen</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation>Empfangen über</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>Empfangen von</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent to</source>
        <translation>Überwiesen an</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>Eigenüberweisung</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mined</source>
        <translation>Erarbeitet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation>Öffentliches Etikett</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation>Andere</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>watch-only</source>
        <translation>beobachtet</translation>
    </message>
    <message>
        <location line="+277"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>Transaktionsstatus, fahren Sie mit der Maus über dieses Feld, um die Anzahl der Bestätigungen zu sehen.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>Datum und Uhrzeit zu der die Transaktion empfangen wurde.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>Art der Transaktion.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>Zeigt an, ob eine beobachtete Adresse in diese Transaktion involviert ist.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>Benutzerdefinierte Absicht bzw. Verwendungszweck der Transaktion.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>Der Betrag, der dem Kontostand abgezogen oder hinzugefügt wurde.</translation>
    </message>
</context>
<context>
    <name>TransactionView</name>
    <message>
        <location filename="../transactionview.cpp" line="+77"/>
        <location line="+19"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>Heute</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>Diese Woche</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>Diesen Monat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>Letzten Monat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>Dieses Jahr</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>Zeitraum...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>Empfangen über</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent to</source>
        <translation>Überwiesen an</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>Eigenüberweisung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mined</source>
        <translation>Erarbeitet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>Andere</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter address or label to search</source>
        <translation>Zu suchende Adresse oder Bezeichnung eingeben</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>Minimaler Betrag</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy address</source>
        <translation>Adresse kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Bezeichnung kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Betrag kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction id</source>
        <translation>Transaktions-id kopieren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy raw transaction</source>
        <translation>Kopiere rohe Transaktion</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit label</source>
        <translation>Bezeichnung bearbeiten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>Transaktionsdetails anzeigen</translation>
    </message>
    <message>
        <location line="+176"/>
        <source>Export Transaction History</source>
        <translation>Transaktionsverlauf exportieren</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Watch-only</source>
        <translation>Beobachtet</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Idem</source>
        <translation>Idem</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Failed</source>
        <translation>Exportieren fehlgeschlagen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the transaction history to %1.</source>
        <translation>Beim Speichern des Transaktionsverlaufs nach %1 ist ein Fehler aufgetreten.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>Exportieren erfolgreich</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction history was successfully saved to %1.</source>
        <translation>Speichern des Transaktionsverlaufs nach %1 war erfolgreich.</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Kommagetrennte-Datei (*.csv)</translation>
    </message>
    <message>
        <location line="-179"/>
        <source>Copy transaction idem</source>
        <translation>Transaktionsidem kopieren</translation>
    </message>
    <message>
        <location line="+188"/>
        <source>Confirmed</source>
        <translation>Bestätigt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>Bezeichnung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Range:</source>
        <translation>Zeitraum:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>bis</translation>
    </message>
</context>
<context>
    <name>UnitDisplayStatusBarControl</name>
    <message>
        <location filename="../nexagui.cpp" line="+117"/>
        <source>Unit to show amounts in. Click to select another unit.</source>
        <translation>Die Einheit in der Beträge angezeigt werden. Klicken, um eine andere Einheit auszuwählen.</translation>
    </message>
</context>
<context>
    <name>UnlimitedDialog</name>
    <message>
        <location filename="../forms/unlimited.ui" line="+14"/>
        <source>Unlimited</source>
        <translation>Unlimited</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Mining</source>
        <translation>&amp;Verminun</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+10"/>
        <source>The largest block that will be mined</source>
        <translation>Der größte Block, der abgebaut wird</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Maximum Generated Block Size (bytes) </source>
        <translation>Maximal generierte Blockgröße (Byte)</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Network</source>
        <translation>&amp;Netzwerk</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Bandwidth Restrictions in KBytes/sec (check to enable):</source>
        <translation>Bandbreitenbeschränkungen in KBytes/Sek. (zur Aktivierung markieren):</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Send</source>
        <translation>Senden</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+71"/>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <location line="-35"/>
        <location line="+59"/>
        <source>Average</source>
        <translation>Durchschnitt</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Receive</source>
        <translation>Erhalten</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Active command-line options that override above options:</source>
        <translation>Aktive Kommandozeilenoptionen, die obige Konfiguration überschreiben:</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Reset all client options to default.</source>
        <translation>Setzt die Clientkonfiguration auf Standardwerte zurück.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>&amp;Optionen zurücksetzen</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>A&amp;bbrechen</translation>
    </message>
    <message>
        <location filename="../unlimiteddialog.cpp" line="-345"/>
        <source>Confirm options reset</source>
        <translation>Zurücksetzen der Konfiguration bestätigen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is a global reset of all settings!</source>
        <translation>Dies ist ein globaler Reset aller Einstellungen!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client restart required to activate changes.</source>
        <translation>Clientneustart nötig, um die Änderungen zu aktivieren.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>Client wird beendet. Möchten Sie den Vorgang fortsetzen?</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+39"/>
        <location line="+42"/>
        <location line="+42"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>Upstream-Traffic-Shaping-Parameter dürfen nicht leer sein</translation>
    </message>
    <message>
        <location line="-108"/>
        <location line="+36"/>
        <location line="+43"/>
        <location line="+41"/>
        <source>Traffic shaping parameters have to be greater than 0.</source>
        <translation>Traffic-Shaping-Parameter müssen größer als 0 sein.</translation>
    </message>
</context>
<context>
    <name>WalletFrame</name>
    <message>
        <location filename="../walletframe.cpp" line="+31"/>
        <source>No wallet has been loaded.</source>
        <translation>Es wurde keine Wallet geladen.</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Token Database Status</source>
        <translation>Status der Token-Datenbank</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token wallet not available because a -reindex is needed. Click &quot;Ok&quot; to perform a one time -reindex on the next startup. Then shutdown Nexa-Qt and restart to complete the process.</source>
        <translation>Token-Wallet nicht verfügbar, da ein -reindex erforderlich ist. Klicken Sie auf „OK“, um beim nächsten Start eine einmalige Neuindizierung durchzuführen. Fahren Sie dann Nexa-Qt herunter und starten Sie es neu, um den Vorgang abzuschließen.</translation>
    </message>
</context>
<context>
    <name>WalletModel</name>
    <message>
        <location filename="../walletmodel.cpp" line="+300"/>
        <source>Send Coins</source>
        <translation>Coins überweisen</translation>
    </message>
</context>
<context>
    <name>WalletView</name>
    <message>
        <location filename="../walletview.cpp" line="+46"/>
        <location line="+20"/>
        <source>&amp;Export</source>
        <translation>E&amp;xportieren</translation>
    </message>
    <message>
        <location line="-19"/>
        <location line="+20"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Daten der aktuellen Ansicht in eine Datei exportieren</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Backup Wallet</source>
        <translation>Wallet sichern</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+21"/>
        <source>Wallet Data (*.dat)</source>
        <translation>Wallet-Daten (*.dat)</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Backup Failed</source>
        <translation>Sicherung fehlgeschlagen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the wallet data to %1.</source>
        <translation>Beim Speichern der Wallet-Daten nach %1 ist ein Fehler aufgetreten.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The wallet data was successfully saved to %1.</source>
        <translation>Speichern der Wallet-Daten nach %1 war erfolgreich.</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Backup Successful</source>
        <translation>Sicherung erfolgreich</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Restore Wallet</source>
        <translation>Wallet wiederherstellen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Restore Failed</source>
        <translation>Wiederherstellung fehlgeschlagen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to restore the wallet data to %1.</source>
        <translation>Beim Versuch, die Wallet-Daten auf %1 wiederherzustellen, ist ein Fehler aufgetreten.</translation>
    </message>
</context>
<context>
    <name>nexa</name>
    <message>
        <location filename="../nexastrings.cpp" line="+56"/>
        <source>Prune configured below the minimum of %d MiB.  Please use a higher number.</source>
        <translation>Kürzungsmodus wurde kleiner als das Minimum in Höhe von %d MiB konfiguriert. Bitte verwenden Sie einen größeren Wert.</translation>
    </message>
    <message>
        <location line="+106"/>
        <source>Error: A fatal internal error occurred, see debug.log for details</source>
        <translation>Fehler: Ein schwerer interner Fehler ist aufgetreten, siehe debug.log für Details</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Pruning blockstore...</source>
        <translation>Kürze Blockspeicher...</translation>
    </message>
    <message>
        <location line="-165"/>
        <source>Distributed under the MIT software license, see the accompanying file COPYING or &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</source>
        <translation>Veröffentlicht unter der MIT-Softwarelizenz, siehe beiligende Datei COPYING oder &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Rescans are not possible in pruned mode. You will need to use -resync which will download the whole blockchain again.</source>
        <translation>Im bereinigten Modus sind keine erneuten Scans möglich. Sie müssen -resync verwenden, wodurch die gesamte Blockchain erneut heruntergeladen wird.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The block database contains a block which appears to be from the future. This may be due to your computer&apos;s date and time being set incorrectly. Only rebuild the block database if you are sure that your computer&apos;s date and time are correct</source>
        <translation>Die Block-Datenbank enthält einen Block, der in der Zukunft auftaucht. Dies kann daran liegen, dass die Systemzeit Ihres Computers falsch eingestellt ist. Stellen Sie die Block-Datenbank nur wieder her, wenn Sie sich sicher sind, dass Ihre Systemzeit korrekt eingestellt ist</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>This is a pre-release test build - use at your own risk - do not use for mining or merchant applications</source>
        <translation>Dies ist eine Vorab-Testversion - Verwendung auf eigene Gefahr - nicht für Mining- oder Handelsanwendungen nutzen</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>WARNING: abnormally high number of blocks generated, %d blocks received in the last %d hours (%d expected)</source>
        <translation>Warnung: Es wurde eine ungewöhnlich hohe Anzahl Blöcke erzeugt, %d Blöcke wurden in den letzten %d Stunden empfangen (%d wurden erwartet)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: check your network connection, %d blocks received in the last %d hours (%d expected)</source>
        <translation>Warnung: Überprüpfen Sie ihre Netzwerkverbindung, %d Blöcke wurden in den letzten %d Stunden empfangen (%d wurden erwartet)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.</source>
        <translation>Warnung: Das Netzwerk scheint nicht vollständig übereinzustimmen! Einige Miner scheinen Probleme zu haben.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.</source>
        <translation>Warnung: Wir scheinen nicht vollständig mit unseren Gegenstellen übereinzustimmen! Sie oder die anderen Knoten müssen unter Umständen Ihre Client-Software aktualisieren.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>You can not run &quot;-salvagewallet&quot; as an HD wallet.

Please relaunch Nexa with &quot;-usehd=0&quot;.</source>
        <translation>Sie können „-salvagewallet“ nicht als HD-Wallet ausführen.

Bitte starten Sie Nexa mit „-usehd=0“ neu.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Corrupted block database detected</source>
        <translation>Beschädigte Blockdatenbank erkannt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to rebuild the block database now?</source>
        <translation>Möchten Sie die Blockdatenbank jetzt neu aufbauen?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error initializing block database</source>
        <translation>Fehler beim Initialisieren der Blockdatenbank</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing wallet database environment %s!</source>
        <translation>Fehler beim Initialisieren der Wallet-Datenbankumgebung %s!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error opening block database</source>
        <translation>Fehler beim Öffnen der Blockdatenbank</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error: Disk space is low!</source>
        <translation>Fehler: Zu wenig freier Speicherplatz auf dem Datenträger!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Failed to listen on any port. Use -listen=0 if you want this.</source>
        <translation>Fehler, es konnte kein Port abgehört werden. Wenn dies so gewünscht wird -listen=0 verwenden.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Importing...</source>
        <translation>Importiere...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Incorrect or no genesis block found. Wrong datadir for network?</source>
        <translation>Fehlerhafter oder kein Genesis-Block gefunden. Falsches Datenverzeichnis für das Netzwerk?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid -onion address: &apos;%s&apos;</source>
        <translation>Ungültige &quot;-onion&quot;-Adresse: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Not enough file descriptors available.</source>
        <translation>Nicht genügend Datei-Deskriptoren verfügbar.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Prune cannot be configured with a negative value.</source>
        <translation>Kürzungsmodus kann nicht mit einem negativen Wert konfiguriert werden.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune mode is incompatible with -txindex.</source>
        <translation>Kürzungsmodus ist nicht mit -txindex kompatibel.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Signing token transaction failed</source>
        <translation>Die Signaturtoken-Transaktion ist fehlgeschlagen</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>User Agent comment (%s) contains unsafe characters.</source>
        <translation>Der User Agent Kommentar  (%s) enthält unsichere Zeichen.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying blocks...</source>
        <translation>Verifiziere Blöcke...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying wallet...</source>
        <translation>Verifiziere Wallet...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Waiting for Genesis Block...</source>
        <translation>Warten auf Genesis-Block...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet %s resides outside data directory %s</source>
        <translation>Wallet %s liegt außerhalb des Datenverzeichnisses %s</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Error: Listening for incoming connections failed (listen returned error %s)</source>
        <translation>Fehler: Abhören nach eingehenden Verbindungen fehlgeschlagen (listen meldete Fehler %s)</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>The transaction amount is too small to send after the fee has been deducted</source>
        <translation>Der Transaktionsbetrag ist zum senden zu niedrig, nachdem die Gebühr abgezogen wurde</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit &lt;https://www.openssl.org/&gt; and cryptographic software written by Eric Young and UPnP software written by Thomas Bernard.</source>
        <translation>Dieses Produkt enthält Software, die vom OpenSSL-Projekt zur Verwendung im OpenSSL-Toolkit &lt;https://www.openssl.org/&gt; entwickelt wird, sowie von Eric Young geschriebene kryptographische Software und von Thomas Bernard geschriebene UPnP-Software.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Activating best chain...</source>
        <translation>Aktiviere beste Blockkette...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cannot resolve -whitebind address: &apos;%s&apos;</source>
        <translation>Kann Adresse in -whitebind nicht auflösen: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copyright (C) 2015-%i The Bitcoin Unlimited Developers</source>
        <translation>Urheberrecht (C) 2015-%i Die &quot;Bitcoin Unlimited&quot;-Entwickler</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Information</source>
        <translation>Hinweis</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid netmask specified in -whitelist: &apos;%s&apos;</source>
        <translation>Ungültige Netzmaske angegeben in -whitelist: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Need to specify a port with -whitebind: &apos;%s&apos;</source>
        <translation>Angabe eines Ports benötigt für -whitebind: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Signing transaction failed</source>
        <translation>Signierung der Transaktion fehlgeschlagen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Starting txindex</source>
        <translation>txindex starten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction amount is too small to pay the fee</source>
        <translation>Der Transaktionsbetrag ist zu niedrig, um die Gebühr zu bezahlen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is experimental software.</source>
        <translation>Dies ist experimentelle Software.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amount too small</source>
        <translation>Transaktionsbetrag zu niedrig</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amounts must be positive</source>
        <translation>Transaktionsbeträge müssen positiv sein</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction has %d outputs. Maximum outputs allowed is %d</source>
        <translation>Transaktion hat %d Ausgaben. Die maximal zulässige Ausgabe ist %d</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction of %d bytes is too large. Maximum allowed is %d bytes</source>
        <translation>Die Transaktion von %d Bytes ist zu groß. Maximal zulässig sind %d Bytes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction too large for fee policy</source>
        <translation>Transaktion ist für die Gebührenrichtlinie zu groß</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer (bind returned error %s)</source>
        <translation>Kann auf diesem Computer nicht an %s binden (bind meldete Fehler %s)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-42"/>
        <source>Loading addresses...</source>
        <translation>Lade Adressen...</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Invalid -proxy address: &apos;%s&apos;</source>
        <translation>Ungültige Adresse in -proxy: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Unknown network specified in -onlynet: &apos;%s&apos;</source>
        <translation>Unbekannter Netztyp in -onlynet angegeben: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Cannot resolve -bind address: &apos;%s&apos;</source>
        <translation>Kann Adresse in -bind nicht auflösen: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -externalip address: &apos;%s&apos;</source>
        <translation>Kann Adresse in -externalip nicht auflösen: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Loading block index...</source>
        <translation>Lade Blockindex...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet...</source>
        <translation>Lade Wallet...</translation>
    </message>
    <message>
        <location line="-38"/>
        <source>Cannot downgrade wallet</source>
        <translation>Wallet kann nicht auf eine ältere Version herabgestuft werden</translation>
    </message>
    <message>
        <location line="-131"/>
        <source>Nexa</source>
        <translatorcomment>Nexa</translatorcomment>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The %s developers</source>
        <translation>Die %s-Entwickler</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bitcoin Bitcoin XT and Bitcoin Unlimited</source>
        <translation>Bitcoin Bitcoin XT und Bitcoin Unlimited</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-wallet.maxTxFee is set very high! Fees this large could be paid on a single transaction.</source>
        <translation>-wallet.maxTxFee ist sehr hoch angesetzt! Gebühren dieser Höhe könnten für eine einzelne Transaktion gezahlt werden.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>-wallet.payTxFee is set very high! This is the transaction fee you will pay if you send a transaction.</source>
        <translation>-wallet.payTxFee ist sehr hoch angesetzt! Dies ist die Transaktionsgebühr, die Sie bezahlen, wenn Sie eine Transaktion senden.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot obtain a lock on data directory %s. %s is probably already running.</source>
        <translation>Das Datenverzeichnis %s kann nicht gesperrt werden. %s läuft wahrscheinlich bereits.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Could not locate RPC credentials. No authentication cookie could be found, and no rpcpassword is set in the configuration file (%s)</source>
        <translation>RPC-Anmeldeinformationen konnten nicht gefunden werden. Es konnte kein Authentifizierungscookie gefunden werden und in der Konfigurationsdatei (%s) ist kein rpcpassword gesetzt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; contained invalid data - see debug.log</source>
        <translation>Die Bereitstellungskonfigurationsdatei &apos;%s&apos; enthielt ungültige Daten – siehe debug.log</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading %s: You can&apos;t enable HD on a already existing non-HD wallet</source>
        <translation>Fehler beim Laden von %s: Sie können HD nicht für eine bereits vorhandene Nicht-HD-Wallet aktivieren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error reading %s! All keys read correctly, but transaction data or address book entries might be missing or incorrect.</source>
        <translation>Fehler beim Lesen von %s! Alle Schlüssel werden korrekt gelesen, aber Transaktionsdaten oder Adressbucheinträge können fehlen oder falsch sein.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error reading from the coin database.
Details: %s

Do you want to reindex on the next restart?</source>
        <translation>Fehler beim Lesen aus der Münzdatenbank.
Einzelheiten: %s

Möchten Sie beim nächsten Neustart neu indizieren?</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Failed to listen on all P2P ports. Failing as requested by -bindallorfail.</source>
        <translation>Fehler beim Abhören aller P2P-Ports. Fehler wie von -bindallorfail angefordert.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fee: %ld is larger than configured maximum allowed fee of : %ld.  To change, set &apos;wallet.maxTxFee&apos;.</source>
        <translation>Gebühr: %ld ist größer als die konfigurierte maximal zulässige Gebühr von: %ld. Setzen Sie die wallet.TxFee auf einen niedrigeren Wert</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.maxTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least the minrelay fee of %s to prevent stuck transactions)</source>
        <translation>Ungültiger Betrag für -wallet.maxTxFee=&lt;Betrag&gt;: &apos;%u&apos; (muss mindestens die MinRelay-Gebühr von %s sein, um festgefahrene Transaktionen zu verhindern)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.payTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least %s)</source>
        <translation>Ungültiger Betrag für -wallet.payTxFee=&lt;Betrag&gt;: &apos;%u&apos; (muss mindestens %s sein)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please check that your computer&apos;s date and time are correct! If your clock is wrong, %s will not work properly.</source>
        <translation>Bitte überprüfen Sie, ob Datum und Uhrzeit Ihres Computers korrekt sind! Wenn Ihre Uhr falsch ist, wird %s nicht richtig funktionieren.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Prune: last wallet synchronisation goes beyond pruned data. You need to -reindex (download the whole blockchain again in case of pruned node)</source>
        <translation>Bereinigen: Die letzte Brieftaschensynchronisierung geht über beschnittene Daten hinaus. Sie müssen -reindizieren (im Falle eines beschnittenen Knotens die gesamte Blockchain erneut herunterladen)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reducing -maxconnections from %d to %d because of file descriptor limitations (unix) or winsocket fd_set limitations (windows). If you are a windows user there is a hard upper limit of 1024 which cannot be changed by adjusting the node&apos;s configuration.</source>
        <translation>Reduzieren von -maxconnections von %d auf %d aufgrund von Dateideskriptorbeschränkungen (Unix) oder winsocket fd_set-Beschränkungen (Windows). Wenn Sie ein Windows-Benutzer sind, gibt es eine feste Obergrenze von 1024, die nicht durch Anpassen der Konfiguration des Knotens geändert werden kann.</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Total length of network version string with uacomments added exceeded the maximum length (%i) and have been truncated.  Reduce the number or size of uacomments to avoid truncation.</source>
        <translation>Die Gesamtlänge der Netzwerkversionszeichenfolge mit hinzugefügten ua-Kommentaren hat die maximale Länge (%i) überschritten und wurde abgeschnitten. Reduzieren Sie die Anzahl oder Größe von ua-Kommentaren, um ein Abschneiden zu vermeiden.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Transaction has %d inputs and %d outputs. Maximum inputs allowed are %d and maximum outputs are %d</source>
        <translation>Transaktion hat %d Eingaben und %d Ausgaben. Die maximal zulässigen Eingaben sind %d und die maximal zulässigen Ausgaben sind %d</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction has %d inputs. Maximum inputs allowed is %d. Try reducing inputs by transferring a smaller amount.</source>
        <translation>Transaktion hat %d Eingaben. Maximal zulässige Eingaben sind %d. Versuchen Sie, die Eingaben zu reduzieren, indem Sie einen kleineren Betrag überweisen.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Warning: Could not open deployment configuration CSV file &apos;%s&apos; for reading</source>
        <translation>Warnung: Die CSV-Datei „%s“ der Bereitstellungskonfiguration konnte nicht zum Lesen geöffnet werden</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Warning: Unknown block versions being mined! It&apos;s possible unknown rules are in effect</source>
        <translation>Warnung: Unbekannte Blockversionen werden abgebaut! Es ist möglich, dass unbekannte Regeln in Kraft sind</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Wallet file corrupt, data salvaged! Original %s saved as %s in %s; if your balance or transactions are incorrect you should restore from a backup.</source>
        <translation>Warnung: Wallet-Datei beschädigt, Daten gerettet! Original %s als %s in %s gespeichert; Wenn Ihr Guthaben oder Ihre Transaktionen nicht korrekt sind, sollten Sie von einer Sicherung wiederherstellen.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You are trying to restore the same wallet which you are trying to replace.</source>
        <translation>Sie versuchen, dasselbe Wallet wiederherzustellen, das Sie ersetzen möchten.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>You are trying to use -wallet.auto but neither -spendzeroconfchange nor -wallet.instant is turned on</source>
        <translation>Sie versuchen, -wallet.auto zu verwenden, aber weder -spendzeroconfchange noch -wallet.instant sind aktiviert</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You can not send free transactions if you have configured a -relay.limitFreeRelay of zero</source>
        <translation>Sie können keine kostenlosen Transaktionen senden, wenn Sie ein -relay.limitFreeRelay von Null konfiguriert haben</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You need to rebuild the database using -resync to go back to unpruned mode.  This will redownload the entire blockchain.</source>
        <translation>Sie müssen die Datenbank mit -resync neu erstellen, um in den nicht bereinigten Modus zurückzukehren. Dadurch wird die gesamte Blockchain erneut heruntergeladen.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&quot;Restore Wallet&quot; succeeded and a backup of the previous wallet was saved to: %s.


When you click &quot;OK&quot; Nexa will shutdown to complete the process.</source>
        <translation>„Wallet wiederherstellen“ war erfolgreich und eine Sicherung der vorherigen Wallet wurde gespeichert unter: %s.


Wenn Sie auf „OK“ klicken, wird Nexa heruntergefahren, um den Vorgang abzuschließen.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>%s </source>
        <translation>%s </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%s corrupt, salvage failed</source>
        <translation>%s beschädigt, salvage fehlgeschlagen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-maxtxpool must be at least %d MB</source>
        <translation>-cache.maxTxPool muss mindestens %d MB groß sein</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-xthinbloomfiltersize must be at least %d Bytes</source>
        <translation>-xthinbloomfiltersize muss mindestens %d Bytes betragen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot write default address</source>
        <translation>Standardadresse kann nicht geschrieben werden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CommitTransaction failed.</source>
        <translation>CommitTransaktion fehlgeschlagen.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; not found</source>
        <translation>Bereitstellungskonfigurationsdatei „%s“ nicht gefunden</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading %s</source>
        <translation>Fehler beim Laden von %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet corrupted</source>
        <translation>Fehler beim Laden von %s: Brieftasche beschädigt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet requires newer version of %s</source>
        <translation>Fehler beim Laden von %s: Wallet erfordert eine neuere Version von %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: You can&apos;t disable HD on a already existing HD wallet</source>
        <translation>Fehler beim Laden von %s: Sie können HD auf einer bereits vorhandenen HD-Brieftasche nicht deaktivieren</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: Keypool ran out, please call keypoolrefill first</source>
        <translation>Fehler: Der Schlüsselpool ist erschöpft. Bitte rufen Sie zuerst keypoolrefill auf</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Initialization sanity check failed. %s is shutting down.</source>
        <translation>Plausibilitätsprüfung der Initialisierung fehlgeschlagen. %s wird heruntergefahren.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds for this token.  Need %d more.</source>
        <translation>Das Guthaben für diesen Token reicht nicht aus. Benötige %d mehr.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds or funds not confirmed</source>
        <translation>Unzureichende Mittel oder Mittel nicht bestätigt</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Keypool ran out, please call keypoolrefill first</source>
        <translation>Keypool abgelaufen, bitte rufen Sie zuerst keypoolrefill an</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading Orphanpool</source>
        <translation>Lade Orphanpool</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading TxPool</source>
        <translation>Lade TxPool</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading banlist...</source>
        <translation>Lade banlist...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Opening Block database...</source>
        <translation>Block-Datenbank wird geöffnet...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Coins Cache database...</source>
        <translation>Coins Cache Datenbank wird geöffnet...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Description database...</source>
        <translation>Tokenbeschreibungsdatenbank wird geöffnet...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Mintage database...</source>
        <translation>Token-Mintage-Datenbank wird geöffnet...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening UTXO database...</source>
        <translation>UTXO-Datenbank wird geöffnet...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2009-%i The Bitcoin Core Developers</source>
        <translation>Teile Copyright (C) 2009-%i The Bitcoin Core Developers</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2014-%i The Bitcoin XT Developers</source>
        <translation>Teile Copyright (C) 2014-%i Die Bitcoin XT-Entwickler</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Reaccepting Wallet Transactions</source>
        <translation>Erneutes Akzeptieren von Wallet-Transaktionen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rescanning...</source>
        <translation>Durchsuche erneut...</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Turn off auto consolidate and try sending again.</source>
        <translation>Deaktivieren Sie die automatische Konsolidierung und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer. %s is probably already running.</source>
        <translation>Auf diesem Computer kann keine Bindung zu %s hergestellt werden. %s läuft wahrscheinlich bereits.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to start RPC services. See debug log for details.</source>
        <translation>RPC-Dienste können nicht gestartet werden. Einzelheiten finden Sie im Debug-Protokoll.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Upgrading block database...This could take a while.</source>
        <translation>Upgrade der Blockdatenbank... Dies kann eine Weile dauern.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database </source>
        <translation>Upgrade der txindex-Datenbank </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database...</source>
        <translation>Upgrade der txindex-Datenbank...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Wallet needed to be rewritten: restart %s to complete</source>
        <translation>Brieftasche musste neu geschrieben werden: Starten Sie %s neu, um den Vorgang abzuschließen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zapping all transactions from wallet...</source>
        <translation>Alle Transaktionen aus der Wallet zappen ...</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>Done loading</source>
        <translation>Laden abgeschlossen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-62"/>
        <source>Wallet is not password protected. Your funds may be at risk! Goto &quot;Settings&quot; and then select &quot;Encrypt Wallet&quot; to create a password.</source>
        <translation>Wallet ist nicht passwortgeschützt. Ihre Gelder könnten gefährdet sein! Gehen Sie zu &quot;Einstellungen&quot; und wählen Sie dann &quot;Wallet verschlüsseln&quot; um ein Passwort zu erstellen.</translation>
    </message>
</context>
</TS>
